<?php get_header(); ?>

          <div class="notfound release-background">
            <div class="notfound__inner">
              <div class="notfound__image lazyload"><img src="<?= get_template_directory_uri() ?>/src/img/common/404.png"></div>
              <div class="notfound__title lazyload">
                <h2>404 Not Found</h2>
                <p>ページが見つかりませんでした。</p>
              </div>
              <div class="notfound__content lazyload">
                <div class="notfound__lead">
                  <p>お探しのページはいずれかの理由により見つかりませんでした。</p>
                </div>
                <ul class="notfound__list">
                  <li>1. ページのURLが変更された可能性があります。</li>
                  <li>2. ページが削除された可能性があります。</li>
                  <li>3. アドレス（URL）をタイピングミスしている可能性があります。</li>
                </ul>
              </div>
              <div class="notfound__button flex-middle-center-wrap lazyload"><a class="button lazyload flex-middle-center -black" href="<?= home_url() ?>"><span class="button__text">トップページへ戻る</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></a></div>
            </div>
          </div>


<?php get_footer(); ?>
