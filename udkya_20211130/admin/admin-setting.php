<?php
if( ! function_exists ('coco_display_text') ) {
  function coco_display_text($arg1) {
    $type  = isset($arg1['type']) ? $arg1['type'] : 'text';
    $class = isset($arg1['class']) ?  $arg1['class'] : 'regular-text';

    $html  = '';
    $html .= '<input class="'.$class.'" name="'.$arg1['label_for'].'" id="'.$arg1['label_for'].'" '.'type="'.$type.'" value="'.esc_html(get_option($arg1['label_for'])).'">';
    $html .= isset($arg1['exp']) ? '<p class="general_notice">'.$arg1['exp'].'</p>' : '';
    echo $html;
  }
}
if( ! function_exists ('coco_display_textarea') ) {
  function coco_display_textarea($arg1) {
    $rows  = isset($arg1['rows']) ? $arg1['rows'] : '5';
    $class = isset($arg1['class']) ? $arg1['class'] : 'regular-text';

    $html  = '';
    $html .= '<textarea class="'.$class.'" name="'.$arg1['label_for'].'" id="'.$arg1['label_for'].'" rows="'.$rows.'">'.esc_html(get_option($arg1['label_for'])).'</textarea>';
    $html .= isset($arg1['exp']) ? '<p class="general_notice">'.$arg1['exp'].'</p>' : '';
    echo $html;
  }
}

define('MENU_ID','setting_menu');

add_action('admin_menu', function () {
  add_menu_page( '運動会屋トップページ設定', '運動会屋トップページ設定', 'edit_theme_options' , MENU_ID, 'setting_menu_setting_page','' , 2 );
});
// ページの中身のHTML
function setting_menu_setting_page() {
  $menu_id = MENU_ID;
  ?>
  <div id="top" class="wrap">
      <h2>トップページ設定</h2>
        <?php
        // add_menu_page()で追加している場合
        // options-head.phpが読み込まれずメッセージが出ないため
        // メッセージが出るように読み込む。
          global $parent_file;
          if ( $parent_file != 'options-general.php' ) {
              require(ABSPATH . 'wp-admin/options-head.php');
            }
        ?>

        <h3>■ ゴールテープ</h3>
        <a href="<?= home_url() ?>/wp-admin/edit.php?post_type=goaltape-words">ゴールテープの文字設定へ</a>

        <form method="post" action="options.php" novalidate="novalidate" >
          <style>
            .form-table {
              background-color: #EDE5CE;
            }
            .form-table th {
              padding: 20px 10px 20px;
            }

            h4 {
              font-size: 1.3em;
              color: #0B080A;
              align: center;
            }
            h4 img {
              padding-right: 10px;
              width: 60px;
            }
          </style>
        <?php
            // 隠しフィールドなどを出力します(register_setting()の$option_groupと同じものを指定)。
            settings_fields( $menu_id );
            // 入力項目を出力します(設定ページのslugを指定)。
            do_settings_sections( $menu_id );
            // 送信ボタンを出力します。
            submit_button();
        ?>
        </form>
    </div>
  <?php
}

define('SET_NUM', 3);
$settings = [ 
  array(
    'id'      => 'setting_title',
    'title'   => 'タイトル', 
    'callback'=> 'coco_display_text',
    'arg'     => array(
                  'exp'   => '')
    )
  ,array(
    'id'      => 'setting_prefix',
    'title'   => 'プレフィックス',
    'callback'=> 'coco_display_text',
    'arg'     => array(
                  'exp'   => '')
    )
  ,array(
    'id'      => 'setting_number',
    'title'   => '数字', 
    'callback'=> 'coco_display_text',
    'arg'     => array(
                  'exp'   => '')
    )
  ,array(
    'id'      => 'setting_suffix',
    'title'   => '単位',
    'callback'=> 'coco_display_text',
    'arg'     => array(
                  'exp'   => '')
    )
    ,array(
      'id'      => 'setting_small',
      'title'   => '補足',
      'callback'=> 'coco_display_text',
      'arg'     => array(
                    'class' => 'regular-text',
                    'exp'   => '')
    )
];

/* option fieldの追加 */
add_filter('admin_init', function () {
  global $settings;
  $menu_id = MENU_ID;
  
  add_settings_section(
    'section_top_1',
    '<h3 id="setting1">■ 運動会屋とは</h3>',
    '',
    $menu_id
  );

  $images = ['src/img/index/icon-group-black.svg','src/img/index/icon-hold-white.svg','src/img/index/icon-hachimaki-black.svg'];
  for($i=0;$i<SET_NUM;$i++) {
    foreach ($settings as $setting) {
      add_settings_section(
        'section_top_1_sub_' . $i,
        '<h4><img src="'.get_template_directory_uri().'/'.$images[$i].'">'.( $i + 1 ) . 'つ目</h4>',
        'eg_setting_section_callback_function',
        $menu_id
      );
      $option_name = $setting['id'] . '_' . $i;
      register_setting($menu_id, $option_name );
      add_settings_field(
        $option_name,
        $setting['title'],
        $setting['callback'],
        $menu_id,
        'section_top_1_sub_'. $i,
        array_merge(array( 'label_for' => $option_name ), $setting['arg'] )
      );
    }
  }

  add_settings_section(
    'section_top_2',
    '<h3 id="setting1">■ よくある質問</h3>',
    '',
    $menu_id
  );
    $arg = array( 'exp' => '');
    $option_name = 'setting_faq_category';
    register_setting($menu_id, $option_name );
    add_settings_field(
      $option_name,
      'FAQのカテゴリ数',
      'callback_faq_category_num',
      $menu_id,
      'section_top_2',
      $arg
    );
});
function callback_faq_category_num( $arg1 ) {
  $label = 'setting_faq_category';
  $data = get_option($label);
  $taxonomy = 'faq-category';
  $terms = get_terms( array( 'taxonomy' => $taxonomy ));
?>
<?php foreach($terms as $term) :
    if(!empty($data)) {
      $checked = in_array($term->term_id, $data) ? ' checked' : '';
    }
?>
  <input type="checkbox" id="<?=$label.$term->term_id?>" name="<?=$label?>[]" value="<?=$term->term_id?>"<?=$checked?>>
  <label for="<?=$label.$term->term_id ?>" ><?= $term->name ?></label>
<?php endforeach; ?>
<?php
  $html .= isset($arg1['exp']) ? '<p class="general_notice">'.$arg1['exp'].'</p>' : '';
  echo $html;
}


function eg_setting_section_callback_function( $arg ) {
  // セクションの紹介文を出力
  // echo '<p>id: ' . $arg['id'] . '</p>';             // id: eg_setting_section
  // echo '<p>title: ' . $arg['title'] . '</p>';       // title: Example settings section in reading
  // echo '<p>callback: ' . $arg['callback'] . '</p>'; // callback: eg_setting_section_callback_function
}

function get_about_options() {
  global $settings;
  $abouts = [];
  for($i=0;$i<SET_NUM;$i++) {
    $set = [];
    foreach( $settings as $setting )  {
      $option_name = $setting['id'] . '_' . $i;
      $set[$setting['id']] = get_option( $option_name );
    }
    $abouts[] = $set;
  }
  return $abouts;
}