<?php
/* 管理画面の設定 */

	add_action('wp_dashboard_setup', 'coco_remove_dashboard_widgets');
	function coco_remove_dashboard_widgets() {
	  global $wp_meta_boxes;

	  // unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	  // unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
	  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	  //unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	}
	add_action("widgets_init", "remove_widgets", 11);
	function remove_widgets() {
		unregister_widget("WP_Widget_Archives"); //アーカイブ
		unregister_widget("WP_Widget_Calendar"); //カレンダー
		unregister_widget("WP_Widget_Categories"); //カテゴリー
		unregister_widget("WP_Widget_Meta"); //メタ情報
		unregister_widget("WP_Widget_Pages"); //固定ページ
		unregister_widget("WP_Widget_Recent_Comments"); //最近のコメント
		unregister_widget("WP_Widget_Recent_Posts"); //最近の投稿
		unregister_widget("WP_Widget_RSS"); //RSS
		unregister_widget("WP_Widget_Search"); //検索
		unregister_widget("WP_Widget_Tag_Cloud"); //タグクラウド
		unregister_widget("WP_Widget_Text"); //テキスト
		unregister_widget("WP_Nav_Menu_Widget"); //ナビゲーションメニュー
		unregister_widget("WP_Widget_Media_Video"); //動画
		unregister_widget("WP_Widget_Media_Image"); //画像
		unregister_widget("WP_Widget_Media_Audio"); //音声
		unregister_widget("WP_Widget_Media_Gallery"); //ギャラリー
		unregister_widget("WP_Widget_Custom_HTML"); //カスタムHTML
	}

	add_filter('dashboard_recent_posts_query_args', 'coco_dashboard_recent_posts_query_args', 10, 1);
	function coco_dashboard_recent_posts_query_args($query_args){
		$query_args['post_type'] = array('post');
		if($query_args['post_status'] == 'publish'){
			$query_args['posts_per_page'] = 30;
			return $query_args;
		}
	}

	add_filter('admin_footer_text', '__return_empty_string');

	add_filter('manage_posts_columns', 'coco_custom_post_column');
	function coco_custom_post_column($columns){
		//unset($columns['cb']);
		//unset($columns['title']);
		unset($columns['author']);
		//unset($columns['categories']);
		//unset($columns['tags']);
		unset($columns['comments']);
		//unset($columns['date']);
		return $columns;
	}

	/* 0.5 */
	/* setting post */
	add_theme_support( 'post-thumbnails', array( 'post', 'voices', 'columns' ) );

	add_filter('manage_posts_columns', 'coco_posts_thumb_column');
	function coco_posts_thumb_column($columns){
		if(!preg_match('/post_type/', $_SERVER['REQUEST_URI'])){
			$columns['thumbnail'] = 'アイキャッチ';
		}
		return $columns;
	}
	add_action('manage_posts_custom_column', 'coco_add_posts_thumb_column', 10, 2);
	function coco_add_posts_thumb_column($column_name, $post_id){
		if('thumbnail' == $column_name){
			$thumbnail = get_the_post_thumbnail($post_id, array(140, 0), 'thumbnail');
			if(isset($thumbnail) && $thumbnail){
				echo $thumbnail;
			}else{
				echo __('<img src="'.get_template_directory_uri().'/admin/img/altimage.jpg" width="200" />');
			}
		}
	}

	/* 「投稿」を「お知らせ」に変更 */
	add_action( 'admin_menu', 'change_post_menu_label' );
	add_action( 'admin_menu', 'change_post_object_label' );
	function change_post_menu_label() {
		global $menu;
		global $submenu;
		$menu[5][0] = 'お知らせ';
		$submenu['edit.php'][5][0] = '一覧';
		$submenu['edit.php'][10][0] = '新しいお知らせ';
		// $submenu['edit.php'][16][0] = 'タグ';
	}
	function change_post_object_label() {
		global $wp_post_types;
		$labels = &$wp_post_types['post']->labels;
		$labels->name = 'お知らせ';
		$labels->singular_name = 'お知らせ';
		$labels->add_new = _x('追加', 'お知らせ');
		$labels->add_new_item = 'お知らせの新規追加';
		$labels->edit_item = 'お知らせの編集';
		$labels->new_item = '新規お知らせ';
		$labels->view_item = 'お知らせを表示';
		$labels->search_items = 'お知らせを検索';
		$labels->not_found = '記事が見つかりませんでした';
		$labels->not_found_in_trash = 'ゴミ箱に記事は見つかりませんでした';
	}

	// 投稿アーカイブページのカスタマイズ
	add_filter('register_post_type_args', function ($args, $post_type) {
		if ($post_type == 'post'){
			$args['rewrite'] = array('slug' => 'news', 'with_front' => true);
			$args['labels']['name'] = 'お知らせ';
			$args['has_archive'] = true;
		}
		return $args;
	}, 10, 2);

	// 投稿アーカイブページはhome_urlになってしまうため、置き換える
	add_filter('post_type_archive_link', function ( $link, $post_type ) {
		if ($post_type == 'post'){
			return home_url('news');
		}
		return $link;
	}, 10, 2);

	// 投稿タイプの single ページの permalink の前に付けたいカテゴリを追加する
	// カテゴリは、管理画面＞パーマリンク＞カテゴリベースで設定する
	add_action('init', 'custom_rewrite_basic');
	function custom_rewrite_basic() {
		add_rewrite_rule('^news/([^/]+)/([0-9]+)/?', 'index.php?category_name=$matches[1]&post_type=post&p=$matches[2]', 'top');
		add_rewrite_rule('^news/([^/]+)/page/([0-9]+)/?', 'index.php?category_name=$matches[1]&post_type=post&paged=$matches[2]', 'top');
		add_rewrite_rule('^news/([^/]+)/([^/]+)/([0-9]+)/?', 'index.php?category_name=$matches[2]&post_type=post&p=$matches[3]', 'top'); // サブカテゴリがある時用
	}
	add_filter('rewrite_rules_array', 'kill_rewrites');
	function kill_rewrites($rules){
		unset($rules['news/([^/]+)/?$']);
    return $rules;
}
	add_filter( 'pre_post_link',	function ( $permalink ) {
    $permalink = '/news' . $permalink;
    return $permalink;
	});

	add_action('admin_print_scripts', 'coco_admin_scripts');
	function coco_admin_scripts(){
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
    wp_enqueue_style('thickbox');
	}

	add_filter('login_headerurl', 'coco_custom_login_logo_url');
	function coco_custom_login_logo_url(){
		return get_bloginfo('url');
	}

	add_filter('login_headertext', 'coco_custom_login_logo_url_title');
	function coco_custom_login_logo_url_title(){
		return 'View Site';
	}

	add_action('admin_menu','coco_custom_menus');
	function coco_custom_menus(){
		remove_menu_page('edit-comments.php');

		if(!current_user_can('level_10')){
			// Main Menus
			//remove_menu_page('index.php');
			//remove_menu_page('edit.php');
			//remove_menu_page('upload.php');
			remove_menu_page('link-manager.php');
			//remove_menu_page('edit.php?post_type=page');
		//	remove_menu_page('edit-comments.php');
			//remove_menu_page('themes.php');
			remove_menu_page('plugins.php');
			//remove_menu_page('users.php');
			remove_menu_page('tools.php');
			//remove_menu_page('options-general.php');
			// Sub Menus
			remove_submenu_page('index.php', 'update-core.php');
			//remove_submenu_page('edit.php', 'post-new.php');
			//remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');
			// remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
			//remove_submenu_page('upload.php', 'media-new.php');
			//remove_submenu_page('edit.php?post_type=page', 'post-new.php?post_type=page')
			remove_submenu_page('plugins.php', 'plugin-install.php');
			remove_submenu_page('plugins.php', 'plugin-editor.php');
			remove_submenu_page('users.php', 'user-new.php');
			remove_submenu_page('users.php', 'profile.php');
			remove_submenu_page('tools.php', 'import.php');
			remove_submenu_page('tools.php', 'export.php');
			remove_submenu_page('options-general.php', 'options-writing.php');
			remove_submenu_page('options-general.php', 'options-reading.php');
			remove_submenu_page('options-general.php', 'options-discussion.php');
			remove_submenu_page('options-general.php', 'options-media.php');
			remove_submenu_page('options-general.php', 'options-permalink.php');
			//remove_submenu_page('options-general.php', 'privacy.php');
		}
	}
	// Removes from post and pages
	add_action('init', 'remove_comment_support', 100);
	function remove_comment_support() {
		remove_post_type_support( 'post', 'comments' );
		remove_post_type_support( 'page', 'comments' );
	}
	// Removes from admin bar
	add_action('admin_bar_menu', 'coco_custom_adminbar', 1000);
	function coco_custom_adminbar($wp_admin_bar){
		$adminbar_contents = array(
			'wp-logo',
			//'site-name',
			'view-site',
			'updates',
			'comments',
			'new-content',
			//'my-account',
			'user-info',
			'edit-profile'
			//'logout'
		);
		foreach($adminbar_contents as $key){
			$wp_admin_bar -> remove_node($key);
		}
	}

	// edit role
	add_action( 'admin_init', 'theme_caps');
	function theme_caps() {
		$editor = get_role('editor');
		$add_caps = array(
			 'edit_theme_options' // 外観＞テーマの表示、　カスタマイズ、メニュー
			// 'manage_options' // 設定
		);
		foreach($add_caps as $key){
			$editor -> add_cap($key);
		}
		$editor->remove_cap( 'manage_options');
	}



	add_filter('admin_bar_menu', 'coco_change_hello_text', 25);
	function coco_change_hello_text($wp_admin_bar){
		$my_account=$wp_admin_bar -> get_node('my-account');
		$newtitle = str_replace('こんにちは、','ようこそ ', $my_account -> title);
		$wp_admin_bar->add_node(array(
	        'id' => 'my-account',
			'title' => $newtitle,
		));
	}

	add_action('admin_bar_menu', 'coco_target_blank', 100);
	function coco_target_blank($wp_admin_bar){
	    $wp_admin_bar -> add_menu(
	    	array(
	      'id'   => 'site-name',
				'meta' => array('target' => '_blank'),
			)
		);
	}

	add_filter('admin_footer_text', 'coco_custom_admin_footer');
	function coco_custom_admin_footer(){
		echo '';
	}

	add_action('init','coco_remove_post_support');
	function coco_remove_post_support(){
		//remove_post_type_support('post', 'title');           // タイトル
		//remove_post_type_support('post', 'editor');          // 本文
		remove_post_type_support('post', 'author');          // 作成者
		//remove_post_type_support('post', 'thumbnail');       // アイキャッチ画像
		// remove_post_type_support('post', 'excerpt');         // 抜粋
		remove_post_type_support('post', 'trackbacks');      // トラックバック
		// remove_post_type_support('post', 'custom-fields');   // カスタムフィールド
		remove_post_type_support('post', 'comments');        // コメント
		remove_post_type_support('page', 'comments');        // コメント
		// remove_post_type_support('post', 'revisions');       // リビジョン
		// remove_post_type_support('post', 'page-attributes'); // 表示順
		remove_post_type_support('post', 'post-formats');    // 投稿フォーマット
		//unregister_taxonomy_for_object_type('category', 'post'); // カテゴリ
		// unregister_taxonomy_for_object_type('post_tag', 'post'); // タグ
	}
	add_post_type_support('page','excerpt');

	add_action('login_enqueue_scripts', 'coco_cuntom_login_style');
	function coco_cuntom_login_style(){
		echo "\n".'<link rel="stylesheet" href="'.get_template_directory_uri().'/login/login.css?'.date('Ymjhis').'" type="text/css" />';
	}

	// add_action('admin_head', 'coco_custom_admin_css_js', 11);
	// function coco_custom_admin_css_js(){
	// 	if(is_admin()){
	// 		// if(!current_user_can('level_10')){
	// 			echo '<link rel="stylesheet" type="text/css" href="'.get_template_directory_uri().'/admin/admin.css">'."\n";
	// 		// }
	// 	}
	// }

	/* Editor Style */
	add_theme_support( 'wp-block-styles' );
	add_theme_support('editor-styles');
	add_editor_style( 'editor-style.css' );
	add_theme_support( 'editor-font-sizes' );  					// 文字サイズの選択設定を削除
	add_theme_support( 'disable-custom-font-sizes' );   // 文字サイズの数値設定を削除
	add_theme_support( 'editor-color-palette' );				// 色指定を削除
	add_theme_support( 'disable-custom-colors' ); 			// 色指定を削除
	add_filter( 'allowed_block_types', 'custom_allowed_block_types' );
	function custom_allowed_block_types( $allowed_block_types ) {
		$allowed_block_types = array(
			// 一般ブロック
			'core/paragraph',           // 段落
			'core/heading',             // 見出し
			'core/image',               // 画像
			'core/quote',               // 引用
			'core/gallery',             // ギャラリー
			'core/list',                // リスト
			// 'core/audio',               // 音声
			// 'core/cover',               // カバー
			// 'core/file',                // ファイル
			// 'core/video',               // 動画

			// フォーマット
			// 'core/code',                // ソースコード
			// 'core/freeform',            // クラシック
			'core/html',                // カスタムHTML
			// 'core/preformatted',        // 整形済み
			// 'core/pullquote',           // プルクオート
			// 'core/table',               // テーブル
			// 'core/verse',               // 詩

			// レイアウト要素
			// 'core/button',              // ボタン
			// 'core/columns',             // カラム
			// 'core/media-text',          // メディアと文章
			// 'core/more',                // 続きを読む
			// 'core/nextpage',            // 改ページ
			// 'core/separator',           // 区切り
			// 'core/spacer',              // スペーサー

			// ウィジェット
			'core/shortcode',           // ショートコード
			// 'core/archives',            // アーカイブ
			// 'core/categories',          // カテゴリー
			// 'core/latest-comments',     // 最新のコメント
			// 'core/latest-posts',        // 最新の記事

			// 埋め込み
			// 'core/embed',               // 埋め込み
			// 'core-embed/twitter',       // Twitter
			// 'core-embed/youtube',       // YouTube
			// 'core-embed/facebook',      // Facebook
			// 'core-embed/instagram',     // Instagram
			// 'core-embed/wordpress',     // WordPress
			// 'core-embed/soundcloud',    // SoundCloud
			// 'core-embed/spotify',       // Spotify
			// 'core-embed/flickr',        // Flickr
			// 'core-embed/vimeo',         // Viemo
			// 'core-embed/animoto',       // Animoto
			// 'core-embed/cloudup',       // Cloudup
			// 'core-embed/collegehumor',  // CollegeHumor
			// 'core-embed/dailymotion',   // Dailymotion
			// 'core-embed/funnyordie',    // Funny or Die
			// 'core-embed/hulu',          // Hulu
			// 'core-embed/imgur',         // Imgur
			// 'core-embed/issuu',         // Issuu
			// 'core-embed/kickstarter',   // Kickstarter
			// 'core-embed/meetup-com',    // Meetup.com
			// 'core-embed/mixcloud',      // Mixcloud
			// 'core-embed/photobucket',   // Photobucket
			// 'core-embed/polldaddy',     // Polldaddy
			// 'core-embed/reddit',        // Reddit
			// 'core-embed/reverbnation',  // ReverbNation
			// 'core-embed/screencast',    // Screencast
			// 'core-embed/scribd',        // Scribd
			// 'core-embed/slideshare',    // Slideshare
			// 'core-embed/smugmug',       // SmugMug
			// 'core-embed/speaker-deck',  // Speaker Deck
			// 'core-embed/ted',           // TED
			// 'core-embed/tumblr',        // Tumblr
			// 'core-embed/videopress',    // VideoPress
			// 'core-embed/wordpress-tv',  // WordPress.tv

			// 再利用ブロック
			'core/block',               // 再利用ブロック
		);
		return $allowed_block_types;
	}


	/* Custom post_type */
	add_action('init', function () {
		register_post_type('goaltape-words', array(
			'label' => 'ゴールテープの文字',
			'public' => true,
			'menu_position' => 3,
			'menu_icon' => 'dashicons-buddicons-groups',
			'supports' => array('title'),
			'has_archive' => false,
			'show_in_rest' => true,
			'rewrite' => false
		));
	});
	add_action('init', function () {
		register_post_type('voices', array(
			'label' => 'お客様の声',
			'public' => true,
			'menu_position' => 5,
			// 'menu_icon' => 'dashicons-buddicons-groups',
			'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
			'has_archive' => true,
			'show_in_rest' => true,
			'taxonomies' => array('post_tag'),
			'rewrite' => array('with_front' => false)
		));
		register_taxonomy('voices-customer', 'voices', array(
			'label' => 'お客様名',
			'hierarchical' => true,
			'show_in_rest' => true,
			'show_admin_column' => true,
			'rewrite' => false
		));
	});
	add_action('init', function () {
		register_post_type('columns', array(
			'label' => 'コラム',
			'public' => true,
			'menu_position' => 5,
			// 'menu_icon' => 'dashicons-buddicons-groups',
			'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
			'has_archive' => true,
			'show_in_rest' => true,
			'taxonomies' => array('post_tag'),
			'rewrite' => array('with_front' => false)

		));
		register_taxonomy('columns-category', 'columns', array(
			'label' => 'コラムカテゴリ',
			'hierarchical' => true,
			'show_in_rest' => true,
			'show_admin_column' => true,
			'rewrite' => array('slug' => 'columns', 'with_front' => false)

		));
	});
	// default setting
	add_action('publish_columns', function ($post_ID, $post) {
		if($post->post_type != 'columns') return;
		$curTerm = wp_get_object_terms($post_ID, 'columns-category');
		if (0 != count($curTerm)) return;

		$first_term = current(get_terms( 'columns-category', array('hide_empty' => false )));
		wp_set_object_terms( $post_ID, $first_term->term_id, 'columns-category');
	}, 10, 2);
	add_action('init', function () {
		register_post_type('staff', array(
			'label' => 'スタッフ一覧',
			'public' => true,
			'menu_position' => 10,
			'menu_icon' => 'dashicons-universal-access',
			'supports' => array('title', 'editor', 'excerpt', 'custom-fields'),
			'has_archive' => true,
			'show_in_rest' => true,
			'rewrite' => array('slug' => 'about-us/staff', 'with_front' => false)
		));
		register_taxonomy('teams', 'staff', array(
			'label' => 'チーム',
			'hierarchical' => true,
			'show_in_rest' => true,
			'show_admin_column' => true,
			'rewrite' => false
		));
		register_taxonomy('office', 'staff', array(
			'label' => '事業所',
			'hierarchical' => true,
			'show_in_rest' => true,
			'show_admin_column' => true,
			'rewrite' => false
		));
	});
	add_action('init', function () {
		register_post_type('faq', array(
			'label' => 'よくある質問',
			'public' => true,
			'menu_position' => 10,
			'menu_icon' => 'dashicons-welcome-learn-more',
			'has_archive' => true,
			'show_in_rest' => true,
			'rewrite' => array('with_front' => false)
		));
		register_taxonomy('faq-category', 'faq', array(
			'label' => 'FAQカテゴリ',
			'hierarchical' => true,
			'show_in_rest' => true,
			'show_admin_column' => true,
			'rewrite' => false
		));
	});
	add_action('init', function () {
		register_post_type('download', array(
			'label' => 'ダウンロードPDF',
			'description' => 'gaがが',
			'public' => true,
			'publicly_queryable' => false,
			'menu_position' => 10,
			'menu_icon' => 'dashicons-format-aside',
			'supports' => array('title'),
			'has_archive' => false,
			'show_in_rest' => true,
			'rewrite' => false
		));
	});
	/* url rewrite rule IDにしたいとき 構造が複雑な時 */
	add_action('registered_post_type', function($post_type, $post_type_object) {
		switch ($post_type) {
			case 'columns':
				add_rewrite_tag('%columns_id%', '([0-9]+)', "post_type=columns&p=");
				add_permastruct('columns', 'columns/%columns-category%/%columns_id%', []);
				break;
			case 'voices':
				add_rewrite_tag('%voices_id%', '([0-9]+)', "post_type=voices&p=");
				add_permastruct('voices', 'voices/%voices_id%', []);
				break;
			default:
				break;	
		}
	}, 10 ,2);
	add_filter('post_type_link', function($post_link, $post, $leavename, $sample ) {
		switch ($post->post_type) {
			case 'columns':
				$rewite_expected = $post->ID;
				$terms = get_the_terms( $post, 'columns-category' );
				if( empty( $terms )) {
					break;
				}
				$term = current(get_the_terms( $post, 'columns-category' ));
				$post_link = str_replace("%columns-category%", $term->slug, $post_link);

				break;
			case 'voices':
				$rewite_expected = $post->ID;
				break;
			default:
				return $post_link;
		}
		$post_link = str_replace("%{$post->post_type}_id%", $rewite_expected, $post_link);
		return $post_link;
	}, 10, 4);


	// manage_posts_custom_column で追加されていること
	add_filter('manage_voices_posts_columns', 'add_custom_columns');
	add_filter('manage_columns_posts_columns', 'add_custom_columns');
	function add_custom_columns($columns){
		$columns['thumbnail'] = 'アイキャッチ';
		return $columns;
	}
	add_filter('manage_staff_posts_columns', function ($columns){
		$columns['display'] = 'スタッフ一覧の表示';
		return $columns;
	});
	add_action('manage_staff_posts_custom_column', function($column_name, $post_id){
		if('display' == $column_name){
			$staff_metas = get_post_meta($post_id);
		  if($staff_metas['visible_in_list'][0] == 0) {
				echo 'なし';
			} else {
				echo 'あり';
			}
		}
	}, 10, 2);


	/* custom field */
	add_action( 'admin_menu', function () {
			add_meta_box( 'writer-staff', '書いた人', 'wirter_field_callback', 'post', 'side', 'default',
				array( '__block_editor_compatible_meta_box' => true, )
			);
			add_meta_box( 'writer-staff', '書いた人', 'wirter_field_callback', 'voices', 'side', 'default',
				array( '__block_editor_compatible_meta_box' => true, )
			);
			add_meta_box( 'writer-staff', '書いた人', 'wirter_field_callback', 'columns', 'side', 'default',
				array( '__block_editor_compatible_meta_box' => true, )
			);
			add_meta_box( 'page-en-title', '英語タイトル', 'page_en_title_callback', 'page', 'advanced', 'core',
				array( '__block_editor_compatible_meta_box' => true, )
			);
	});
	function wirter_field_callback() {
		global $post_id;
		wp_nonce_field(wp_create_nonce(__FILE__), 'nonce_writer_staff_id');
		$args = array(
			'post_type' => 'staff',
			'posts_per_page' => -1
		);
		$staffs = get_posts($args);
		$writer_id = get_post_meta($post_id, '_udkya_writer_id', true);
?>
		<select name="_udkya_writer_id">
			<option value="">選択してください</option>
<?php foreach($staffs as $staff) : ?>
<?php	if( $writer_id == $staff->ID ) : ?>
			<option value="<?= $staff->ID ?>" selected><?= $staff->post_title ?></option>
<?php else : ?>
			<option value="<?= $staff->ID ?>"><?= $staff->post_title ?></option>
<?php endif; ?>
<?php endforeach; ?>
		</select>
		<p><small>スタッフに登録すると追加されます</small></p>

<?php
	}
	function page_en_title_callback() {
		global $post_id;
		wp_nonce_field(wp_create_nonce(__FILE__), 'nonce_writer_staff_id');

		$en_title = get_post_meta($post_id, '_udkya_page_en_title', true);
?>
		<input class="large-text" type="text" name="_udkya_page_en_title" value="<?= $en_title ?>">
		<p><small>フラッグの下の英字がスラッグと違う時は入力</small></p>

<?php
	}

	add_action( 'save_post', 'save_post_callback', 10, 3 );
	function save_post_callback() {
		global $post_id;

		$nonce = isset($_POST['nonce_writer_staff_id']) ? $_POST['nonce_writer_staff_id'] : null;
		$nonce |= isset($_POST['nonce_writer_staff_id']) ? $_POST['nonce_writer_staff_id'] : null;
		if(!wp_verify_nonce($nonce, wp_create_nonce(__FILE__))){
			return $post_id;
		}
		if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){
			return $post_id;
		}
		if(!current_user_can('edit_post', $post_id)){
			return $post_id;
		}

		if($_POST['_udkya_writer_id'] != '') {
			update_post_meta( $post_id, '_udkya_writer_id', $_POST['_udkya_writer_id']);
		} else {
			delete_post_meta( $post_id, '_udkya_writer_id' );
		}
		if($_POST['_udkya_page_en_title'] != '') {
			update_post_meta( $post_id, '_udkya_page_en_title', $_POST['_udkya_page_en_title']);
		} else {
			delete_post_meta( $post_id, '_udkya_page_en_title' );
		}
	}


