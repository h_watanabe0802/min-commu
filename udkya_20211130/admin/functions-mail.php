<?php
/*
*	File Name  ：functions-mail.php
*	Author     ：Tsukasa Chinen
*	Last Update：2019.09.28
*	Copyright Tsukasa Chinen

*/
/* ------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------ */
define('MAILMENU_ID','mail_menu'); 

  /* phpmailer */
  // add_action('phpmailer_init', 'oky_send_smtp_email');
  // function oky_send_smtp_email($phpmailer){
  //   $phpmailer->isSMTP();
  //   $phpmailer->Host = get_option('smtp_host');
  //   $phpmailer->Port = get_option('smtp_port');
  //   $phpmailer->SMTPSecure = "tls";
  //   $phpmailer->SMTPAuth = true;
  //   $phpmailer->Username = get_option('smtpmailaddress');
  //   $phpmailer->Password = get_option('smtpmailpassword');
  //   $phpmailer->SMTPOptions = array('ssl' => array('verify_peer' => false,'verify_peer_name' => false,'allow_self_signed' => true));
  // }
  add_action('admin_menu', 'add_admin_mail_menu');
  function add_admin_mail_menu() {
    add_menu_page( 'お問い合わせメール設定', 'お問い合わせメール設定', 'edit_theme_options', MAILMENU_ID, 'mail_menu_setting_page', '', 20 );
  }
	// ページの中身のHTML
	function mail_menu_setting_page() {
    $menu_id = MAILMENU_ID;
		?>
		<div id="top" class="wrap">
				<h1>メール設定</h1>
				<p></p>
					<?php
					// add_menu_page()で追加している場合
					// options-head.phpが読み込まれずメッセージが出ないため
					// メッセージが出るように読み込む。
						global $parent_file;
						if ( $parent_file != 'options-general.php' ) {
								require(ABSPATH . 'wp-admin/options-head.php');
              }
					?>
					<a href="#context_1">お問い合わせメール内容設定</a>
					<a href="#context_2">資料ダウンロードメール設定</a>

					<form method="post" action="options.php" novalidate="novalidate" >
					<?php
              // 隠しフィールドなどを出力します(register_setting()の$option_groupと同じものを指定)。
							settings_fields( $menu_id );
							// 入力項目を出力します(設定ページのslugを指定)。
							do_settings_sections( $menu_id );
							// 送信ボタンを出力します。
							submit_button();
					?>
					</form>
			</div>
		<?php
	}

  /* option fieldの追加 */
  add_filter('admin_init', 'oky_add_option_field_sendmail');
	function oky_add_option_field_sendmail(){
    $menu_id = MAILMENU_ID;

    add_settings_section(
			'mail_context_section_1',
      '<h3 id="context_1">■ お問い合わせメール設定</h3>',
			'mail_setting_section_callback_function',
			$menu_id
    );
    
    // // add_settings_section(
		// // 	'mail_section',
		// 	'<h3 id="setting">■ 送信メールSMTP設定</h3>',
		// 	'',
		// 	$menu_id
		// );

    // mail_section
    // $settings = [ 
    //   array(
    //     'id'      => 'smtpmailaddress',
    //     'title'   => '送信用メールアドレス', 
    //     'callback'=> 'coco_display_text',
		// 		'arg'     => array(
    //                   'type'  => 'email',
    //                   'exp'   => '送信用のアドレスとして使用するメールアドレスです.')
    //     )
    //   ,array(
    //     'id'      => 'smtpmailpassword',
    //     'title'   => '送信用メールパスワード',
    //     'callback'=> 'coco_display_text',
		// 		'arg'     => array(
    //                   'type'  => 'password',
    //                   'exp'   => '送信に使うメールアドレスのログインパスワードです。メール送信にのみ使用します。')
    //     )
    //   ,array(
    //     'id'      => 'smtp_host',
    //     'title'   => 'SMTPホストサーバ', 
    //     'callback'=> 'coco_display_text',
    //     'arg'     => array(
    //                   'exp'   => 'stmp.example.com')
    //     )
    //   ,array(
    //     'id'      => 'smtp_port',
    //     'title'   => 'メール送信ポート',
    //     'callback'=> 'coco_display_text',
    //     'arg'     => array(
    //                   'exp'   => '587')
    //     )
		// ];
		// foreach ($settings as $set) {
		// 	register_setting($menu_id, $set['id'] );
		// 	add_settings_field(
    //     $set['id'],
    //     $set['title'],
    //     $set['callback'],
    //     $menu_id,
    //     'mail_section',
    //     array_merge(array( 'label_for' => $set['id'] ),$set['arg'])
    //   );
    // }

    // mail_context_section
    $settings = [
      array(
        'id'      => 'contacter_mail_subject_1',
        'title'   => '問い合わせした方へのメールタイトル',
        'callback'=> 'coco_display_text',
        'arg'     => array(
                      'class' => 'regular-text',
                      'exp'   => '')
      )
      ,array(
        'id'      => 'contacter_mail_header_1',
        'title'   => '問い合わせした方へのメールヘッダ',
        'callback'=> 'coco_display_textarea',
        'arg'     => array(
                      'class' => 'large-text',
                      'rows'  => 10,
                      'exp'   => '間にフォーム入力内容の写しが入ります')
      )
      ,array(
        'id'      => 'contacter_mail_footer_1',
        'title'   => '問い合わせした方へのメールフッタ',
        'callback'=> 'coco_display_textarea',
        'arg'     => array(
                      'class' => 'large-text',
                      'rows'  => 10,
                      'exp'   => '')
      )
      ,array(
        'id'      => 'reply_to_1',
        'title'   => '返信先のメールアドレス',
        'callback'=> 'coco_display_text',
        'arg'     => array(
                      'exp'   => '問合せた方へのメールで、返信ボタンを押した時のアドレスを変えられます。1つだけ入力してください。空欄なら送信時のメールになります')
      )
      ,array(
        'id'      => 'to_mails_1',
        'title'   => 'お問い合わせ受信用メールアドレス',
        'callback'=> 'coco_display_text',
        'arg'     => array(
                      'exp'   => 'カンマ(,)区切りで複数指定できます。変更したら必ずテストしてください')
      )
      // ,array(
      //   'id'      => 'fromname_1',
      //   'title'   => '送信時From名',
      //   'callback'=> 'coco_display_text',
      //   'arg'     => array(
      //                 'exp'   => 'メールの送信のお名前です.')
      // )
      ,array(
        'id'      => 'owner_mail_subject_1',
        'title'   => '確認メールタイトル',
        'callback'=> 'coco_display_text',
        'arg'     => array(
                      'class' => 'large-text',
                      'exp'   => '')
      )
      ,array(
        'id'      => 'owner_mail_body_1',
        'title'   => '確認メールヘッダ',
        'callback'=> 'coco_display_textarea',
        'arg'     => array(
                      'class' => 'large-text',
                      'rows'  => 10,
                      'exp'   => 'この文の下に、フォーム入力内容の記録が入ります')
      )
    ];
    foreach ($settings as $set) {
      register_setting($menu_id, $set['id'] );
      add_settings_field(
        $set['id'],
        $set['title'],
        $set['callback'],
        $menu_id, 
        'mail_context_section_1',
        array_merge(array( 'label_for' => $set['id'] ), $set['arg'])
      );
    }

    add_settings_section(
			'mail_context_section_2',
      '<h3 id="context_2">■ 資料ダウンロードメール設定</h3>',
			'',
			$menu_id
    );

    $settings = [
      array(
        'id'      => 'to_mails_2',
        'title'   => 'お問い合わせ受信用メールアドレス',
        'callback'=> 'coco_display_text',
        'arg'     => array(
                      'exp'   => 'カンマ(,)区切りで複数指定できます。変更したら必ずテストしてください')
      )
      ,array(
        'id'      => 'owner_mail_subject_2',
        'title'   => '確認メールタイトル',
        'callback'=> 'coco_display_text',
        'arg'     => array(
                      'class' => 'large-text',
                      'exp'   => '')
      )
      ,array(
        'id'      => 'owner_mail_body_2',
        'title'   => '確認メールヘッダ',
        'callback'=> 'coco_display_textarea',
        'arg'     => array(
                      'class' => 'large-text',
                      'rows'  => 10,
                      'exp'   => 'この文の下に、フォーム入力内容の記録が入ります')
      )
    ];
    foreach ($settings as $set) {
      register_setting($menu_id, $set['id'] );
      add_settings_field(
        $set['id'],
        $set['title'],
        $set['callback'],
        $menu_id, 
        'mail_context_section_2',
        array_merge(array( 'label_for' => $set['id'] ), $set['arg'])
      );
    }
  }
  function mail_setting_section_callback_function( $arg ) {
    // セクションの紹介文を出力
    echo '<p>使える文字 [contact_company]：お客様法人名、[contact_name]：お名前、[contact_email]：アドレス</p>';
    echo '<p> [full_address]：会社住所、[tel]：会社電話番号、[url]：ＨＰアドレス、[site_name]：サイトの名前</p>';

  }


class CoCoMail_Contact {
  private $contact_name;
  private $contact_email;
  private $contact_company;
	  private $contact_division;
  private $body;
  // private $from_email; plugin を利用
  // private $from_header; plugin を利用

  function __construct() {
    $this->contact_name  = isset($_POST['inquiry-yourname']) ? $_POST['inquiry-yourname'] : null;
    $this->contact_email = isset($_POST['inquiry-email']) ? $_POST['inquiry-email'] : null;
    $contact_tel         = isset($_POST['inquiry-phone']) ? $_POST['inquiry-phone'] : null;
    $inquiry_matter      = isset($_POST['inquiry-matter']) ? $_POST['inquiry-matter'] : null;
    $this->contact_company = isset($_POST['inquiry-company']) ? $_POST['inquiry-company'] : null;
     $this->contact_division      = isset($_POST['inquiry-division']) ? $_POST['inquiry-division'] : null;
    $inquiry_zip         = isset($_POST['inquiry-zip']) ? $_POST['inquiry-zip'] : null;
    $inquiry_address     = isset($_POST['inquiry-address']) ? $_POST['inquiry-address'] : null;
      $inquiry_opportunity      = isset($_POST['inquiry-opportunity']) ? $_POST['inquiry-opportunity'] : null;
      $inquiry_opportunitytxt      = isset($_POST['inquiry-opportunitytxt']) ? $_POST['inquiry-opportunitytxt'] : null;
    $inquiry_purpose     = isset($_POST['inquiry-purpose']) ? $_POST['inquiry-purpose'] : null;
    $inquiry_meeting     = isset($_POST['inquiry-meeting']) ? $_POST['inquiry-meeting'] : null;
    $inquiry_meeting_how  = isset($_POST['inquiry-meeting-how']) ? explode(',', $_POST['inquiry-meeting-how']) : null;
    $inquiry_meeting_tool = isset($_POST['inquiry-meeting-tool']) ? $_POST['inquiry-meeting-tool'] : null;
    $inquiry_meeting_date = isset($_POST['inquiry-meeting-date']) ? $_POST['inquiry-meeting-date'] : null;

    $body  = '-----------------------------------------------------------------------------'."\n";
    $body .= '■お問い合わせ案件 : ';
    $body .= wp_strip_all_tags($inquiry_matter) . "\n\n";
    $body .= '■法人・団体名 : ';
    $body .= wp_strip_all_tags($this->contact_company) . "\n\n";
    $body .= '■所属部署 : ';
    $body .= wp_strip_all_tags($this->contact_division) . "\n\n";  
    $body .= '■お名前 : ';
    $body .= wp_strip_all_tags($this->contact_name) . "\n\n";
    $body .= '■電話番号 : ';
    $body .= wp_strip_all_tags($contact_tel) . "\n\n";
    $body .= '■メールアドレス : ';
    $body .= wp_strip_all_tags($this->contact_email) . "\n\n";
    $body .= '■住所 : ';
    $body .= wp_strip_all_tags($inquiry_zip) . "\n";
    $body .= wp_strip_all_tags($inquiry_address) . "\n\n";
    $body .= '■運動会屋を知ったきっかけ : ';
    $body .= wp_strip_all_tags($inquiry_opportunity) . "\n\n";     
    $body .= wp_strip_all_tags($inquiry_opportunitytxt) . "\n\n";         
    $body .= '■運動会の目的、規模、予算など : ';
    $body .= wp_strip_all_tags($inquiry_purpose) . "\n\n";
    $body .= '■お打ち合わせ: ';
    $body .= ($inquiry_meeting ? '希望する' : '希望しない') . "\n\n";
    $body .= '■お打ち合わせ方法のご希望 : ' . "\n";
    $inquiryMeetingHow = inquiryMeetingHow();
    foreach ($inquiry_meeting_how as $val) {
      if($val != 'other') {
        $body .= '・' . $inquiryMeetingHow[$val] . "\n";
      } else {
        $body .= '・' . $inquiryMeetingHow[$val].'（'.$inquiry_meeting_tool.'）' . "\n";
      }
    }
    $body .= "\n";
    $body .= '■お打ち合わせ希望日時 : ';
    $body .= wp_strip_all_tags($inquiry_meeting_date) . "\n\n";
    $body .= "\n";
    $body .= '-----------------------------------------------------------------------------'."\n\n";
    $this->body = $body;
  }

	public function send_mail(){
    $sendAdmin = $this->send_owner();
		$sendUser = $this->send_customer();

    $result = '';
		if($sendAdmin && $sendUser){
			$result = 0;
		}elseif(!$sendAdmin && !$sendUser){
			$result = 1;
		}else{
			if(!$sendAdmin){
				$result = 2;
			}elseif(!$sendUser){
				$result = 3;
			}			
    }
		return $result;
  }

  public function send_customer() {
    $to        = $this->contact_email;

    $header_text  = $this->exchange_keywords(get_option('contacter_mail_header_1'));
    $footer_text  = $this->exchange_keywords(get_option('contacter_mail_footer_1'));
    $subject = $this->exchange_keywords(get_option('contacter_mail_subject_1'));

    $message    = "\n";
    $message   .= $header_text."\n\n";
    $message   .= "\n";
    $message   .= $this->body;
    $message   .= "\n\n";
    $message   .= $footer_text;

    // $headers[] = $this->from_header();
    if(!empty(get_option('reply_to_1'))) {
      $headers[] = $this->reply_header(get_option('reply_to_1'));
    }
    
    return wp_mail($to, $subject, $message, $headers);
  }

  public function send_owner() {
    $to_owners_mails = explode(',',str_replace(' ', '' ,(get_option('to_mails_1'))));
    $header_text  = $this->exchange_keywords(get_option('owner_mail_body_1'));
    $subject = $this->exchange_keywords(get_option('owner_mail_subject_1'));

    $message  = "\n";
    $message .= $header_text."\n\n";
    $message .= "\n";
    $message .= $this->body;
    $message .= "\n\n";
		$message .= '※このメールはサーバー上に保存されませんので大切に保管しておいてください。'."\n\n";
    $message .= 'なお、本メールの返信先は、お客様のアドレスとなっております。'."\n";
    $message .= 'HP '.home_url() ."\n";

    $headers[] = $this->reply_header($this->contact_email);

	  return wp_mail($to_owners_mails, $subject, $message, $headers);
  }

  private function exchange_keywords($target) {
    $replace_array =  array(
      'contact_name'  => $this->contact_name,
      'contact_email' => $this->contact_email,
      'contact_company' => $this->contact_company,
        'contact_division' => $this->contact_division,
      'full_address'  => full_address(),
      'tel'           => get_option('tel'),
      'url'           => home_url(),
      'site_name'     => get_bloginfo('name'),
    );
    foreach ($replace_array as $search => $replace) {
      $target = str_replace('['.$search.']', $replace, $target);
    }
    return $target;
  }
  private function from_header($from_name, $from_email) {
    return "From: ".$from_name." <".$from_email.">";
  }
  private function reply_header($reply_email) {
    return 'Reply-To: '.$reply_email;
  }
}


class CoCoMail_Download {
  private $contact_name;
  private $contact_email;
  private $contact_company;
  private $body;
  // private $from_email; plugin を利用
  // private $from_header; plugin を利用

  function __construct() {
    $this->contact_name  = isset($_POST['values']['yourname']) ? $_POST['values']['yourname'] : null;
    $this->contact_email = isset($_POST['values']['email']) ? $_POST['values']['email'] : null;
    $contact_tel         = isset($_POST['values']['phone']) ? $_POST['values']['phone'] : null;
    $this->contact_company = isset($_POST['values']['company']) ? $_POST['values']['company'] : null;


    $body  = '-----------------------------------------------------------------------------'."\n";
    $body .= '■法人・団体名 : ';
    $body .= wp_strip_all_tags($this->contact_company) . "\n\n";
    $body .= '■お名前 : ';
    $body .= wp_strip_all_tags($this->contact_name) . "\n\n";
    $body .= '■電話番号 : ';
    $body .= wp_strip_all_tags($contact_tel) . "\n\n";
    $body .= '■メールアドレス : ';
    $body .= wp_strip_all_tags($this->contact_email) . "\n\n";
    $body .= '-----------------------------------------------------------------------------'."\n\n";
    $this->body = $body;
  }

  public function send_owner() {
    $to_owners_mails = explode(',',str_replace(' ', '' ,(get_option('to_mails_2'))));
    $subject = $this->exchange_keywords(get_option('owner_mail_subject_2'));

    $header_text  = $this->exchange_keywords(get_option('owner_mail_body_2'));
    $message  = "\n";
    $message .= $header_text."\n\n";
    $message .= "\n";
    $message .= $this->body;
    $message .= "\n\n";
		$message .= '※このメールはサーバー上に保存されませんので大切に保管しておいてください。'."\n\n";
    $message .= 'なお、本メールの返信先は、お客様のアドレスとなっております。'."\n";
    $message .= 'HP '.home_url() ."\n";

    $headers[] = $this->reply_header($this->contact_email);

	  return wp_mail($to_owners_mails, $subject, $message, $headers);
  }

  private function exchange_keywords($target) {
    $replace_array =  array(
      'contact_name'  => $this->contact_name,
      'contact_email' => $this->contact_email,
      'contact_company' => $this->contact_company,
        'contact_division' => $this->contact_division,
      'full_address'  => full_address(),
      'tel'           => get_option('tel'),
      'url'           => home_url(),
      'site_name'     => get_bloginfo('name'),
    );
    foreach ($replace_array as $search => $replace) {
      $target = str_replace('['.$search.']', $replace, $target);
    }
    return $target;
  }
  private function from_header($from_name, $from_email) {
    return "From: ".$from_name." <".$from_email.">";
  }
  private function reply_header($reply_email) {
    return 'Reply-To: '.$reply_email;
  }
}
