  $(function($){
    const $select = $('.media_select');
    const $clear = $('.media_clear');
    const url_class = $select.parents('td').find('.media_url');
    const preview_class = $select.parents('td').find('img.media_image');
    var uploader;

    $select.on('click',function(e){
      $selected = $(this);
      e.preventDefault()
      if(uploader) {
        uploader.open();
        return;
      }
      uploader = wp.media({

        title : 'Select Image',

        button: {
          text: 'Selecct Image'
        },

        multiple: false
      });
      uploader.open();

      uploader.on('select',function(){
      const imagas = uploader.state().get('selection');
      imagas.each(function(image){
        const url1 = image.attributes.url;
        $selected.parent().find(url_class).val(url1);
        $selected.parent().find(preview_class).attr('src',url1);
      });
      uploader.close()
    });
  });

  $clear.on('click',function(){
    $(this).parent().find(url_class).val('');
    $(this).parent().find(preview_class).attr('src','')
  });

  var $catChecklist = $('.categorychecklist input[type=checkbox]');
  $catChecklist.on('click', function(){;
      $(this).parents('.categorychecklist').find('input[type=checkbox]').attr('checked', false);
      $(this).attr('checked', true);
  });

  var $quickeditCatChecklist = $('.cat-checklist input[type=checkbox]');
  $quickeditCatChecklist.on('click', function(){;
    $(this).parents('.cat-checklist').find('input[type=checkbox]').attr('checked', false);
    $(this).attr('checked', true);
  });
});