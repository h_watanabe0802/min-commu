<?php get_header(); ?>
<?php get_template_part( 'partial/content', 'title' ); ?>

          <div class="faq">
            <div class="faq__image"><img src="<?= get_template_directory_uri() ?>/src/img/faq/faq-rolling-ball.png" alt="玉転がしの画像"></div>
<?php
  $terms = get_terms('faq-category');
  $for_first = ' -active'; // 初めのsectionだけ追加する
  foreach( $terms as $term ) :
    $args = array(
        'post_type' => 'faq',
        'tax_query' => array(
            array(
                'taxonomy' => 'faq-category',
                'field' => 'id',
                'terms' => $term->term_id
            )
        )
    );
    $query = new WP_Query($args);      
?>
            <section class="faq__section release-background js-faq-accordion-box<?= $for_first ?>" id="js-faq-id-<?= $term->term_id ?>"><a class="faq__section-title flex-middle-left js-faq-accordion-trigger" href="/">
                <h3 class="faq__section-title-text"><?= $term->name ?></h3><svg class="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 18 18" style="enable-background:new 0 0 18 18;" xml:space="preserve"><path class="st0" d="M9,14.1c-0.3,0-0.6-0.1-0.9-0.4L0.4,6c-0.5-0.5-0.5-1.3,0-1.7s1.3-0.5,1.7,0L9,11.1l6.9-6.9 c0.5-0.5,1.3-0.5,1.7,0c0.5,0.5,0.5,1.3,0,1.7l-7.8,7.8C9.6,14,9.3,14.1,9,14.1z"/></svg></a>
              <!-- /.faq__section-title-->
              <div class="faq__section-inner js-faq-accordion-inner">
                <div class="faq__section-content">
                  <div class="faq__accordion accordion">
<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
                    <div class="faq__accordion-box lazyload">
                      <div class="faq__question">
                        <dl class="flex-top">
                          <dt>Q</dt>
                          <dd><?php the_title() ?></dd>
                        </dl>
                      </div>
                      <div class="faq__answer">
                        <dl class="flex-top">
                          <dt>A</dt>
                          <dd><?php the_content() ?></dd>
                        </dl>
                      </div>
                    </div>
<?php endwhile; endif;?>
                  </div>
                </div>
                <!-- /.faq__section-content-->
              </div>
            </section>
            <!-- /.faq__section-->
<?php
  $for_first = ''; // 最初のsectionだけ必要なのでここで空白を代入
  endforeach; ?>
            </div>


<?php get_footer(); ?>
