<?php get_header(); ?>
<?php get_template_part( 'partial/content', 'title' ); ?>

<?php $teams = get_categories( array( 'taxonomy' => 'teams', 'hide_empty' => false ) ); ?>
          <div class="tab">
            <ul>
<?php for($i=0;$i<count($teams);$i++) : ?>
              <li><a href="#teams<?= $i ?>"><?= $teams[$i]->name ?></a></li>
<?php if($i % 3 == 2 && $i != count($teams) ) : ?>
            </ul>
            <ul>
<?php endif; ?>
<?php endfor; ?>
            </ul>
          </div>
          <!-- /.tab-->
<?php for($i=0;$i<count($teams);$i++) :
  $staffs = get_posts( array(
    'post_type' => 'staff',
    'posts_per_page' => -1,
    'tax_query' => array(
        array(
          'taxonomy' => 'teams',
          'field' => 'id',
          'terms' => $teams[$i]->term_id
    ))
  ));
?>
          <section class="department" id="teams<?= $i ?>">
            <div class="section__header">
              <h3 class="section__header-text"><?= $teams[$i]->name ?></h3>
              <p class="section__header-lead"><?= $teams[$i]->description ?></p>
            </div>
            <!-- /.section__header-->
            <div class="department__content">
<?php foreach($staffs as $staff) :
  $office = current(get_the_terms( $staff, 'office' ));
  $staff_metas = get_post_meta($staff->ID);
  if($staff_metas['visible_in_list'][0] == 0) {
    continue;
  }
  $lazyload = 'class="lazyload" data-';
?>
              <a class="department__person" href="<?= get_the_permalink( $staff ) ?>">
                <div class="department__person-cloud">
                  <p><?= mb_substr_excerpt($staff_metas['words-for-customer'][0], 26) ?></p>
                </div>
                <div class="department__person-image" style="background-image: url(<?= wp_get_attachment_image_url( $staff_metas['profile-image-sub'][0], 'medium') ?>)"><img <?=$lazyload?>src="<?= wp_get_attachment_image_url( $staff_metas['profile-image-main'][0], 'medium' ) ?>" alt="<?= $staff_metas['display_name'][0] ?>の顔写真"></div>
                <div class="department__person-text">
                  <div class="department__person-catchcopy">
                    <p><?= $staff_metas['catch-copy'][0] ?></p>
                  </div>
                  <div class="department__person-name">
                    <p><?= $staff_metas['display_name'][0] ?></p>
                  </div>
                  <div class="department__person-belong">
                    <dl>
                      <dt><?= $teams[$i]->name ?></dt>
                      <dd><?= $office->name ?></dd>
                    </dl>
                  </div>
                </div></a>
<?php endforeach; ?>
            </div>
          </section>
          <!-- /.department-->
<?php endfor; ?>
<?php get_footer(); ?>
