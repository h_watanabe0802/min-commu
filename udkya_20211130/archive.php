<?php get_header(); ?>
<?php get_template_part( 'partial/content', 'title' ); ?>
<?php if($post->post_type != 'voices') : ?>
<?php
  $taxonomy = get_taxonomy_slug($post->post_type);
?>
          <div class="list-tab tab">
            <ul>
              <li><a href="<?= get_post_type_archive_link( $post->post_type ) ?>">All</a></li>
              <?= get_all_terms_list( $taxonomy, true ) ?>
            </ul>
          </div>
<?php endif; ?>
<?php get_template_part( 'partial/list', 'posts' ); ?>

<?php get_footer(); ?>
