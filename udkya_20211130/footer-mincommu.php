<footer class="min_footer">
	<div class="min_footer__innar">
		<img class="chara" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/mr_m_08.png" alt="chara">
		<img class="mr_m09" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/mr_m_09.svg" alt="イベントは楽しくなくちゃ">
		<img class="logo_h1" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/logo_h1.svg" alt="みんコミュ">
		<ul class="sns">
			<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/f.svg" alt="facebook"></a></li>
			<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/TWITTER.svg" alt="twitter"></a></li>
			<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/ig.svg" alt="instagram"></a></li>
			<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/youtube.svg" alt="youtube"></a></li>
			<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/LINE.svg" alt="line"></a></li>
		</ul>
		<ul class="f_link">
			<li><a href="">運営会社</a></li>
			<li><a href="">個人情報保護方針</a></li>
			<li><a href="">サイトマップ</a></li>
		</ul>
		<p class="copy">
			© 2021 みんコミュ All Rights Reserved.
		</p>
	</div>
</footer>
<?php wp_footer(); ?>
</body>

</html>