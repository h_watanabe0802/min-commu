<?php if( is_single() ) : ?>
        </article>
<?php else : ?>
        </main>
<?php endif; ?>
      </div>
      <footer class="footer release-background" id="js-footer">
        <div class="footer__inner">
          <div class="nav">
<?php get_template_part( 'partial/navigation', 'head' ); ?>

            <div class="nav__relation">

              <ul class="nav__relation-list">

				  				    <li><a href="https://rescue.udkya.com/" target="_blank" rel="noopener"><span>運動会レスキュー隊</span></a></li>
				             <li><a href="https://neo.udkya.com/" target="_blank" rel="noopener"><span>NEO運動会のススメ</span></a></li>
                        <li><a href="https://online.udkya.com/" target="_blank" rel="noopener"><span>オンライン運動会</span></a></li>
                        <li><a href="https://www.udkya.com/online/en" target="_blank" rel="noopener"><span>Online UNDOKAI</span></a></li>

				                </ul>
							  </div>
			              <div class="nav__relation">
				              <ul class="nav__relation-list">
								  			          <li><a href="https://www.min-commu.net/" target="_blank" rel="noopener"><span>みんコミュ</span></a></li>
			          <li><a href="https://www.min-commu.net/gurulink/" target="_blank" rel="noopener"><span>ぐるりんく</span></a></li>
                        <li><a href="https://runner.udkya.com/" target="_blank" rel="noopener"><span>ランナー体操</span></a></li>
                        <li><a href="https://sugoroku.udkya.com/" target="_blank" rel="noopener"><span>運動会屋すごろく</span></a></li>
					
              </ul>
			  </div>
            <div class="nav__sport flex-middle-center"><a href="https://sportinlife.go.jp"><img class="nav__sport-image" src="<?= get_template_directory_uri() ?>/src/img/common/icon-sport.svg" alt="Sport in Lifeアイコン"><span class="nav__sport-text">Sport in Life</span></a></div>

            <!-- /.nav__sport-->
<?php get_template_part( 'partial/navigation', 'sns' ); ?>
            <div class="nav__info">
                       <p class="nav__info-sitepolicy"><a href="/site-policy">サイトポリシー</a></p>
				<a class="nav__info-logo" href="<?= home_url() ?>"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/logo-small.svg" alt="[object Object]ロゴ"></a>
              <p class="nav__info-copyright"><small>Copyright &copy; <a href="https://www.undokai.co.jp/">運動会屋</a> All Rights Reserved.</small></p>
            </div>
          </div>
          <!-- /.nav-->
        </div>
      </footer>
      <!-- /.footer-->
      <div class="gotop" id="js-gotop">
        <button class="gotop__button"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/go-top.png" alt="TOPに戻る"></button>
      </div>
      <!-- /.gotop-->
<?php $contact_page_link = get_page_link( get_page_by_title( 'お問い合わせ' )) ?>
      <div class="inquiry">
        <div class="inquiry__inner">
          <div class="inquiry__content -mobile flex-middle">
            <div class="inquiry__person"><a href="https://online-udk.ovice.in/lobby/enter/">
                <picture>
                  <source media="(min-width: 1280px)" data-srcset="<?= get_template_directory_uri() ?>/src/img/common/banner-inquiry-psrson-medium2.png?Ver=1.2" alt="クイックでoVice体験"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/banner-inquiry-psrson-small2.png?Ver=1.2" alt="クイックでoVice体験">
                </picture></a></div>
            <div class="inquiry__list-small flex-middle"><a class="flex-middle inquiry__form" aria-label="お問い合わせページへ" href="<?= $contact_page_link ?>">
                <dl>
                  <dt class="flex-middle-center"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/icon-mail-black.svg" alt="メールアイコン"></dt>
                  <dd>フォーム</dd>
                </dl></a><a class="flex-middle inquiry__tel" aria-label="お電話で連絡" href="tel:<?= get_option('tel') ?>">
                <dl>
                  <dt class="flex-middle-center"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/icon-phone.svg" alt="電話アイコン"></dt>
                  <dd>お電話</dd>
                </dl></a><a class="flex-middle inquiry__line" aria-label="LINEアプリで連絡" href="https://lin.ee/beE9ebF/775ldykn">
                <dl>
                  <dt class="flex-middle-center"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/icon-line-color.svg" alt="LINE アイコン"></dt>
                  <dd>LINE</dd>
                </dl></a></div>
            <div class="inquiry__list-medium"><a class="inquiry__form" href="<?= $contact_page_link ?>">
                <dl>
                  <dd><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/inquiryform-black.svg" alt="お問い合わせフォーム"></dd>
                </dl></a><a class="inquiry__tel" href="tel:<?= get_option('tel')?>">
                <dl>
                  <dd><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/freedial-black.svg" alt="フリーダイヤル <?= get_option('tel')?>"></dd>
                </dl></a></div>
          </div>
        </div>
      </div>
      <!-- /.inquiry-->
    </div>
    <!-- /.wrapper-->
<?php wp_footer(); ?>
<?php if(is_front_page() || is_single()) : ?>
  <script>
      $(function(){
      	/* Carousel */
      	const $carouselEle = $('#js-swiper-carousel')
      	const $carouselParinationEle = $('#js-swiper-carousel-pagination')
      	if($carouselEle !== null){
      		let $carousel = new Swiper($carouselEle, {
      			speed: 1000,
      			slidesPerView: 1,
      			spaceBetween: 14,
      			loop: true,
      			pagination: {
      				el: $carouselParinationEle,
      				clickable: true,
      			},
      			breakpoints: {
      				600: {
      					slidesPerView: 2,
      					spaceBetween: 14,
      				},
      				1024: {
      					slidesPerView: 2,
      					spaceBetween: 40,
      				},
      				1280: {
      					slidesPerView: 2,
      					spaceBetween: 120,
      				}
      			},
      			autoplay: {
      				delay: 5000,
      			},
      		})
      	}
      })
    </script>
<?php elseif(is_post_type_archive( 'faq' )) : ?>
    <script>
      $(function(){
      	if(location.hash){
      		$('.js-faq-accordion-box').removeClass('-active')
      		const $hash = location.hash.replace(/#/g, '')
      		const $hashFaqBoxEle = $('.js-faq-accordion-box#js-faq-id-' + $hash)
      
      		$hashFaqBoxEle.addClass('-active')
      		let $hashFaqBoxPaddingTop = parseInt($hashFaqBoxEle.css('padding-top'))
      		let $hashFaqBoxScrollOffset
      
      		if($windowWidth >= $tabletMedium && $windowWidth < $pc){
      			$hashFaqBoxScrollOffset = parseInt($hashFaqBoxEle.offset().top - $hashFaqBoxPaddingTop)
      		}else if($windowWidth >= $tabletMedium && $windowWidth >= $pc){
      			$hashFaqBoxScrollOffset = parseInt($hashFaqBoxEle.offset().top - ($hashFaqBoxPaddingTop - 20))
      		}
      
      		$('html, body').animate({scrollTop: $hashFaqBoxScrollOffset}, 400, function(){
      			$hashFaqBoxEle.find('.js-faq-accordion-inner').slideDown()
      		})
      	}
      	$(document).on('click', '.js-faq-accordion-trigger', function(e){
      
      		e.preventDefault()
      
      		let $faqBox = $(this).parent('.js-faq-accordion-box')
      		let $faqBoxInner = $faqBox.find('.js-faq-accordion-inner')
      		let $paddingTop = parseInt($faqBox.css('padding-top'))
      		let $marginTop = parseInt($faqBox.css('margin-top'))
      		let $scrollOffset
      
      		if(!$faqBox.hasClass('-active')){
      			$faqBox.addClass('-active')
      			$faqBoxInner.slideDown()
      			if($windowWidth < $tabletMedium){
      				$scrollOffset = parseInt($faqBox.offset().top - $paddingTop)
      			}else if($windowWidth >= $tabletMedium && $windowWidth < $pc){
      				$scrollOffset = parseInt($faqBox.offset().top)
      			}else if($windowWidth >= $tabletMedium && $windowWidth >= $pc){
      				$scrollOffset = parseInt($faqBox.offset().top + $paddingTop)
      			}
      			$('html, body').animate({scrollTop: $scrollOffset})
      		}else{
      			$faqBox.removeClass('-active')
      			$faqBoxInner.slideUp()
      
      			if($windowWidth < $pc){
      				$scrollOffset = parseInt($faqBox.offset().top - ($paddingTop * 2 + $marginTop))
      			}else{
      				$scrollOffset = parseInt($faqBox.offset().top - ($paddingTop + $marginTop))
      			}
      			$('html, body').animate({scrollTop: $scrollOffset})
      		}
      	})
      })
    </script>

<?php elseif(is_page('about-us')) : ?>
  	<script>
      $(window).on('load', function() {
      	/* More Read */
      	const $textContent = $('#js-greeting-text')
      	const $textContentHeight = $textContent.outerHeight()
      	const $textContentHeightMin = $textContent.attr('data-height')
      
      	const $moreRead = $('#js-greeting-button')
      	const $moreReadButton = $moreRead.find('a')
      	const $moreReadButtonHeight = $moreReadButton.outerHeight()
      
      	$textContent.css({'height': $textContentHeightMin})
      
      	$moreReadButton.on('click', function(e){
      
      		e.preventDefault()
      
      		if(!$moreRead.hasClass('-close')){
      			$textContent.css({'height': ($textContentHeight + $moreReadButtonHeight + ($moreReadButtonHeight / 2))})
      			$moreRead.find('span').first().text('閉じる')
      			$moreRead.addClass('-close')
      		}else{
      			$textContent.css({'height': $textContentHeightMin})
      			$moreRead.find('span').first().text('続きを読む')
      			$moreRead.removeClass('-close')
      		}
      	})
      })
    </script>
<?php elseif( is_page('service-price') || is_page('service-price-confirm') ) : ?>
		<script>
      $(function(){
      	$(document).on('click', '.js-modal-button', function(e){
      		e.preventDefault()
      		let $targetModal = $(this).attr('href')
      		if($('.js-modal-box.' + $targetModal).length){
      			$('.js-modal-box.' + $targetModal).css({'display': 'block'})
      			$('.js-modal').fadeIn()
      			scrollStop()
      		}
      	})
      	$(document).on('click', '.js-modal-close', function(e){
      		e.preventDefault()
      		$('.js-modal').fadeOut('normal', function(){
      			$('.js-modal-box').css({'display': 'none'})
      			scrollReturn()
      		})
      	})
      })
    </script>
<?php
  endif;
  if(!is_user_logged_in()){
    echo get_option('tracking-before-body-end');
  }
?>
<script type='text/javascript'>
piAId = '944983';
piCId = '36850';
piHostname = 'go.udkya.com';

(function() {
	function async_load(){
		var s = document.createElement('script'); s.type = 'text/javascript';
		s.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + piHostname + '/pd.js';
		var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
	}
	if(window.attachEvent) { window.attachEvent('onload', async_load); }
	else { window.addEventListener('load', async_load, false); }
})();
</script>
  </body>
</html>
