<?php
/* ------------------------------------------------------------------------------------------------
 0 - [WP Settings]
 		0.1   -> Notice Error
		0.2   -> Remove Default Meta Tag
		0.3   -> Remove ?var=***
		0.4   -> Allow SVG File Upload
		0.5   -> Remove Meta Prefetch
 1 - [Theme Support]
 		1.1   -> Load CSS
 		1.3   -> Load IE Polifil
 		1.4   -> Set Title
 		1.5   -> Set Meta
 		1.6   -> Set OGP
 		1.7   -> Set Tracking
 		1.8   -> Custom Menu
 		1.9   -> Custom Logo
 		1.10  -> Add Original Settings
 		1.11  -> Shortcodes
 		1.12  -> Allow Shortcode
 		1.13  -> Send Email
 		1.14  -> Not Found Page
 		1.15  -> Breadcrumb
------------------------------------------------------------------------------------------------ */

	/* 管理バーを非表示 */	
	add_filter('show_admin_bar', '__return_false');

	/* 0.1 */
	error_reporting(E_ALL & ~E_NOTICE);
	require_once(get_template_directory().'/admin/admin.php');
	require_once(get_template_directory().'/admin/admin-setting.php');
	require_once(get_template_directory().'/admin/functions-mail.php');

	/* 0.2 */
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
	remove_action('wp_head', 'wp_shortlink_wp_head');
	remove_action('wp_head', 'print_emoji_detection_script',7);
	remove_action('wp_head', 'rel_canonical');
	remove_action('wp_head', 'usces_action_ogp_meta');
	remove_action('wp_head', 'rest_output_link_wp_head');
	remove_action('wp_head', 'wp_oembed_add_discovery_links');
	remove_action('wp_head', 'wp_oembed_add_host_js');
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_styles', 'print_emoji_styles');
	function dequeue_plugins_style(){
		wp_dequeue_style('wp-block-library');
		wp_dequeue_style( 'wp-block-library-theme' );
	}
	add_action( 'wp_enqueue_scripts', 'dequeue_plugins_style', 9999);
	
	add_filter('wp_resource_hints', 'coco_remove_dns_prefetch', 10, 2);
	function coco_remove_dns_prefetch($hints, $relation_type){
		if('dns-prefetch' === $relation_type){
			return array_diff(wp_dependencies_unique_hosts(), $hints);
		}
		return $hints;
	}

	/* 0.3 */
	add_filter('style_loader_src', 'coco_remove_cssjs_ver', 9999);
	add_filter('script_loader_src', 'coco_remove_cssjs_ver', 9999);
	function coco_remove_cssjs_ver($src){
		if(strpos($src,'ver='.get_bloginfo('version')))
			$src = remove_query_arg('ver', $src);
			return $src;
	}

	/* 0.4 */
	add_filter('ext2type', 'coco_ext2type');
	function coco_ext2type($ext2types){
    array_push($ext2types, array('image' => array('svg', 'svgz')));
    return $ext2types;
	}

	add_filter('upload_mimes', 'coco_mime_types');
	function coco_mime_types($mimes){
    $mimes['svg']  = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';
    return $mimes;
	}

	add_filter('getimagesize_mimes_to_exts', 'coco_mime_to_ext');
	function coco_mime_to_ext($mime_to_ext){
    $mime_to_ext['image/svg+xml'] = 'svg';
    return $mime_to_ext;
	}

	/* 0.5 */
	function coco_dump($arg){
		echo '<pre>';
		var_dump($arg);
		echo '</pre>';
	}

	/* 1.0 Set Title */
	add_action('after_setup_theme', 'coco_setup_theme_title');
	function coco_setup_theme_title(){
		add_theme_support('title-tag');
	}
	add_filter('document_title_separator', 'coco_setup_theme_title_separator');
	function coco_setup_theme_title_separator($sep){
		$sep = '|';
		return $sep;
	}

	add_filter('document_title_parts', 'coco_change_document_title_parts');
	function coco_change_document_title_parts($titleParts){

		$siteName = get_bloginfo('name');
		$titleParts['tagline'] = '';
		$titleParts['title'] = coco_title_decided_by_client();
		if(is_paged()){
			if(!empty($titleParts['page'])){
				$titleParts['page'] = '';
			}
		}
		return $titleParts;
	}

	function coco_title_decided_by_client() {
		$title = '';
		if( is_page('contact') ) {
			$title = 'お問い合わせ／資料ダウンロード';
		} elseif ( is_page('sdgs') ) {
			$title = 'SDGsについて';
		} elseif (is_singular() || is_page() ) {
			if( is_singular( 'staff' )) {
				$title = get_staff_detail_title(). '';
			}
			$title .= trim(get_the_title());
		} elseif ( is_tag() ) {
			$post_type = coco_get_post_type_for_tag_post();
			if( is_array($post_type) ) { // all
				$title = get_the_list_title();
			} elseif($post_type == 'columns') {
				$title = '運動会屋タイムズ';
			} else {
				$post_type_obj = get_post_type_object( $post_type );
				$title = $post_type_obj->label;
			}
			$title .= ' #'.single_tag_title('', false);
		} elseif ( is_archive() ) {
			global $post;
			if( $post->post_type == 'columns') {
				$title = '運動会屋タイムズ';
			} else {
				$post_type_obj = get_post_type_object( $post->post_type );
				$title = $post_type_obj->label;
			}
			if( is_tax() || is_category() ) {
				$title .= ' '.get_the_list_title();
			}
		}	elseif( is_404() ){
			$title = '404 Not Found';
		}
		if( is_paged() ) {
			global $paged;
			$title .= '（'.$paged.'ページ目）';
		}
		if(empty($title)) {
			$title =  get_bloginfo('name');
		}
		return $title;
	}

	/* 1.1 Set description */
	function coco_description() {
		$description = '';

		if(is_archive()){
			$currentPaged = get_query_var('paged');
			$currentPaged > 0 ? $pageNumber = $currentPaged : '';
		}

		if( is_front_page() ){
			$description = get_option('site_catchcopy01');
		} elseif( is_home() ){ // news list
			$description = get_option('description_archive_post');
		} elseif( is_page() || is_single() ){
			global $post;
			if(empty($post->post_excerpt)) {
				$description  = strip_tags( $post->post_content );
				$description  = str_replace('&hellip;', '', $description);
				$replaceArray = array(PHP_EOL => '', '"' => '\'', ' ' => '', '　' => '', '&nbsp;' => '', "\t" => '');
				$description  = str_replace(array_keys($replaceArray), $replaceArray, $description);
				$description  = coco_character_limit($description, 90);
				$description  = str_replace('&hellip;', '', $description);
			} else {
				$description = coco_character_limit(strip_tags($post->post_excerpt), 90);
			}
		} elseif( is_month() ) {
			$description = get_query_var('year').'年'.get_query_var('monthnum').'月のお知らせ一覧';
			$description .= ' '.get_bloginfo('name');
		} elseif( is_search() ){
			$description = '「'.$_GET['s'].'」の検索結果のページです。';
		} elseif( is_tag() ) {
			$termDescription = tag_description();
			if( !empty($termDescription) ){
				$description = strip_tags($termDescription);
				$description = str_replace(PHP_EOL, '', $description);
			} else {
				$description = coco_title_decided_by_client(). ' の一覧ページです。';
			}
		} elseif( is_archive() ) {
			$termDescription = '';
			if( is_category() ) {
				$termDescription = category_description();
			} elseif( is_tax() ) {
				global $term_id;
				global $taxonomy;
				$termDescription = term_description($term_id, $taxonomy);
			}
			if( !empty($termDescription) ){
				$description = strip_tags($termDescription);
				$description = str_replace(PHP_EOL, '', $description);
			} else {
				global $post;
				$description = get_option('description_archive_'.$post->post_type);
				if( is_category() || is_tax() ) {
					$description .= '（'. get_the_list_title(). 'の一覧）';
				}
			}
		} elseif( is_404() ) {
			$description = 'お探しのページが見つかりませんでした。';
		}
		if(isset($pageNumber)) {
			$description .= '（ページ '.$pageNumber.'）';
		}
		return $description;
	}

	add_action('wp_head', 'coco_wp_head_meta_description', 1);
	function coco_wp_head_meta_description() {
		$catDescription = coco_description();
		echo !empty($catDescription) ? '<meta name="description" content="'.$catDescription.'">'.PHP_EOL : '';
	}
	function coco_character_limit($content, $length){
		$beforeContent = strip_shortcodes($content);
		$afterContent  = strip_tags(str_replace(array(PHP_EOL, "&nbsp;", "&nbsp", "&nbs", "&nb", "&n", " ", "　"), '', $beforeContent));

		if(!empty($length)){
			$afterContent = mb_substr($afterContent, 0, $length, 'utf-8');
			if(mb_strlen($beforeContent, 'utf-8') > $length){
		  	$afterContent .= '&hellip;';
			}
		}
		return $afterContent;
	}


	/* 1.3 Set Meta Canonical */
	add_action('wp_head', 'coco_wp_head_meta_canonical', 1);
	function coco_wp_head_meta_canonical() {
		if(is_archive()){
			$canonical = '';
			$currentPaged = get_query_var('paged');

			$currentURL  = (empty($_SERVER['HTTPS'] ) ? 'http://' : 'https://').$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			if($currentPaged > 0){
				$canonical .= substr($currentURL, 0, strlen($currentURL) - 8);
				$canonical .= '/';
				echo '<link rel="canonical" href="'.$canonical.'">'.PHP_EOL;
			}
		}
	}
	
	/* 1.4 Set Author Copyright */
	add_action('wp_head', 'coco_wp_head_meta_author_copyright', 1);
	function coco_wp_head_meta_author_copyright() {
		$authorCopyright  = '<meta name="author" content="'.get_bloginfo('name').'">'.PHP_EOL;
		$authorCopyright .= '<meta name="copyright" content="'.get_bloginfo('name').'">'.PHP_EOL;
		echo $authorCopyright;
	}

	/* 1.5 Prefetch */
	add_action('wp_head', 'coco_wp_head_meta_prefetch', 4);
	function coco_wp_head_meta_prefetch(){
		$prefetch = '';
		$prefetch .= '<link rel="dns-prefetch" href="//www.googletagmanager.com">';
		if( is_front_page() ) {
			$prefetch .= '<link rel="preload" as="font" type="font/woff2" href="'.get_template_directory_uri().'/src/font/KosugiMaru-Regular.woff2" crossorigin="anonymous">'.PHP_EOL;
		}
		$prefetch .= '<link rel="preload" as="font" type="font/woff2" href="'.get_template_directory_uri().'/src/font/Makinas-4-Square.woff2" crossorigin="anonymous">'.PHP_EOL;
		$prefetch .= '<link rel="preload" as="image" href="'.get_template_directory_uri().'/src/img/common/bg-page-header-medium.jpg">'.PHP_EOL;
		$prefetch .= '<!-- CSS-->'.PHP_EOL;
		echo $prefetch;
	}
	
	/* 1.6 Set OGP */
	add_action('wp_head', 'coco_wp_head_meta_ogp', 10);
	function coco_wp_head_meta_ogp() {
		global $post_id;
		if(is_404()){
			return;
		}
		if(is_front_page()){
			$ogType = 'website';
		}else{
			$ogType = 'article';
		}

		if(is_front_page()){
			$ogTitle = get_bloginfo('name');
		} else {
			$ogTitle = coco_title_decided_by_client().' | '.get_bloginfo('name');
		}

		$ogImage = get_template_directory_uri().'/src/img/common/ogp.png';
		if(is_page() || is_single()){
			if(get_the_post_thumbnail_url( $post_id )!==false) {
				$ogImage = get_the_post_thumbnail_url($post_id);
			}
		}

		$ogp = '<!-- OGP -->'.PHP_EOL;
    $ogp .= '<meta name="twitter:card" content="summary_large_image">'.PHP_EOL;
		$ogp .= '<meta property="og:title" content="'.$ogTitle.'">'.PHP_EOL;
    $ogp .= '<meta property="og:type" content="'.$ogType.'">'.PHP_EOL;
    $ogp .= '<meta property="og:url" content="'.(empty($_SERVER['HTTPS'] ) ? 'http://' : 'https://').$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'">'.PHP_EOL;
    $ogp .= '<meta property="og:image" content="'.$ogImage.'">'.PHP_EOL;
    $ogp .= '<meta property="og:site_name" content="'.get_bloginfo('name').'">'.PHP_EOL;
		$ogp .= '<meta property="og:description" content="'.coco_description().'">'.PHP_EOL;
		echo $ogp;
	}

	function coco_page_info($slug, $return){
		$page = get_page_by_path($slug);
		if($return == 'url'){
			return get_permalink($page->ID);
		}elseif($return == 'id'){
			return $page->ID;
		}elseif($return == 'name'){
			return $page->post_title;
		}
	}

	/* 1.1 CSS */
	add_action('wp_enqueue_scripts', function() {
		if(is_admin()){
			return;
		}
		if(is_front_page() || is_singular() ) {
			wp_enqueue_style('swiper', get_template_directory_uri().'/src/css/swiper.css', [], '2020042303', 'print');
			// wp_enqueue_style('css-swiper', 'https://unpkg.com/swiper/css/swiper.min.css', [], '', null);
		}
		wp_enqueue_style('css-common', get_template_directory_uri().'/src/css/common.css', [], '2021040812', null);
		if( is_front_page()){
			wp_enqueue_style('css-index', get_template_directory_uri().'/src/css/index.css', [], '2020060907', null);
		}
		if( is_single() ) {
			wp_enqueue_style('css-editor', get_template_directory_uri().'/src/css/editor.css', [], '2020091611', null);
				wp_enqueue_style('css-trial', get_template_directory_uri().'/src/css/trial.css', [], '20210226', null);
			
		} elseif( is_page() ) {
      set_page_css();
		} elseif( is_404() ) {
			wp_enqueue_style('css-notfound', get_template_directory_uri().'/src/css/notfound.css', [], '2020051304', null);
		}

		if( is_post_type_archive( 'staff' ) || is_singular( 'staff' )) {
			wp_enqueue_style('css-staff', get_template_directory_uri().'/src/css/staff.css', [], '2020053104', null);
		} elseif( is_post_type_archive( 'faq' ) ) {
			wp_enqueue_style('css-faq', get_template_directory_uri().'/src/css/faq.css', [], '2020051304', null);
		}
	});
	function set_page_css() {
		if(is_page('contact') ) {
			wp_enqueue_style('css-page', get_template_directory_uri().'/src/css/form.css', [], '', null);
			return;
		}
		global $post;
		wp_enqueue_style('css-page', get_template_directory_uri().'/src/css/'.$post->post_name.'.css', [],  '2021021922', null);

/*
		global $post;
		if($post->post_parent != 0) {
			$parent = get_post( $post->post_parent);
			$parent_slug = $parent->post_name;
		} else {
			$parent_slug = $post->post_name;
		}
		wp_enqueue_style('css-page', get_template_directory_uri().'/src/css/'.$parent_slug.'.css?mod=20200325', array(), false, 'all');
*/
	}

	add_filter('style_loader_tag', 'coco_replace_link_stylesheet_tag');
	function coco_replace_link_stylesheet_tag($tag){
		$tag = preg_replace("/id='swiper-css'/", "id='swiper-css' onload=\"this.media='all'\"", $tag);
		return preg_replace(array( "/id='.+-css'/", '/ \/>/'), array( '', '>'), $tag);
	}

	add_action('wp_head', 'coco_wp_head_icon', 20);
	function coco_wp_head_icon(){
		$icon  = '<!-- ICON-->'."\n";
    $icon .= '<link rel="icon" href="'.home_url().'/favicon.ico">'.PHP_EOL;
    $icon .= '<link rel="apple-touch-icon-precomposed" sizes="57x57" href="'.get_template_directory_uri().'/src/favicon/apple-touch-icon-57x57.png">'.PHP_EOL;
    $icon .= '<link rel="apple-touch-icon-precomposed" sizes="72x72" href="'.get_template_directory_uri().'/src/favicon/apple-touch-icon-72x72.png">'.PHP_EOL;
    $icon .= '<link rel="apple-touch-icon-precomposed" sizes="114x114" href="'.get_template_directory_uri().'/src/favicon/apple-touch-icon-114x114.png">'.PHP_EOL;
    $icon .= '<link rel="apple-touch-icon-precomposed" sizes="144x144" href="'.get_template_directory_uri().'/src/favicon/apple-touch-icon-144x144.png">'.PHP_EOL;
    $icon .= '<link rel="apple-touch-icon-precomposed" sizes="152x152" href="'.get_template_directory_uri().'/src/favicon/apple-touch-icon-152x152.png">'.PHP_EOL;
    $icon .= '<link rel="icon" type="image/png" sizes="192x192" href="'.get_template_directory_uri().'/src/favicon/icon-192x192.png">'.PHP_EOL;
		echo $icon;
	}

	/* 1.2 */
	add_action('wp_enqueue_scripts', function(){
		if(is_admin()){
			return;
		}

		wp_deregister_script('jquery');
		wp_deregister_script('jquery-core');
		wp_enqueue_script('jquery', get_template_directory_uri().'/src/js/jquery-3.5.0.min.js', [], '', true);
		wp_enqueue_script('lazysizes', get_template_directory_uri().'/src/js/lazysizes.min.js', [], '', true);
		if(is_front_page() || is_singular() ) {
			wp_enqueue_script('swiper', get_template_directory_uri().'/src/js/swiper.min.js?ver=20200710', [], '', true);
		}
		wp_enqueue_script('config', get_template_directory_uri().'/src/js/config.js', [], '2021011513', true);
		wp_enqueue_script('functions', get_template_directory_uri().'/src/js/jquery-functions.js', [], '', true);



		
		if( is_front_page()){
			wp_enqueue_script('adobeFont', get_template_directory_uri().'/src/js/adobeFont.js', [], '', true);
			wp_enqueue_script('index-functions', get_template_directory_uri().'/src/js/jquery-index-functions.js', [], '2020042303', true);
		}
		if(is_page(array('contact', 'confirm', 'thanks'))){
			wp_enqueue_script('datetimepicker', get_template_directory_uri().'/src/js/jquery.datetimepicker.full.js', [], '', true);
			wp_enqueue_script('yubinbango', get_template_directory_uri().'/src/js/yubinbango.js', [], '', true);
			wp_enqueue_script('gogole-recaptcha', 'https://www.google.com/recaptcha/api.js', [], '', true);
			wp_enqueue_script('validation-config', get_template_directory_uri().'/src/js/validation-config.js', [], '', true);
			wp_enqueue_script('validation-inquiry', get_template_directory_uri().'/src/js/validation-inquiry.js', [], '', true);
			wp_enqueue_script('validation-download', get_template_directory_uri().'/src/js/validation-download.js', [], '', true);
						wp_enqueue_script('allonload', get_template_directory_uri().'/src/js/allonload.js', [], '', true);
		}
	});

	if(!is_admin()){
		add_filter('script_loader_tag', 'coco_replace_script_tag');
		function coco_replace_script_tag($tag){
			$async_files = array(
				'lazysizes.min.js', 'adobeFont.js'
			);
			foreach($async_files as $file) {
				$tag = preg_replace("/{$file}'/", "{$file}\" async", $tag);
			}
			$defer_files = array(
				'swiper.min.js', 'jquery-functions.js',
				'jquery.datetimepicker.full.js', 'yubinbango.js', 'api.js',
				'validation-config.js', 'validation-inquiry.js', 'validation-download.js'
			);
			foreach($defer_files as $file) {
				$tag = preg_replace("/{$file}'/", "{$file}\" defer", $tag);
			}
			return preg_replace(array("/'/", '/ type=\"text\/javascript\"/' ), array('"', ''), $tag);
		}
	}

	/* 1.3 */
	add_action('wp_head', 'coco_wp_head_ie', 100);
	function coco_wp_head_ie(){
		if(is_ie()) {
			$ie  = '<!-- IE Polyfill-->'."\n";;
			$ie .= '<script src="'.get_template_directory_uri().'/src/js/picturefill.js"></script>'.PHP_EOL;
			echo $ie;
		}
	}
	function is_ie() {
		$browser = strtolower($_SERVER['HTTP_USER_AGENT']);
		if (strstr($browser , 'trident') || strstr($browser , 'msie')) { return true;	}
		return false;
	}

	/* 1.8 */
	add_action('wp_head', 'coco_wp_head_tracking', 200);
	function coco_wp_head_tracking(){
		if(is_admin()){
			return;
		}
		$tracking = get_option('tracking-before-head-end');

		if(!is_user_logged_in()){
			echo $tracking;
		}
	}

	/* 1.9 Custom Logo */
	add_theme_support('menus');
	register_nav_menus(
		array(
			 'menu-nav'			  => 'ナビゲーションメニュー'
		)
	);
	add_filter('nav_menu_css_class', 'coco_nav_attributes_filter', 100, 1);
	add_filter('nav_menu_item_id', 'coco_nav_attributes_filter', 100, 1);
	add_filter('page_css_class', 'coco_nav_attributes_filter', 100, 1);
	function coco_nav_attributes_filter($var){
		return is_array($var) ? array_intersect($var,
		array('')) : '';
	}

	function trim_link($url, $page_url) {
		$trimed_url = str_replace($page_url, '', $url);
		return !empty($trimed_url) ? $trimed_url : $url;
	}
	function url_here() {
		$http = (is_ssl() ? 'https' : 'http') . '://';
		$url = $http . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
		return $url;
	}

	// menu-nav
	function echo_menu_nav($class) {
		if(!($locations = get_nav_menu_locations())) {
			return;
		}
		if(!($location = $locations["menu-nav"])) {
			return;
		}

		$menu = wp_get_nav_menu_object($location);
		$menuItems = wp_get_nav_menu_items($menu->term_id);

		$func = 'menu_'.$class;
		$html = $func($menuItems);

		echo $html;
	}

	// menu-nav ナビゲーションメニュー
	function menu_nav__main($menuItems) {
		$html = '';
		$page_url = url_here();
		for($i=0;$i<count($menuItems);$i++) {

			$anchor_link = '';
			if(!empty($menuItems[$i]->attr_title)){
				$anchor_link = $menuItems[$i]->attr_title;
			}
			$li_class = '';
			$url = $menuItems[$i]->url.$anchor_link;
			// echo $url.PHP_EOL;
			if($page_url == $menuItems[$i]->url) {
				$li_class = ' class="-current"';
				$url = trim_link($url, $page_url);
			}
			if($menuItems[$i]->menu_item_parent == 0) {
				$html .= '<li'.$li_class.'><a href="'.$url.'">'.$menuItems[$i]->title.'</a>'.PHP_EOL;
				$html .= "\t".'<ul class="nav__sub">'.PHP_EOL;
			} else {
				$html .= "\t\t".'<li><a href="'.$url.'">- '.$menuItems[$i]->title.'</a></li>'.PHP_EOL;
			}
			if($i == count($menuItems) - 1 || $menuItems[$i+1]->menu_item_parent == 0) {
				$html .= "\t".'</ul>'.PHP_EOL;
				$html .= '</li>'.PHP_EOL;
			}
		}
		return $html;
	}

	/* 1.10 */
	add_theme_support('custom-logo', array(
		 'width'       => 260
		,'height'      => 60
		,'flex-height' => true
		,'flex-width'  => true
	));

	function coco_custom_logo(){
		if(function_exists('the_custom_logo')){
			$custom_logo_id = get_theme_mod('custom_logo');
			$image = wp_get_attachment_image_src($custom_logo_id , 'full');
			return $image[0];
		}
	}

	add_filter('admin_init', 'coco_add_general_field_site');
	function coco_add_general_field_site(){
		add_settings_section(
			 'additional_section'
			,'<h2>このサイトの情報</h2>'
			,'additional_section_callback_function_site'
			,'general'
		);

		function additional_section_callback_function_site($arg){
			//SectionDescription
			// echo '<h2>サイト設定</h2>';
		}

		$settings = array(
		  array(
				 'id'       => 'site_catchcopy01'
			  ,'title'    => 'サイトディスクリプション'
			  ,'callback' => 'coco_display_textarea'
			  ,'arg'      => array(
				 'exp'   => ''
				)
			 )
			 ,array(
				'id'       => 'description_archive_post'
			 ,'title'    => 'お知らせアーカイブページのディスクリプション'
			 ,'callback' => 'coco_display_textarea'
			 ,'arg'      => array(
				 'exp'   => '<a target="_blank" href='. get_post_type_archive_link( 'post' ). '>お知らせアーカイブページ</a>'
				)
			 )
			,array(
				'id'       => 'description_archive_columns'
			 ,'title'    => '運動会屋タイムズアーカイブページのディスクリプション'
			 ,'callback' => 'coco_display_textarea'
			 ,'arg'      => array(
				 'exp'   => '<a target="_blank" href='. get_post_type_archive_link( 'columns' ). '>運動会屋タイムズアーカイブ</a>'
				)
		 	)
			,array(
				'id'       => 'description_archive_voices'
			 ,'title'    => 'お客様の声アーカイブページのディスクリプション'
			 ,'callback' => 'coco_display_textarea'
			 ,'arg'      => array(
				 'exp'   => '<a target="_blank" href='. get_post_type_archive_link( 'voices' ). '>お客様の声アーカイブ</a>'
				)
		 	)
			,array(
				'id'       => 'description_archive_staff'
			 ,'title'    => 'スタッフ一覧ページのディスクリプション'
			 ,'callback' => 'coco_display_textarea'
			 ,'arg'      => array(
				 'exp'   => '<a target="_blank" href='. get_post_type_archive_link( 'staff' ). '>スタッフ一覧</a>'
				)
		 	)
			,array(
				'id'       => 'description_archive_faq'
			 ,'title'    => 'よくある質問ページのディスクリプション'
			 ,'callback' => 'coco_display_textarea'
			 ,'arg'      => array(
				 'exp'   => '<a target="_blank" href='. get_post_type_archive_link( 'faq' ). '>よくある質問</a>'
				)
		 	)
			,array(
				 'id'       => 'zipcode'
				,'title'    => '郵便番号'
				,'callback' => 'coco_display_text'
				,'arg'      => array(
					 'class' => 'short-text'
					,'exp'   => 'ハイフン（ - ）あり'
				)
			)
			,array(
				 'id'       => 'pref'
				,'title'    => '都道府県'
				,'callback' => 'coco_display_text'
				,'arg'      => array(
					 'class' => 'regshortular-text'
					,'exp'   => ''
				)
			)
			,array(
				'id'       => 'city'
			 ,'title'    => '市区町村'
			 ,'callback' => 'coco_display_text'
			 ,'arg'      => array(
					'class' => 'short-text'
				 ,'exp'   => ''
			 )
		 )
		 ,array(
				'id'       => 'address'
			 ,'title'    => '住　所'
			 ,'callback' => 'coco_display_text'
			 ,'arg'      => array(
					'class' => 'regular-text general'
				 ,'exp'   => ''
			 )
		 	)
			// ,array(
			// 	'id'       => 'address2'
			//  ,'title'    => '住　所（改行後）'
			//  ,'callback' => 'coco_display_text'
			//  ,'arg'      => array(
			// 		'class' => 'regular-text general'
			// 	 ,'exp'   => ''
			//  )
		 	// )
			// ,array(
			// 	'id'       => 'googlemap'
			//  ,'title'    => 'Google Map URL'
			//  ,'callback' => 'coco_display_text'
			//  ,'arg'      => array(
			// 		'class' => 'regular-text general'
			// 	 ,'exp'   => ''
			//  )
		 	// )
			,array(
				'id'        => 'tel'
				,'title'    => 'TEL'
				,'callback' => 'coco_display_text'
				,'arg'      => array(
					 'class' => 'regular-text general'
					,'exp'   => 'ハイフン（ - ）あり'
				)
			)
			// ,array(
			// 	'id'        => 'fax'
			// 	,'title'    => 'FAX'
			// 	,'callback' => 'coco_display_text'
			// 	,'arg'      => array(
			// 		 'class' => 'regular-text general'
			// 		,'exp'   => 'ハイフン（ - ）あり'
			// 	)
			// )
			// ,array(
			// 	'id'        => 'email'
			// 	,'title'    => 'メールアドレス'
			// 	,'callback' => 'coco_display_text'
			// 	,'arg'      => array(
			// 		 'class' => 'regular-text general'
			// 		,'exp'   => ''
			// 		,'type'  =>'email'
			// 	)
			// )

			// ,array(
			// 	'id'        => 'facebook'
			// 	,'title'    => 'Facebook'
			// 	,'callback' => 'coco_display_text'
			// 	,'arg'      => array(
			// 		 'class' => 'regular-text general'
			// 		,'exp'   => 'Facebook PageのURLを入力'
			// 	)
			// )
			,array(
				'id'       => 'tracking-before-head-end'
			 ,'title'    => 'アクセス解析コード（&lt;/head&gt;の直前）'
			 ,'callback' => 'coco_display_textarea'
			 ,'arg'      => array(
					'exp'  => ''
				 ,'rows' => 10
			 )
		 )
		 ,array(
				'id'       => 'tracking-after-body-start'
			 ,'title'    => 'アクセス解析コード（&lt;body&gt;のすぐ下）'
			 ,'callback' => 'coco_display_textarea'
			 ,'arg'      => array(
					'exp'  => ''
				 ,'rows' => 10
			 )
		 )
		 ,array(
				'id'       => 'tracking-before-body-end'
			 ,'title'    => 'アクセス解析コード（&lt;/body&gt;の直前）'
			 ,'callback' => 'coco_display_textarea'
			 ,'arg'      => array(
					'exp'  => ''
				 ,'rows' => 10
			 )
		 )
	 );

		foreach((array) $settings as $set){
			register_setting('general', $set['id']);
			add_settings_field(
  			 $set['id']
				,$set['title']
				,$set['callback']
				,'general'
				,'additional_section'
				,array_merge(array('label_for' => $set['id']), $set['arg'])
			);
		}
	}
	function full_address() {
		return get_option('pref'). get_option('city'). get_option('address');
	}

	function coco_display_text($arg1) {
    $type  = isset($arg1['type']) ? $arg1['type'] : 'text';
		$class = isset($arg1['class']) ?  $arg1['class'] : 'regular-text';

		$html  = '';
		$html .= '<input class="'.$class.'" name="'.$arg1['label_for'].'" id="'.$arg1['label_for'].'" '.'type="'.$type.'" value="'.esc_html(get_option($arg1['label_for'])).'">';
		$html .= isset($arg1['exp']) ? '<p class="general_notice">'.$arg1['exp'].'</p>' : '';
		echo $html;
	}

	function coco_display_textarea($arg1) {
		$rows  = isset($arg1['rows']) ? $arg1['rows'] : '5';
		$class = isset($arg1['class']) ? $arg1['class'] : 'regular-text';

		$html  = '';
		$html .= '<textarea class="'.$class.'" name="'.$arg1['label_for'].'" id="'.$arg1['label_for'].'" rows="'.$rows.'">'.esc_html(get_option($arg1['label_for'])).'</textarea>';
		$html .= isset($arg1['exp']) ? '<p class="general_notice">'.$arg1['exp'].'</p>' : '';
		echo $html;
	}

	/* 1.11 */
	function coco_csrf_token(){
	  $tokenLength = 16;
	  $bytes = openssl_random_pseudo_bytes($tokenLength);
	  return bin2hex($bytes);
	}

	/* 1.12 */
	add_filter('wp_kses_allowed_html', 'coco_wp_kses_allowed_html', 10, 2);
	function coco_wp_kses_allowed_html($tags, $context){
		$tags['img']['srcset'] = true;
		$tags['input']['value'] = true;
		$tags['textarea'] = true;
		return $tags;
	}
	
	/* 1.13 */
	add_shortcode('url', function(){
	  return home_url();
	});
	add_shortcode('name', function(){
	  return get_bloginfo('name');
	});
	add_shortcode('theme', function(){
	  return get_template_directory_uri();
	});
	add_shortcode('image-directory', function(){
	  return get_template_directory_uri().'/src/img';
	});
	add_filter( 'wp_kses_allowed_html', 'my_wp_kses_allowed_html', 10, 2 );
	function my_wp_kses_allowed_html( $tags, $context ) {
		$tags['img']['srcset'] = true;
		$tags['source']['data-srcset'] = true;
		$tags['source']['srcset'] = true;
		$tags['input']['value'] = true;
		$tags['textarea'] = true;
		return $tags;
	}
	add_shortcode('zipcode', function() {
		 return get_option('zipcode');
	});
	add_shortcode('pref', function() {
	  return get_option('pref');
	});
	add_shortcode('city', function() {
	  return get_option('city');
	});
	add_shortcode('full_address', function() {
	  return full_address();
	});
	add_shortcode('tel', function() {
	  return get_option('tel');
	});
	add_shortcode('about-us-staff-list', function() {
		$html = '';
		$random = rand(1,5);
		for($i=1;$i<=5;$i++) {
			if($i==$random) {
				$html .= '<li class="-display">';
			} else {
				$html .= '<li>';
			}
			$html .= '<img class="lazyload" data-src="'.get_template_directory_uri().'/src/img/about-us/staff0'.$i.'.jpg?ver=1.0.1" alt="運動会屋スタッフ 顔写真'.$i.'"></li>';
		}
	  return $html;
	});
	/* 1.14 */
  add_action('template_redirect', 'coco_404');
	function coco_404() {
		if(!is_admin()) {
			if(is_attachment()
				|| is_author()
				|| is_year() || is_month() || is_day()
				|| is_singular( 'faq' )
				|| is_singular( 'goaltape-words' )
				|| is_post_type_archive( 'goaltape-words' )
				|| is_tax('voices-customer' )
			) {

				global $wp_query;
				$wp_query->set_404();
				status_header(404);
			}
		}
	}

	add_filter('author_rewrite_rules', '__return_empty_array');
	add_action('init', 'disable_author_archive');
	function disable_author_archive(){
		if(!empty($_GET['author']) || preg_match('#/author/.+#', $_SERVER['REQUEST_URI'])){
			wp_redirect(home_url());
			exit;
		}
	}

	/* 1.15 Breadcrumb */
	function display_breadcrumb() {
		$breadcrumb  = '<li><a href="'. home_url() .'">TOP</a></li>'.PHP_EOL;
		 
		if( is_single() ) {
			global $post;
			global $wp_query;
			// coco_dump($wp_query);
			$post_type = $post->post_type;
			if($post_type == 'post') {
				$taxonomy = 'category';
				$category = current(get_the_category());
			} elseif($post_type == 'columns') {
				$taxonomy = 'columns-category';
				$category = current(get_the_terms( $post, $taxonomy));
			}
			if(!empty($category)) {
				$cat_ancestor_ids = array();
				if( $category->parent != 0 ) {
					$cat_ancestor_ids = array_merge( $cat_ancestor_ids, get_ancestors( $category->term_id, $taxonomy) );
				}
				$cat_ancestor_ids[] = $category->term_id;
				$cat_ancestors = array();
				foreach($cat_ancestor_ids as $id) {
					$cat_ancestors[] = get_term( $id, $taxonomy );
				}
			}
		} elseif( is_tax() ) {
			global $taxonomy;
			$post_type = get_taxonomy($taxonomy)->object_type[0];
		} elseif( is_category() ) {
			global $wp_query;
			$post_type = 'post';
			$cat_slugs = explode('/', $wp_query->query['category_name']);
			$cat_ancestors = array();

			if(count($cat_slugs) > 1) {
				for($i=0;$i<count($cat_slugs)-1;$i++) {
					$cat_ancestors[] = get_category_by_slug($cat_slugs[$i]);
				}
			}

		}
					if(!empty($post_type)) {
			$breadcrumb .= '<li><a href="'. get_post_type_archive_link( $post_type ) .'">'.  get_the_list_title( $post_type ) .'</a></li>'.PHP_EOL;
		}
		if(!empty($cat_ancestors)) {
			foreach( $cat_ancestors as $cat_ancestor ) {
				$breadcrumb .= '<li><a href="'. get_category_link( $cat_ancestor ) .'">'. $cat_ancestor->name .'</a></li>'.PHP_EOL;
			}
			

		}

		if( is_page() ) {
			$ancestors = get_post_ancestors(get_the_ID());
			foreach($ancestors as $ancestor) {
				$breadcrumb .= '<li><a href="'. get_the_permalink($ancestor) .'">'. get_the_title($ancestor) .'</a></li>'.PHP_EOL;
			}
		}

		$title = get_the_title();
		if( is_archive() ) {
			$title = get_the_list_title();
		} elseif(is_search()) {
			if (have_posts()) {
				$title = '検索結果';
			} else {
				$title = '検索結果（なし）';
			}
		} elseif( is_404() ) {
      $title = '404 Not Found';
		}

		$breadcrumb .= '<li><span>'. $title .'</span></li>'.PHP_EOL;
		echo $breadcrumb;
	}

	function display_staff_breadcrumb() {
		$breadcrumb  = '<li><a href="'. home_url() .'">TOP</a></li>';
		$company_page =  get_page_by_title( '運動会屋とは' );
		$breadcrumb .= '<li><a href="'.  get_page_link( $company_page ) .'">' . $company_page->post_title . '</a></li>';

		if(is_single()) {
			$breadcrumb .= '<li><a href="'. get_post_type_archive_link( 'staff' ) .'">'. get_the_list_title('staff') .'</a></li>';
		}
		if(is_archive()) {
			$title = get_the_list_title();
		} else {
			$title = get_staff_detail_title();
		}
		$breadcrumb .= '<li><span>'. $title .'</span></li>';

		echo $breadcrumb;
	}

	function get_staff_detail_title() {
		return 'スタッフ紹介';
	}
	/* 1.16 Customize Post */
	// add_action( 'after_setup_theme', function () {
	// 	add_theme_support( 'post-thumbnails' );
  //   set_post_thumbnail_size( 800, 9999, array( 'top', 'left') );
	// });
	function coco_get_the_thumbnail_url($size = 'large') {
		global $post;
		if(has_post_thumbnail()) {
			$url = get_the_post_thumbnail_url( $post, $size );
		} else {
			$url = get_template_directory_uri() . '/src/img/common/no-image.jpg';
		}
		return $url;
	}
	function coco_echo_the_thumbnail_alt() {
		global $post;
		$alt = '';
		if(has_post_thumbnail()) {
			$thumbnail_id = get_post_thumbnail_id( $post->ID );
			$alt = get_post_meta($thumbnail_id, '_wp_attachment_image_alt', true);   
		}
		echo $alt;
	}

	/* excerpt 単語数 */
	function coco_trim_excerpt( $text = '', $length = 55 ) {
		$text = strip_shortcodes( $text );
		$text = excerpt_remove_blocks( $text );

		$text = str_replace( ']]>', ']]&gt;', $text );
		$text = str_replace(array("\r\n","\r","\n","&nbsp;"),'',$text);


		$excerpt_length = apply_filters( 'excerpt_length', $length );
		$excerpt_more = apply_filters( 'excerpt_more', ' ' . '&nbsp;&hellip;' );
		$text         = wp_trim_words( $text, $excerpt_length, $excerpt_more );

		return $text;
	}

	/* 文字数で抜粋 */
	function mb_substr_excerpt($text, $length) {
		if(mb_strlen($text, 'UTF-8') > $length) {
			$text = mb_substr($text, 0, $length - 2, 'UTF-8') . "&hellip;";
		}
		return $text;
	}

	/* contents 表示の時にタグなどを置き換え */
	add_action('the_content', 'coco_change_html_tag');
	function coco_change_html_tag($content) {
		$content = preg_replace('/<h1>(.*)<\/h1>/', '<h3>$1</h3>', $content);
		$content = preg_replace('/<h2>(.*)<\/h2>/', '<h3>$1</h3>', $content);
		$content = str_replace('./', '', $content);
		$content = str_replace('<strong>', '<b>', $content);
		$content = str_replace('</strong>', '</b>', $content);
		$content = str_replace('<em>', '<i>', $content);
		$content = str_replace('</em>', '</i>', $content);
		return $content;
	}

	/* 1.17 Customize List Page */
	/* List Archive Title */
	function get_the_list_title($post_type=null) {
		if( is_tag() || $post_type == 'post_tag' ) {
			return '関連する記事';
		}
		if( !empty($post_type) ) {
			$post_type_obj = get_post_type_object( $post_type );
			return $post_type_obj->label;
		}
		if( is_archive() ) {
      $archive_title = preg_replace('/.*: (.*)/', '$1', get_the_archive_title());
			$title = "{$archive_title}";
			return $title;
		}
	}
	/* Pagination */
	function get_pagination_marks($last_page, $paged) {
		if(!$last_page || $last_page <= 1) return [];
		if($last_page <= 5) {
			return range(1, $last_page);
		}

		if($paged <= 3 ) {
			$marks_pre = [1, 2, 3];
		} else {
			if( $paged >= $last_page - 2) {
				$marks_pre = [1, 'dot', $last_page - 3];
			} else{
				$marks_pre = [1, 'dot', $paged - 1];
			}
		}
		if(($paged > 3) && ( $paged < $last_page - 2)) {
			$marks_suf = [$paged, $paged + 1, 'dot', $last_page];
		} else {
			$center = null;
			if ($paged <= 3 ) {
				$marks_suf = [4, 'dot', $last_page];
			} else {
				$marks_suf = [$last_page - 2, $last_page - 1, $last_page];
			}
		}

		return array_merge($marks_pre, $marks_suf);
	}

	function coco_pagination($last_page = '', $range = 1){
		$showitems = ($range * 2) + 1;
		$num_of_delimiter = 3;
		global $paged;
		if(empty($paged)){
			$paged = 1;
		}
		if($last_page == ''){
			global $wp_query;
			$last_page = $wp_query->max_num_pages;
		}

		$html = '';
		$html .= "\t\t\t\t\t\t".'<ul class="flex-middle-center">'."\n";

		/* Go First Page
		if($paged > 2 && $paged > $range+1 && $showitems < $last_page){
			$html .= '<li class="pager--prev"><a href="'.get_pagenum_link(1).'"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>'."\n";
		}
			*/

		/* Go prev Page */
		if($paged > 1 && $showitems < $last_page){
			$html .= "\t\t\t\t\t\t\t".'<li class="-prev"><a href="'.get_pagenum_link($paged - 1).'">prev</a></li>'."\n";
		}

		/* Page number and current */
		$li_pages = get_pagination_marks($last_page, $paged);
		foreach($li_pages as $li_page) {
			if($li_page == 'dot') {
				$html .= "\t\t\t\t\t\t\t".'<span>&hellip;</span>'."\n";
				continue;
			}
			if($paged == $li_page){
				$html .= "\t\t\t\t\t\t\t".'<li><b>'.$li_page.'</b></li>'."\n";
			}else{
				$html .= "\t\t\t\t\t\t\t".'<li><a href="'.get_pagenum_link($li_page).'">'.$li_page.'</a></li>'."\n";
			}
		}

		/* Go Next Page */
		if($paged < $last_page && $showitems < $last_page){
			$html .= "\t\t\t\t\t\t\t".'<li class="-next"><a href="'.get_pagenum_link($paged + 1).'">next</a></li>'."\n";
		}

		/* Go Final Page */
		// if($paged+$range-1 < $last_page && $showitems < $last_page){
		// 	// $html .= '<li class=""><a href="'.get_pagenum_link($last_page).'">'.$last_page.'</a></li>'."\n";
		// 	if($paged == $last_page){
		// 		$html .= "\t\t\t\t\t\t\t".'<li><b>'.$last_page.'</b></li>'."\n";
		// 	}else{
		// 		$html .= "\t\t\t\t\t\t\t".'<li><a href="'.get_pagenum_link($last_page).'">'.$last_page.'</a></li>'."\n";
		// 	}
		// }

		$html .= "\t\t\t\t\t\t".'</ul>'."\n";
		return $html;
	}

	/* Date list */
	function get_monthly_list($limit) {
		$list = wp_get_archives('type=monthly&limit='.$limit.'&format=html&show_post_count=true&echo=0');
		return preg_replace('/<\/a>&nbsp;(\([0-9]*\))/', '$1</a>', $list);
	}

	/* Tag list */
	function coco_get_the_tag_list( $post_id ) {
		$tags = get_the_tags( $post_id );
		if(empty($tags)) return '';
		$html = '';
		foreach($tags as $tag) {
			$tag_link = get_tag_link( $tag->term_id );
			
			$html .= "<li><a href='{$tag_link}'><span>#";
			$html .= "{$tag->name}</span></a></li>";
		}
		return $html;
	}
	function get_all_tags_list($number=null) {
		if(is_numeric($number)) {
			$tags = get_tags('orderby=count&number='.$number);
		} else {
			$tags = get_tags('orderby=count');
		}
		$html = '';
		foreach($tags as $tag) {
			$tag_link = get_tag_link( $tag->term_id );
			
			$html .= "<li><a href='{$tag_link}'><span>#";
			$html .= "{$tag->name}</span></a></li>";
		}
		return $html;
	}

	/* All Term list */
	function get_all_terms_list( $taxonomy, $hide_empty, $number=null ) {
		if(empty($taxonomy)) {
			return '';
		}
		if(is_numeric($number)) {
			$terms = get_categories( array( 'taxonomy' => $taxonomy, 'number' =>$number, 'hide_empty' => $hide_empty, ) );
		} else {
			$terms = get_categories( array( 'taxonomy' => $taxonomy, 'hide_empty' => $hide_empty,  ));
		}
		$html = '';
		foreach( $terms as $term ) {
			$term_link = get_term_link( $term, $taxonomy );
			$html .= "<li><a href='{$term_link}'>";
			$html .= "{$term->name}</a></li>";
		}
		return $html;
	}

	function get_faq_list() {
		$term_ids = get_option('setting_faq_category');
		$taxonomy = 'faq-category';
		$html = '';
		if(empty($term_ids)) return '';
		foreach( $term_ids as $term_id ) {
			$term = get_term( $term_id, $taxonomy );
			$html .= '<li><a href="'. get_post_type_archive_link( 'faq' ) .'#'.$term_id.'">';
			$html .= "{$term->name}</a></li>";
		}
		return $html;
	}
	function get_taxonomy_slug( $post_type ) {
		switch ($post_type) {
			case 'post':
				$category_slug = 'category';
				break;
			case 'voices':
				$category_slug = 'voices-customer';
				break;
			case 'columns':
				$category_slug = 'columns-category';
				break;
			default:
				break;
		}
		return $category_slug;
	}

	/* 1.17.1 タグアーカイブページの対象を設定 */
	add_action( 'pre_get_posts', function ( $query ) {
		if(!is_admin()) {
			if( is_archive() && !is_post_type_archive( 'faq' ) && !is_post_type_archive( 'staff' )) {
				$query->set( 'posts_per_page', 12); //
			}
		}
		if ( !is_admin() // Only target front end queries
				&& $query->is_main_query() // Only target the main query
				&& $query->is_tag()        // Only target tag archives
		) {
			$query->set( 'post_type', coco_get_post_type_for_tag_post() );
		}
	});

	function get_matches_for_tag_post_type(){
		preg_match('/g=(voices|columns|post)/', $_SERVER['QUERY_STRING'], $matches);
		return $matches;
	}

	function coco_all_post_type_for_tag_post() {
		return ['post', 'voices', 'columns'];
	}

	function coco_get_post_type_for_tag_post(){
		$matches = get_matches_for_tag_post_type();
		if( $matches == array() ) {
			return coco_all_post_type_for_tag_post();
		}
		return $matches[1];
	}

	function coco_echo_body_class() {
		if(is_404()
		|| is_page('contact') && isset($_POST['step']) && ( $_POST['step'] == 1 || $_POST['step'] == 2 )
		) {
			return ' class="not-small-logo"';
		} 
	}
	function inquiryMeetingHow() {
		return array(
			'zoom' => 'Zoom',
			'google meet' => 'Google Meet',
			'skype' => 'Skype',
			'facetoface' => '対面',
			'other' => 'その他',
		);	
	}
	add_action('wp_ajax_coco_ajax', 'coco_ajax');
	add_action('wp_ajax_nopriv_coco_ajax', 'coco_ajax');
	function coco_ajax(){
		// $_POST['values']['email']
		// $_POST['values']['company']
		// $_POST['values']['division']
		// $_POST['values']['yourname']
		// $_POST['values']['phone']
		// $_POST['values']['recaptcha']
		// メールが送信できたら1、できなければ0をechoする
		$mailer = new CoCoMail_Download;
		echo $mailer->send_owner();
		die();
	}

	add_action('wp_footer', 'coco_add_ajaxuri', 1);
	function coco_add_ajaxuri(){
		$script  = '<script>var $ajaxURL = \''.admin_url('admin-ajax.php').'\';</script>'."\n";
		echo $script;
	}

	add_editor_style('editor-style.css');
	add_theme_support('editor-styles');



	/* メールアドレス確認 */
add_filter( 'wpcf7_validate_email', 'wpcf7_validate_email_filter_extend', 11, 2 );
add_filter( 'wpcf7_validate_email*', 'wpcf7_validate_email_filter_extend', 11, 2 );
function wpcf7_validate_email_filter_extend( $result, $tag ) {
    $type = $tag['type'];
    $name = $tag['name'];
    $_POST[$name] = trim( strtr( (string) $_POST[$name], "n", " " ) );
    if ( 'email' == $type || 'email*' == $type ) {
        if (preg_match('/(.*)_confirm$/', $name, $matches)){ //確認用メルアド入力フォーム名を ○○○_confirm としています。
            $target_name = $matches[1];
            if ($_POST[$name] != $_POST[$target_name]) {
                if (method_exists($result, 'invalidate')) {
                    $result->invalidate( $tag,"確認用のメールアドレスが一致していません");
                } else {
                    $result['valid'] = false;
                    $result['reason'][$name] = '確認用のメールアドレスが一致していません';
                }
            }
        }
    }
    return $result;
}