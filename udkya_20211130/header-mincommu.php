<!DOCTYPE html>
<html lang="ja">

<head>
    <meta name="robots" content="noindex,nofollow">
    <!-- Basic Page Needs
================================================== -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- Mobile Specific Metas
================================================== -->

    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/top/favicon.ico" type="images/top/favicon.ico" sizes="16x16" />
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/images/top/favicon.ico" type="images/top/favicon.ico" sizes="16x16" />
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/images/top/fav.jpg" />

    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
    <meta name="twitter:card" content="summary_large_image" />

    <title>みんコミュ</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <!-- <link rel="stylesheet" type="text/css" href="./css/reset.css" /> -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=M+PLUS+1p" />

    <meta property="og:type" content="article" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="https://www.beams.co.jp/special/2021aw_all/kids_01/" />
    <meta property="og:title" content="" />
    <meta property="og:image" content="https://www.beams.co.jp/special/2021aw_all/kids_01/src/img/mincommu/ogp.jpg" />
    <meta property="og:description" content="" />

    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:image" content="https://www.beams.co.jp/special/2021aw_all/kids_01/src/img/mincommu/ogp.jpg" />

    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">

    <link href="http://fonts.googleapis.com/earlyaccess/notosansjp.css">


    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/src/css/mincommu/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/src/css/mincommu/common.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/src/css/mincommu/caseList.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/src/css/mincommu/caseDetails.css" />
    <!-- Javascript Files
================================================== -->

    <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>

    <!-- Slick Js-->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css">
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/src/js/mincommu/top.js"></script>

    <!--[if lt IE 9]>
	  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
    <header class="min_header">
        <div class="min_header__innar">
            <div class="sp_contents">
                <a href="" class="sp_open">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/hum.svg" alt="">
                </a>
            </div>
            <h1>
                <a href="">
                    <img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/logo_h1.svg" alt="みんコミュ">
                </a>
            </h1>
            <nav>
                <ul>
                    <li><a href="">みんコミュとは</a></li>
                    <li><a href="">メリット</a></li>
                    <li><a href="">イベント紹介</a></li>
                    <li><a href="<?php echo home_url(); ?>/case_/">実例紹介</a></li>
                    <li><a href="">ご利用料金</a></li>
                    <li><a href="<?php echo home_url(); ?>/mincommu/columns/">コラム</a></li>
                    <li><a href="">FAQ</a></li>
                    <li><a href="">お問い合わせ</a></li>
                </ul>
            </nav>
        </div>
        <div class="sp_contents">
            <div class="nav_wrap">
                <nav class="sp_nav">
                    <a href="" class="sp_close">
                        <img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/menu_close.svg" alt="cloce">
                    </a>
                    <a href="" class="sp_logo">
                        <img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/logo_sp.svg" alt="みんコミュ">
                    </a>
                    <ul class="menu_contents">
                        <li><a href="">みんコミュとは</a></li>
                        <li><a href="">メリット</a></li>
                        <li><a href="">イベント紹介</a></li>
                        <li><a href="<?php echo home_url(); ?>/case">実例紹介</a></li>
                        <li><a href="">ご利用料金</a></li>
                        <li><a href="<?php echo home_url(); ?>/columns/plan">コラム</a></li>
                        <li><a href="">FAQ</a></li>
                        <li><a href="">お問い合わせ</a></li>
                    </ul>
                    <a href="" class="sp_nav_btn">
                        <img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sp_btn_01.svg" alt="無料相談">
                    </a>
                    <ul class="sns">
                        <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sp_f.svg" alt="facebook"></a></li>
                        <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sp_TWITTER.svg" alt="twitter"></a></li>
                        <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sp_insta.svg" alt="instagram"></a></li>
                        <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sp_youtube.svg" alt="youtube"></a></li>
                        <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sp_LINE.svg" alt="line"></a></li>
                    </ul>
                    <ul class="f_link">
                        <li><a href="">運営会社</a></li>
                        <li><a href="">個人情報保護方針</a></li>
                        <li><a href="">サイトマップ</a></li>
                    </ul>
                    <p class="copy">
                        © 2021 みんコミュ All Rights Reserved.
                    </p>
                </nav>
            </div>
        </div>
    </header>