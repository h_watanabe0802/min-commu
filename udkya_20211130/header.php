<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <?php wp_head(); ?>
  </head>
  <body<?= coco_echo_body_class() ?>>
<?php
    if(!is_user_logged_in()){
			echo get_option('tracking-after-body-start');
    }
?>
    <div class="wrapper newkv" id="js-wrapper">
      <header class="header release-background" id="js-header">
        <div class="header__inner flex-middle">
          <h1 class="header__logo"><a id="js-header-logo" href="<?= home_url() ?>"><img class="-small" id="js-logo-small" src="<?= get_template_directory_uri() ?>/src/img/common/logo-small.svg" alt="運動会屋ロゴ"><img class="lazyload -large" id="js-logo-large" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_template_directory_uri() ?>/src/img/common/logo-large.svg" alt="運動会屋ロゴ"></a></h1>
          <div class="nav">
<?php get_template_part( 'partial/navigation', 'head' ); ?>
<?php get_template_part( 'partial/navigation', 'sns' ); ?>
<?php get_template_part( 'partial/navigation', 'middle' ); ?>
            <div class="nav__info"><a class="nav__info-logo" href="<?= home_url() ?>"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/logo-small.svg" alt="[object Object]ロゴ"></a>
              <p class="nav__info-address">〒<?= get_option( 'zipcode' ) ?> <?= full_address() ?></p>
              <p class="nav__info-copyright"><small>Copyright &copy; <a href="https://www.undokai.co.jp/">運動会屋</a> All Rights Reserved.</small></p>
            </div>
          </div>
          <!-- /.nav-->
          <div class="header__burger">
            <button class="header__burger-button" id="js-menu-button"><span class="header__burger-line"></span></button>
          </div>
          <!-- /.header__burger-->
        </div>
        <!-- /.header__inner-->
      </header>
      <!-- /.header-->
      <div class="container">
<?php if( is_singular('staff') ) : ?>
        <article class="main">
<?php elseif( is_single() ) : ?>
        <article class="main article">
<?php else : ?>
        <main class="main">
<?php endif; ?>
