<?php

/**
 * Template Name:category_column
 */

get_header('mincommu'); ?>





<?php $post_type = get_query_var('post_type');

$taxonomy = get_query_var('taxonomy');
$post_type = get_taxonomy($taxonomy)->object_type[0];


?>




<!-- ▼mailArea -->
<section class="mailArea" style="display: block;">
    <a href="">
        <div class="mailArea_circle">
            <img src="img/floating.png" alt="">
        </div>
        <div class="mailArea_circle_text">
            <img src="img/floating_txt.png" alt="">
        </div>
    </a>
</section>
<!-- ▼mailArea -->

<!-- ▼kvArea -->
<div class="kv_area column_kv">
    <div class="kv_area--wrap">
        <h2 class="title">
            コラム
        </h2>
        <p class="title-en">
            column
        </p>
        <div class="pic">
            <img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/mr_m_00.png" alt="">
        </div>
        <p class="text">
            テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
        </p>
    </div>
</div>
<!-- ▲kvArea -->

<!-- ▼title -->
<div class="titleArea">
    <h2 class="titleArea__title">
        実例紹介一覧
    </h2>
    <p>
        LIST
    </p>
</div>
<!-- ▲title -->

<!-- ▼category -->
<div class="category sp_contents">
    <div class="category_title">
        <p>CATEGORY</p>
    </div>
    <div class="select">

        <select name="selectCategory" size="1">
            <option value="すべて">すべて</option>
            <option value="コミュニケーション">コミュニケーション</option>
            <option value="モチベーション">モチベーション</option>
            <option value="従業員満足度">従業員満足度</option>
            <option value="福利厚生">福利厚生</option>
            <option value="共通認識">共通認識</option>
            <option value="一体感">一体感</option>
        </select>
    </div>
</div>
<!-- ▲category -->

<!-- ▼sort -->
<div class="category sp_contents">
    <div class="category_title">
        <p>SORT</p>
    </div>
    <div class="select">
        <select name="selectCategory" size="1">
            <option value="すべて">新着順</option>
            <option value="人気順">人気順</option>
        </select>
    </div>
</div>
<!-- ▲sort -->


<!-- ▼contents -->
<section class="contents">

    <!-- ▼contents__left -->
    <div class="contents__left">
        <div class="contents__left--category pc_contents">
            <h3 class="title">
                category
            </h3>
            <?php $self_name = get_the_title(); ?>
            <ul>
                <li>
                    <div class="wrap">
                        すべて
                    </div>
                </li>
                <li class="<?php if ("plan" == $self_name) {
                                echo 'on';
                            } ?>">
                    <div class="wrap">
                        <div class="pic">
                            <img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/man.png" alt="">
                        </div>
                        <p>
                            <a href="<?php echo home_url(); ?>/columns/plan">
                                企画
                            </a>
                        </p>
                    </div>
                </li>
                <li class="<?php if ("Preparation" == $self_name) {
                                echo 'on';
                            } ?>">
                    <div class="wrap">
                        <div class="pic">
                            <img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/man.png" alt="">
                        </div>
                        <p>
                            <a href="<?php echo home_url(); ?>/columns/Preparation">
                                準備
                            </a>
                        </p>
                    </div>
                </li>
                <li class="<?php if ("operation" == $self_name) {
                                echo 'on';
                            } ?>">
                    <div class="wrap">
                        <div class="pic">
                            <img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/man.png" alt="">
                        </div>
                        <p>
                            <a href="<?php echo home_url(); ?>/columns/operation">
                                運営
                            </a>
                        </p>
                    </div>
                </li>

            </ul>

        </div>

        <div class="contents__left--sort pc_contents">
            <p class="title">
                SORT
            </p>
            <div class="on">
                新着順
            </div>
            <div href="#">
                人気順
            </div>
        </div>


        <div class="contents__left--wrap">
            <?php

            $title = get_the_title();


            $wp_query = new WP_Query();


            if (isset($_GET['tags'])) {



                $hotword = $_GET['tags'] == "FLUFFY FUR BAG" ? "fluffy-fur-bag" : $_GET['tags'];

                $param = array(
                    'posts_per_page' => '2', //表示件数。-1なら全件表示
                    'post_type' => 'column', //カスタム投稿タイプの名称を入れる
                    'post_status' => 'publish', //取得するステータス。publishなら一般公開のもののみ
                    // 'orderby' => 'post_date', //日付順に並び替え
                    // 'order' => 'DESC',
                    'paged' => $paged,
                );
            } else {



                $param = array(
                    'posts_per_page' => '3', //表示件数。-1なら全件表示
                    'post_type' => 'column', //カスタム投稿タイプの名称を入れる
                    'post_status' => 'publish', //取得するステータス。publishなら一般公開のもののみ
                    // 'orderby' => 'post_date', //日付順に並び替え
                    // 'order' => 'DESC',
                    'paged' => $paged,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'column_list',
                            'field' => 'slug',
                            'terms' => $title,
                        ),
                    )
                );
            }

            // Add 2016.05.16 Sato[S]
            // カテゴリー
            if (isset($_GET['category']) && $_GET['category'] != 'all-category') {
                $shop_array = array(
                    'key' => 'category',
                    'value' => $_GET['category'],
                    'compare' => '='
                );
            }
            // パターン
            if (isset($_GET['pattern']) && $_GET['pattern'] != 'all') {
                $height_array = array(
                    'key' => 'pattern',
                    'value' => $_GET['pattern'],
                    'compare' => '='
                );
            }


            $param = array_merge($param, array('meta_query' => array()));
            array_push($param['meta_query'], $shop_array, $height_array, $fn, $utility, $tax_query);
            // Add 2016.05.16 Sato[E]

            $wp_query->query($param);
            if ($wp_query->have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>



                    <!-- ▼block -->
                    <div class="block 
                                <?php
                                $on = get_field("scene");
                                if ($on == "on") : ?>
                                    online
                                <?php elseif ($on == "off") : ?>
                                    offline
                                <?php elseif ($on == "on_off") : ?>
                                    onoffline
                                <?php endif; ?>">
                        <a class="pic" href="<?php echo get_permalink(); ?>">
                            <img src="<?php the_field('main_img'); ?>" alt="<?php the_field('alt'); ?>">
                            <div class="tag">
                                <?php
                                $on = get_field("scene");
                                if ($on == "on") : ?>
                                    <p>ON LINE</p>

                                <?php elseif ($on == "off") : ?>
                                    <p>OFF LINE</p>
                                <?php elseif ($on == "on_off") : ?>

                                <?php endif; ?>

                            </div>
                        </a>
                        <div class="wrap">
                            <div class="c-wrap">
                                <a href="<?php echo home_url(); ?>/columns/<?php the_field("category") ?>" class="category">
                                    <div class="pic">
                                        <img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/man.png" alt="">
                                    </div>
                                    <p>
                                        <?php

                                        $on = get_field("category");
                                        if ($on == "plan") : ?>
                                            企画
                                        <?php elseif ($on == "Preparation") : ?>
                                            準備
                                        <?php elseif ($on == "operation") : ?>
                                            運営
                                        <?php endif; ?>
                                    </p>
                                </a>
                                <p class="date">
                                    公開日：2021.02.27
                                </p>
                            </div>
                            <div class="hashtag">
                                <ul>
                                    <?php
                                    $terms = get_terms('tags');
                                    foreach ($terms as $term) {
                                        echo '<li><a href="' . home_url() . '/minkomyu/tag/?tags=' . $term->name . '">#' . $term->name . '</a></li>'; //タームのリンク
                                    }
                                    ?>
                                </ul>

                            </div>
                            <h3 class="title">
                                <a href="<?php echo get_permalink(); ?>">
                                    <?php the_field('main_ttl'); ?>
                                </a>
                            </h3>
                            <div class="column_text1">
                                <?php the_field('main_textarea'); ?>
                            </div>

                            <!-- <div class="name">
                                <span>社名：</span><?php the_field('company'); ?>
                            </div>
                            <div class="num">
                                <span>人数：</span><?php the_field('num'); ?>
                            </div> -->
                            <!-- <div class="resArea">
                                <div class="resArea-wrap">
                                    <p class="resArea-title">
                                        開催後の反響
                                    </p>
                                    
                                    
                                    <?php if (have_rows('event')) : ?>
                                        <?php while (have_rows('event')) : the_row(); ?>

                                            <p class="resArea-text">
                                                <?php the_sub_field('event-1'); ?>
                                            </p>

                                        <?php endwhile; ?>
                                    <?php endif; ?>



                                </div>
                            </div> -->
                            <a href="<?php echo get_permalink(); ?>" class="more">MORE</a>
                        </div>
                    </div>
                    <!-- ▲block -->
                <?php endwhile; ?>







                <!-- ▲block -->
        </div>
    </div>
    <!-- ▲contents__left -->

    <!-- ▼contents__right -->
    <div class="contents__right">
        <div class="contents__right--hashlist">
            <div class="inner">
                <p class="title">
                    HASHTAGS
                </p>
                <ul class="wrap">
                    <li>
                        <a href="<?php echo home_url(); ?>/tag/?tags=参加型">
                            #参加型
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo home_url(); ?>/tag/?tags=テスト">
                            #テスト
                        </a>
                    </li>
                    <li>
                        <a href="">
                            #司会進行
                        </a>
                    </li>
                    <li>
                        <a href="">
                            #プログラム
                        </a>
                    </li>
                    <li>
                        <a href="">
                            #大人数
                        </a>
                    </li>
                    <li>
                        <a href="">
                            #少人数
                        </a>
                    </li>
                    <li>
                        <a href="">
                            #パッケージ
                        </a>
                    </li>
                    <li>
                        <a href="">
                            #交流会
                        </a>
                    </li>
                    <li>
                        <a href="">
                            #オンライン
                        </a>
                    </li>
                    <li>
                        <a href="">
                            #オフライン
                        </a>
                    </li>
                    <li>
                        <a href="">
                            #内定式
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- ▲contents__right -->
    <div class="pagination">
        <?php global $wp_rewrite;
                $paginate_base = get_pagenum_link(1);
                if (strpos($paginate_base, '?') || !$wp_rewrite->using_permalinks()) {
                    $paginate_format = '';
                    $paginate_base = add_query_arg('paged', '%#%');
                } else {
                    $paginate_format = (substr($paginate_base, -1, 1) == '/' ? '' : '/') .
                        user_trailingslashit('page/%#%/', 'paged');;
                    $paginate_base .= '%_%';
                }
                echo paginate_links(array(
                    'base' => $paginate_base,
                    'format' => $paginate_format,
                    'total' => $wp_query->max_num_pages,
                    'mid_size' => 2,
                    'current' => ($paged ? $paged : 1),
                    'prev_text' => '&#60;',
                    'next_text' => '&#62;',
                )); ?>
    </div>

<?php else : ?>

    <p class="no_item">該当する商品が見つかりませんでした。</p>
<?php endif; ?>

</section>
<!-- ▼contents -->



<script>
    //CATEGORYの背景色
    $('.contents__left--category li').click(function() {
        $('.contents__left--category li').removeClass('on');
        $(this).toggleClass('on');
    })

    //SORTの下線
    $('.contents__left--sort div').click(function() {
        $('.contents__left--sort div').removeClass('on');
        $(this).toggleClass('on');
    })
</script>

<?php get_footer('mincommu'); ?>