<?php get_header(); ?>
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
<?php if(isset($_POST['step']) && ( $_POST['step'] == 1 || $_POST['step'] == 2 )) : ?>
					<div class="breadcrumb">
            <ul class="breadcrumb__list flex-middle">
              <?php display_breadcrumb() ?>
            </ul>
          </div>
          <!-- /.breadcrumb-->
<?php else : ?>
	<?php get_template_part( 'partial/content', 'title' ); ?>
<?php endif; ?>
			<div class="contact">
<?php if(empty($_POST['step']) || $_POST['step'] == 'back'): ?>
				<ul class="contact_tab" id="js-contact-tab">
					<li><a class="-active" href="js-inquiry">お問い合わせ</a></li>
					<li><a href="js-download">資料ダウンロード</a></li>
				</ul>
<?php endif; ?>
<?php
	if(isset($_POST['step']) && $_POST['step'] == 2 && !empty($_POST['inquiry-recaptcha'])):
		$mailer = new CoCoMail_Contact;
		echo '<pre>';
		$mailer->send_mail();
		echo '</pre>';

		get_template_part( 'partial/form', 'thanks' );
	elseif(isset($_POST['step']) && $_POST['step'] == 1):
		get_template_part( 'partial/form', 'confirm' );
	else:
?>
<?php get_template_part( 'partial/form', 'inquiry' ); ?>
<?php get_template_part( 'partial/form', 'download' ); ?>
<?php endif; ?>
			</div>
			<!-- /.contact -->
<?php endwhile; endif; ?>
<?php get_footer(); ?>
