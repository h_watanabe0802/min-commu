<?php

/**
 * Template Name: mincommu
 */

get_header('mincommu'); ?>

<meta name="robots" content="noindex,nofollow">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://use.typekit.net/xfg0bkf.css">

<!-- main_contents -->

<div class="kv_area">
	<picture>
		<source media="(max-width: 769px)" srcset="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sp_kv.png">
		<img class="" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/kv.png" alt="みんコミュ">
	</picture>
</div>
<div class="outer">
	<div class="inner">
		<main class="contents-area">
			<!-- section00 -->
			<section class="sec sec0">
				<div class="sec-in">
					<h2>
						ABOUT
					</h2>
					<div class="sec0-top">
						<h3>
							<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec1_sub.svg" alt="みんコミュとは">
						</h3>
						<p>
							「より参加者に楽しんでもらいたい」<br>
							そんな貴方の負担を軽くし、<br>
							企業/団体のコミュニケーションの<br>
							きっかけになるイベントを提供します！
						</p>
					</div>
					<div class="sp_contents">
						<p class="sec0-center">
							非日常な一日の中での一体感・誰でもできる・つながる・共感・言語問わずアットホームに
							コミュニケーションの取れるイベントを全面的にサポートします
						</p>
					</div>
					<div class="sec0-bottom">
						<p>
							みんなで何か一つのことをする<br>
							機会をつくりたい方と<br>
							私たちが培った企画運営の<br class="sp_br">ノウハウを<br class="pc_br">
							つかって<br class="sp_br">素敵な仕掛けを一緒につくります
						</p>
					</div>
				</div>
				<div class="anim_move">
					<p class="sec0_anim1">みんなで何か一つのことをする機会をつくりたい</p>
					<p class="sec0_anim2">社内イベントを企画したいけど、どうしたらいいのか</p>
					<p class="sec0_anim3">本当は社内のコミュニケーションを改善したい</p>
					<p class="sec0_anim4">社内イベントを企画したいけど、どうしたらいいのか</p>
					<p class="sec0_anim5">みんなで何か一つのことをする機会をつくりたい</p>
				</div>
			</section>
			<!-- section01 -->
			<section class="sec sec01">
				<div class="sec-in-top">
					<div class="sec-in">

						<!-- heading -->
						<div class="area-head">
							<p class="number">01.</p>
							<h2 class="heading">メリット</h2>
							<p class="english">MERIT</p>
							<p class="lead">サポート範囲の広さ、担当者の方に負荷のかからないような手厚いサポートを心がけています</p>
						</div>
						<div class="area-merit">
							<ul class="lst-merit">
								<li>
									<div class="grp-head">
										<div class="number">
											<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/hash01.svg" alt="#01">
										</div>
										<p class="heading01">
											お客様の利用目的に<br>
											合わせてご提案
										</p>
									</div>
									<div class="grp-card">
										<div class="image">
											<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec01_lst_img01.jpg" alt="">
										</div>
										<div class="grp-card-bottom">
											<p class="heading02">
												ニーズをスピーディーに<br>
												具現化し御社だけの<br>
												オリジナルイベントを
											</p>
											<p class="sentence">
												みんコミュはお客様のお声に合わせ、イベント内容・開催目的・規模・ご予算・社内環境に合わせて最も効果的なプランをご提供します
											</p>
										</div>
									</div>
								</li>
								<li>
									<div class="grp-head">
										<div class="number">
											<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/hash02.svg" alt="#02">
										</div>
										<p class="heading01">
											豊富な実績で<br>
											しっかりサポート
										</p>
									</div>
									<div class="grp-card">
										<div class="image">
											<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec01_lst_img01.jpg" alt="">
										</div>
										<div class="grp-card-bottom">
											<p class="heading02">
												豊富な実績で<br>
												しっかりサポート
											</p>
											<p class="sentence">
												どうすればコミュニケーションが取れるか、今後も取りやすくなるか、今と未来を考えながらイベント設計します
											</p>
										</div>
									</div>
								</li>
								<li>
									<div class="grp-head">
										<div class="number">
											<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/hash03.svg" alt="#03">
										</div>
										<p class="heading01">
											豊富な実績で<br>
											しっかりサポート
										</p>
									</div>
									<div class="grp-card">
										<div class="image">
											<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec01_lst_img01.jpg" alt="">
										</div>
										<div class="grp-card-bottom">
											<p class="heading02">
												次に繋がる<br>
												アフターフォロー
											</p>
											<p class="sentence">
												イベント実施後はアンケートを行い、次回イベントをより良いものにするサポートを行います
											</p>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="sec-in-bottom">
					<div class="btn__area">
						<a href="">
							<span>自分が担当するプロジェクトの</span>
							相談をしてみる
						</a>
						<div class="btn_anim">
							<img class="anim1" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/txt_02.svg" alt="プロジェクトについて相談してみる">

						</div>
						<img class="anim2" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/m.svg" alt="キャラクター">

					</div>
					<p class="bk1-1">MERIT</p>
					<img class="bk1-2" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/01_text.svg" alt="社内イベントは楽しくなくちゃ">
				</div>
			</section>
			<!-- section02 -->
			<section class="sec sec02">
				<div class="sec-in">
					<!-- heading -->
					<div class="area-head">
						<p class="number">02.</p>
						<h2 class="heading">イベント紹介</h2>
						<p class="english">EVENT</p>
						<p class="lead">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
				</div>
				<!-- オンライン -->
				<div class="sec-in-contents sic1">
					<h3 class="sic1_h3">
						<span>
							オンライン
						</span>
						ON LINE
						<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/icon_01.svg" alt="オンライン">
					</h3>
					<div class="slide_area">
						<div class="slick s2 s2-1">
							<div>
								<!-- オンライン　カード１ -->
								<div class="grp-card gc1">
									<div class="image">
										<img class="num" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/num1-1.png" alt="1/5">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
									</div>
									<div class="grp-card-bottom">
										<h4 class="heading02">
											<span>ON LINE</span>
											運動会
										</h4>
										<p class="sentence">
											コミュニケーションが取れる<br>
											オンラインイベント<br>
											チームでゲームで勝てるよう競いあったり個人戦でチームに貢献したり、一体感やチームワークの向上が期待できます

										</p>
									</div>
								</div>
							</div>
							<div>
								<!-- オンライン　カード２ -->
								<div class="grp-card gc2">
									<div class="image">
										<img class="num" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/num1-2.png" alt="2/5">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
									</div>
									<div class="grp-card-bottom">
										<h4 class="heading02">
											<span>ON LINE</span>
											懇親会
										</h4>
										<p class="sentence">
											参加者の方が会話しやすくレクリエーションを用意したりクイズ大会をいれたり、次に合った時に会話のキッカケのある懇親会をご提案
											もちろん食事の手配もお任せください
										</p>
									</div>
								</div>
							</div>
							<div>
								<!-- オンライン　カード3 -->
								<div class="grp-card gc3">
									<div class="image">
										<img class="num" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/num1-3.png" alt="3/5">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
									</div>
									<div class="grp-card-bottom">
										<h4 class="heading02">
											<span>ON LINE</span>
											ヨガ
										</h4>
										<p class="sentence">
											コミュニケーション能力の高い講師を揃えているから参加者と会話しながら参加者に合わせてできるのが魅力
											始業前や夕礼終りの30分、しっかりイベントとして時間を設けて開催など、細かい調整もできます
										</p>
									</div>
								</div>
							</div>
							<div>
								<!-- オンライン　カード4 -->
								<div class="grp-card gc4">
									<div class="image">
										<img class="num" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/num1-4.png" alt="4/5">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
									</div>
									<div class="grp-card-bottom">
										<h4 class="heading02">
											<span>ON LINE</span>
											ぐるりんく
										</h4>
										<p class="sentence">
											コミュニケーションが取れるようチームで話す時間をあえて設けたプログラム
											選べるゲームを絞ることで価格を抑えた設計で参加者が少人数の場合やお試しとしても最適なプラン
										</p>
									</div>
								</div>
							</div>
							<div>
								<!-- オンライン　カード5 -->
								<div class="grp-card gc5">
									<div class="image">
										<img class="num" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/num1-5.png" alt="5/5">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
									</div>
									<div class="grp-card-bottom">
										<h4 class="heading02">
											<span>ON LINE</span>
											決起会
										</h4>
										<p class="sentence">
											会社の士気を上げる決起会をオンラインで運営
											各事業所を繋いで決起会を運営するにとどまらず、ご要望に合わせて抽選会や表彰式などプラスアルファの提案をします
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- オフライン -->
				<div class="sec-in-contents sic2">
					<h3 class="sic1_h3">
						<span>
							オフライン
						</span>
						OFF LINE
						<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/icon2.svg" alt="オフライン">
					</h3>
					<div class="slide_area">
						<div class="slick s2 s2-2">
							<div>
								<!-- オフライン　カード１ -->
								<div class="grp-card gc1">
									<div class="image">
										<img class="num" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/num2-1.png" alt="1/5">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
									</div>
									<div class="grp-card-bottom">
										<h4 class="heading02">
											<span>ON LINE</span>
											運動会
										</h4>
										<p class="sentence">
											コミュニケーションが取れる<br>
											オフラインイベント<br>
											チームでゲームで勝てるよう競いあったり個人戦でチームに貢献したり、一体感やチームワークの向上が期待できます

										</p>
									</div>
								</div>

							</div>
							<!-- オフライン　カード２ -->
							<div>
								<div class="grp-card gc2">
									<div class="image">
										<img class="num" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/num2-2.png" alt="1/5">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
									</div>
									<div class="grp-card-bottom">
										<h4 class="heading02">
											<span>ON LINE</span>
											運動会
										</h4>
										<p class="sentence">
											コミュニケーションが取れる<br>
											オンラインイベント<br>
											チームでゲームで勝てるよう競いあったり個人戦でチームに貢献したり、一体感やチームワークの向上が期待できます

										</p>
									</div>
								</div>
							</div>
							<!-- オフライン　カード3" -->
							<div>
								<div class="grp-card gc3">
									<div class="image">
										<img class="num" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/num2-3.png" alt="1/5">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
									</div>
									<div class="grp-card-bottom">
										<h4 class="heading02">
											<span>ON LINE</span>
											運動会
										</h4>
										<p class="sentence">
											コミュニケーションが取れる<br>
											オンラインイベント<br>
											チームでゲームで勝てるよう競いあったり個人戦でチームに貢献したり、一体感やチームワークの向上が期待できます

										</p>
									</div>
								</div>
							</div>
							<!-- オフライン　カード4 -->
							<div>
								<div class="grp-card gc4">
									<div class="image">
										<img class="num" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/num2-4.png" alt="1/5">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
									</div>
									<div class="grp-card-bottom">
										<h4 class="heading02">
											<span>ON LINE</span>
											運動会
										</h4>
										<p class="sentence">
											コミュニケーションが取れる<br>
											オンラインイベント<br>
											チームでゲームで勝てるよう競いあったり個人戦でチームに貢献したり、一体感やチームワークの向上が期待できます

										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- オンライン オフライン-->
				<div class="sec-in-contents sic3">
					<h3 class="sic1_h3">
						<span>
							オンライン /　オフライン
						</span>
						HYBRID
						<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/icon3.svg" alt="オンライン">
					</h3>
					<div class="slide_area">
						<div class="slick s2 s2-3">
							<div>
								<!-- オンライン　カード１ -->
								<div class="grp-card gc1">
									<div class="image">
										<img class="num" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/num3-1.png" alt="1/5">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
									</div>
									<div class="grp-card-bottom">
										<h4 class="heading02">
											<span>ON LINE</span>
											運動会
										</h4>
										<p class="sentence">
											コミュニケーションが取れる<br>
											オンラインイベント<br>
											チームでゲームで勝てるよう競いあったり個人戦でチームに貢献したり、一体感やチームワークの向上が期待できます

										</p>
									</div>
								</div>
							</div>
							<div>
								<!-- オンライン　カード２ -->
								<div class="grp-card gc2">
									<div class="image">
										<img class="num" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/num3-2.png" alt="2/5">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
									</div>
									<div class="grp-card-bottom">
										<h4 class="heading02">
											<span>ON LINE</span>
											懇親会
										</h4>
										<p class="sentence">
											参加者の方が会話しやすくレクリエーションを用意したりクイズ大会をいれたり、次に合った時に会話のキッカケのある懇親会をご提案
											もちろん食事の手配もお任せください
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- section03 -->
			<section class="sec sec03">
				<div class="sec-in">
					<!-- heading -->
					<div class="area-head">
						<p class="number">03.</p>
						<h2 class="heading">実例紹介</h2>
						<p class="english">CASE STUDY</p>
						<p class="lead">サポート範囲の広さ、担当者の方に負荷のかからないような手厚いサポートを心がけています</p>
					</div>
					<div class="area-hashtag">
						<p class="heading">HASHTAGS</p>
						<ul class="lst-hashtag">
							<li>#参加型</li>
							<li>#企画</li>
							<li>#運営</li>
							<li>#プログラム</li>
							<li>#大人数</li>
							<li>#少人数</li>
							<li>#パッケージ</li>
							<li>#交流会</li>
							<li>#オンライン</li>
							<li>#オフライン</li>
							<li>#内定式</li>
							<li>#新年会</li>
							<li>#大人数</li>
							<li>#少人数</li>
							<li>#オンボーディング</li>
						</ul>
					</div>
					<div class="area-caseStudy">
						<ul class="lst-caseStudy">
							<li class="card is-01">
								<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec03_lst_img01.jpg">
							</li>
							<li class="card is-02">
								<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec03_lst_img02.jpg">
							</li>
							<li class="card is-03">
								<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec03_lst_img03.jpg">
							</li>
							<li class="card is-04">
								<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec03_lst_img04.jpg">
							</li>
						</ul>
					</div>
					<div class="area-btn">
						<div class="btn btn-more01">
							<a href="">事例紹介をもっと見る</a>
						</div>
					</div>
				</div>
				<div class="area-corporate">
					<p class="heading">その他のご利用企業様</p>
					<div class="slider_wrap">
						<div class="slider slider01">
							<ul class="slick01">
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img02.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img02.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
							</ul>
						</div>
						<div class="slider slider02">
							<ul class="slick01">
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img02.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img02.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
							</ul>
						</div>
					</div>
					<div class="slider_wrap__bottom">
						<div class="btn__area">
							<a href="">
								<span>自分が担当するプロジェクトの</span>
								相談をしてみる
							</a>
							<div class="btn_anim">
								<img class="anim1" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/txt_02.svg" alt="プロジェクトについて相談してみる">

							</div>
							<img class="anim2" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/m.svg" alt="キャラクター">

						</div>
					</div>

				</div>
				<img class="bk3-2" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/01_text.svg" alt="社内イベントは楽しくなくちゃ">
			</section>

			<!-- section04 -->
			<section class="sec sec04">
				<div class="sec-in">
					<!-- heading -->
					<div class="area-head">
						<p class="number">04.</p>
						<h2 class="heading">ご利用の流れ</h2>
						<p class="english">FLOW</p>
						<p class="lead">サポート範囲の広さ、担当者の方に負荷のかからないような手厚いサポートを心がけています</p>
					</div>
					<!-- flow -->
					<div class="area-flow">
						<ul class="lst-flow">
							<li class="is-01">
								<div class="number">
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/step1.svg" alt="STEP1">
								</div>
								<div class="area-txt">
									<p class="heading">お問い合わせ・ヒアリング</p>
									<p class="sentence">
										お問い合わせフォームにてお気軽にご連絡ください。内容を確認後、弊社よりご連絡いたします。イベントの目的、ご予算、日程などの詳細を確認させていただきます。
									</p>
								</div>
							</li>
							<li class="is-02">
								<div class="number">
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/step2.svg" alt="STEP2">
								</div>
								<div class="area-txt">
									<p class="heading">ご提案</p>
									<p class="sentence">
										ヒアリングさせていただいた内容を基に最適なご提案をさせていただきます。<br class="br-pc">
										イベントのストーリー・構成、プログラム、演出などの企画内容とお見積りをご提示いたします。
									</p>
								</div>
							</li>
							<li class="is-03">
								<div class="number">
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/step3.svg" alt="STEP3">
								</div>
								<div class="area-txt">
									<p class="heading">ご契約</p>
									<p class="sentence">
										ご提案内容にご納得いただけましたら、ご契約ください。
									</p>
								</div>
							</li>
							<li class="is-04">
								<div class="number">
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/step4.svg" alt="STEP4">
								</div>
								<div class="area-txt">
									<p class="heading">イベント企画</p>
									<p class="sentence">
										本番に向け、お客様と共にイベントを創り上げていきます。演出・コンテンツの決定、手配、製作などのタスクをスケジュール管理し、本番まで漏れのないようプロジェクトを進めます。
									</p>
								</div>
							</li>
							<li class="is-05">
								<div class="number">
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/step5.svg" alt="STEP5">
								</div>
								<div class="area-txt">
									<p class="heading">本番</p>
									<p class="sentence">
										設営、リハーサル、進行、音響、映像などすべてプロのスタッフにお任せください。<br class="br-pc">
										イベントを通して参加者の皆様に最大級の感動をお届けいたします。
									</p>
								</div>
							</li>
							<li class="is-06">
								<div class="number">
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/step6.svg" alt="STEP6">
								</div>
								<div class="area-txt">
									<p class="heading">振り返り</p>
									<p class="sentence">
										イベント終了後にアンケートを実施し、目的の達成度、参加者の満足度などを振り返ります。成功のポイントと今後のイベントへのヒント、課題を抽出します。
									</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</section>

			<!-- section05 -->
			<section class="sec sec05">
				<div class="sec-in">
					<!-- heading -->
					<div class="area-head">
						<p class="number">05.</p>
						<h2 class="heading">選ばれる理由</h2>
						<p class="english">REASON</p>
						<p class="lead">サポート範囲の広さ、担当者の方に負荷のかからないような手厚いサポートを心がけています</p>
					</div>
					<!-- REASON -->
					<div class="area-reason">
						<ul class="lst-reason">
							<li>
								<div class="image">
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_slider_img01.png">
								</div>
								<div class="caption">
									<p class="sentence">
										目的に合わせた<br>
										オリジナルなイベント設計
									</p>
								</div>
							</li>
							<li>
								<div class="image">
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_slider_img02.png">
								</div>
								<div class="caption">
									<p class="sentence">
										約2000社の実績で<br>
										ノウハウが豊富
									</p>
								</div>
							</li>
							<li>
								<div class="image">
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_slider_img03.png">
								</div>
								<div class="caption">
									<p class="sentence">
										寄り添った<br>
										営業
									</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</section>
			<!-- section06 -->
			<section class="sec sec06">

				<div class="sp_contents">
					<img class="bk6-1" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bk6-1.png" alt="背景１">
				</div>
				<div class="sec-in-wrap">

					<div class="sec-in">
						<!-- heading -->
						<div class="area-head">
							<p class="number">06.</p>
							<h2 class="heading">ご利用料金</h2>
							<p class="english">price</p>
							<p class="lead">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
						</div>
						<div class="sec-in-contents">
							<div class="slick s6">
								<div>
									<div class="price_contents">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img6-1.jpg" alt="">
									</div>
								</div>
								<div>
									<div class="price_contents">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img6-1.jpg" alt="">
									</div>
								</div>
								<div>
									<div class="price_contents">
										<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img6-1.jpg" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="sp_contents">
					<img class="bk6-2" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bk6-2.png" alt="背景１">
				</div>
				<div class="sec-in-bottom">
					<div class="btn__area">
						<a href="">
							<span>自分が担当するプロジェクトの</span>
							相談をしてみる
						</a>
						<div class="btn_anim">
							<img class="anim1" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/txt_02.svg" alt="プロジェクトについて相談してみる">

						</div>
						<img class="anim2" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/m.svg" alt="キャラクター">

					</div>
				</div>
			</section>

			<!-- section07 -->
			<section class="sec sec07">
				<div clasS="sec07_anim">
					<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/mr_m_06.svg" alt="思い出に残るイベントで思い出に残そう">
				</div>
				<p class="bk7-11">
					<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/column07.svg" alt="colamun">

				</p>
				<div class="sec-in">
					<!-- heading -->
					<div class="area-head">
						<p class="number">07.</p>
						<h2 class="heading">コラム</h2>
						<p class="english">column</p>
						<p class="lead">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
					<div class="sec-in-contents">
						<!-- コラム１ -->
						<div class="grp-card ">
							<div class="image">
								<span>NEW</span>

								<img class="img7-1" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
							</div>
							<div class="grp-card-bottom">
								<p class="day">2021.02.27</p>
								<dl>
									<dt>CATEGORY：</dt>
									<dd>
										<span>研修</span>
									</dd>
								</dl>
								<h3 class="heading02">
									国籍を越える「オンライン運動会」〜言葉を超えたコミュニケーションイベント〜
								</h3>
								<p class="sentence">
									みなさん、こんにちは！運動会プロジェクトです。
									今回の記事は運動会プロジェクトが自慢する某大学の教育学部在学中のアルバイトスタッフが作成した…
								</p>
								<ul>
									<li><a href="">#運動会</a></li>
									<li><a href="">#オンライン運動会</a></li>
								</ul>
								<a href="" class="read-more">READ MORE</a>
							</div>

						</div>
						<!-- コラム１ -->
						<div class="grp-card ">
							<div class="image">
								<span>NEW</span>

								<img class="img7-1" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/img_02_01.jpg" alt="">
							</div>
							<div class="grp-card-bottom">
								<p class="day">2021.02.27</p>
								<dl>
									<dt>CATEGORY：</dt>
									<dd>
										<span>研修</span>
									</dd>
								</dl>
								<h3 class="heading02">
									国籍を越える「オンライン運動会」〜言葉を超えたコミュニケーションイベント〜
								</h3>
								<p class="sentence">
									みなさん、こんにちは！運動会プロジェクトです。
									今回の記事は運動会プロジェクトが自慢する某大学の教育学部在学中のアルバイトスタッフが作成した…
								</p>
								<ul>
									<li><a href="">#運動会</a></li>
									<li><a href="">#オンライン運動会</a></li>
								</ul>
								<a href="" class="read-more">READ MORE</a>
							</div>

						</div>
					</div>

					<a class="more-btn" href="<?php echo home_url(); ?>/columns/?tags=all">
						コラムをもっと見る
					</a>
				</div>

				<div class="bk7-1"></div>
				<div class="bk7-2"></div>
			</section>
			<!-- section08 -->
			<section class="sec sec08">
				<div class="sec-in">
					<!-- heading -->
					<div class="area-head">
						<p class="number">08.</p>
						<h2 class="heading">メディア掲載</h2>
						<p class="english">Media</p>
						<p class="lead">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>


				</div>
				<div class="area-corporate">

					<div class="slider_wrap">
						<div class="slider slider01">
							<ul class="slick01">
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img02.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img02.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
							</ul>
						</div>
						<div class="slider slider02">
							<ul class="slick01">
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img02.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img02.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
								<li>
									<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/bnr_corporate_img01.jpg">
								</li>
							</ul>
						</div>
					</div>
					<div class="slider_wrap__bottom">
						<div class="btn__area">
							<a href="">
								<span>自分が担当するプロジェクトの</span>
								相談をしてみる
							</a>
							<div class="btn_anim">
								<img class="anim1" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/txt_02.svg" alt="プロジェクトについて相談してみる">

							</div>
							<img class="anim2" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/m.svg" alt="キャラクター">

						</div>
					</div>

				</div>
				<div class="sec08_anim">
					<img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/Media.svg" alt="Media">
				</div>
				<img class="bk8-2" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/01_text.svg" alt="社内イベントは楽しくなくちゃ">
			</section>
			<!-- section09 -->
			<section class="sec sec09">
				<div class="sec-in">
					<!-- heading -->
					<div class="area-head">
						<p class="number">09.</p>
						<h2 class="heading">よくある質問</h2>
						<p class="english">REASON</p>
						<p class="lead">サポート範囲の広さ、担当者の方に負荷のかからないような手厚いサポートを心がけています</p>
					</div>
					<!-- QA -->
					<div class="area-accordion">
						<input type="checkbox" id="check1" class="accordion-hidden" checked="checked">
						<label for="check1" class="accordion-open">
							<span class="qa"><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_icon_question.svg"></span>
							<span class="sentence">企画が不要なときはイベントの進行・運営だけを頼むことはできますか？</span>
						</label>
						<label for="check1" class="accordion-close">
							<span class="qa"><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_icon_answer.svg"></span>
							<span class="sentence">もちろん可能です。配信方法がわからないなど小さなことでも結構ですので、まずはお気軽にご相談ください。</span>
						</label>

						<input type="checkbox" id="check2" class="accordion-hidden">
						<label for="check2" class="accordion-open">
							<span class="qa"><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_icon_question.svg"></span>
							<span class="sentence">どのようなオンライン会議ツールがイベントに適していますか？</span>
						</label>
						<label for="check2" class="accordion-close">
							<span class="qa"><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_icon_answer.svg"></span>
							<span class="sentence">もちろん可能です。配信方法がわからないなど小さなことでも結構ですので、まずはお気軽にご相談ください。</span>
						</label>

						<input type="checkbox" id="check3" class="accordion-hidden">
						<label for="check3" class="accordion-open">
							<span class="qa"><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_icon_question.svg"></span>
							<span class="sentence">イベント本番のどれくらい前に相談すればよいですか？</span>
						</label>
						<label for="check3" class="accordion-close">
							<span class="qa"><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_icon_answer.svg"></span>
							<span class="sentence">もちろん可能です。配信方法がわからないなど小さなことでも結構ですので、まずはお気軽にご相談ください。</span>
						</label>

						<input type="checkbox" id="check4" class="accordion-hidden">
						<label for="check4" class="accordion-open">
							<span class="qa"><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_icon_question.svg"></span>
							<span class="sentence">自社内からの配信はできますか？</span>
						</label>
						<label for="check4" class="accordion-close">
							<span class="qa"><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_icon_answer.svg"></span>
							<span class="sentence">もちろん可能です。配信方法がわからないなど小さなことでも結構ですので、まずはお気軽にご相談ください。</span>
						</label>

						<input type="checkbox" id="check5" class="accordion-hidden">
						<label for="check5" class="accordion-open">
							<span class="qa"><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_icon_question.svg"></span>
							<span class="sentence">開催に必要な機材は用意してもらえますか？</span>
						</label>
						<label for="check5" class="accordion-close">
							<span class="qa"><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_icon_answer.svg"></span>
							<span class="sentence">もちろん可能です。配信方法がわからないなど小さなことでも結構ですので、まずはお気軽にご相談ください。</span>
						</label>

						<input type="checkbox" id="check6" class="accordion-hidden">
						<label for="check6" class="accordion-open">
							<span class="qa"><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_icon_question.svg"></span>
							<span class="sentence">予算の上限が決まっているのですが、その範囲で提案は可能ですか？</span>
						</label>
						<label for="check6" class="accordion-close">
							<span class="qa"><img src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/sec05_icon_answer.svg"></span>
							<span class="sentence">もちろん可能です。配信方法がわからないなど小さなことでも結構ですので、まずはお気軽にご相談ください。</span>
						</label>
					</div>
				</div>
			</section>
			<!-- section10 -->
			<div class="sec sec10 ">

				<div class="sec-in sec_contact">
					<!-- heading -->
					<div class="area-head">
						<p class="number">10.</p>
						<h2 class="heading">お問い合わせ</h2>
						<p class="english">CONTACT</p>
						<p class="lead">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
					<?php echo do_shortcode('[contact-form-7 id="6" title="みんコミュ　お問い合わせ"]'); ?>
				</div>

				<div class="btn__area">
					<a href="" class="sec10_btn">
						<span>自分が担当するプロジェクトの</span>
						相談をしてみる
					</a>
					<div class="btn_anim">
						<img class="anim1" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/txt_02.svg" alt="プロジェクトについて相談してみる">

					</div>
					<img class="anim2" src="<?php echo get_template_directory_uri(); ?>/src/img/mincommu/m.svg" alt="キャラクター">

				</div>
			</div>

		</main>
	</div>
</div>

<script>
	$(function() {
		$(".sec10_btn").click(function() {
			$("input#pt-9").click()
			return false;
		});
		$(".anim_move p").addClass("on")
	})
</script>
<?php get_footer('mincommu'); ?>