<?php get_header(); ?>
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
<?php get_template_part( 'partial/content', 'title' ); ?>
          <div class="editor">
<?php  the_content(); ?>
          </div>
          <!-- /.editor-->
<?php endwhile; endif; ?>
<?php get_footer(); ?>
