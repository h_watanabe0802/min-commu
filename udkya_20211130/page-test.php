<?php get_header(); ?>
<?php 
  $tapes = get_posts(array( 'post_type' => 'goaltape-words'));
  $tape_words = array();
  foreach($tapes as $tape) {
    $tape_words[] = get_post_meta( $tape->ID, 'top-display-words', true );
  }
  shuffle($tape_words);
  $news = get_posts(array( 'post_type' => 'post' , 'posts_per_page' => 3));
  $top_post = get_post( get_option( 'sticky_posts' )[0] );
  if( empty($top_post) ) {
    $top_post = $news[0];
  }
  $news_categories = [];
  foreach($news as $new) {
    $cat = current(get_the_category($new->ID));
    if($cat->parent == 0) {
      $news_categories[] = $cat;
    }
    else {
      $news_categories[] = get_category( $cat->parent );
    }
  }
  $abouts_data = get_about_options();
?>
          <div class="mainvisual release-background lazyload">
            <div class="mainvisual__outer">
              <div class="mainvisual__inner">
                <h2 class="mainvisual__title">
                  <picture>
                    <source media="(min-width: 1024px)" srcset="<?= get_template_directory_uri() ?>/src/img/index/kv_tx01.png">
                    <source media="(min-width: 768px)" srcset="<?= get_template_directory_uri() ?>/src/img/index/kv_tx01_sp.png"><img src="<?= get_template_directory_uri() ?>/src/img/index/kv_tx01_sp.png" alt="社内/組合のイベントでお困りのアナタ！">
                  </picture>
                </h2>
                <!-- /.mainvisual__title-->
                <div class="mavinvisual-content">
                  <div class="mainvisual__people">
                    <!-- /.mainvisual__people-group-->
                    <div class="mainvisual__people-person lazyload">
                      <ul class="flex-middle">
                        <li class="person01 -a" id="js-person01"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_template_directory_uri() ?>/src/img/index/kv_tx02.png" alt="一体感出すイベントなら絶対運動会やろ！"></li>
                        <li class="person02 -a" id="js-person02"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_template_directory_uri() ?>/src/img/index/kv_tx03.png" alt="いやいや運動会以外でもみんなとつながれるはず！"></li>
						<li class="person03 -a" id="js-person03"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_template_directory_uri() ?>/src/img/index/kv_tx04.png" alt="More Ideas!まずは相談を"></li>
						  <li class="person04 -a" id="js-person04">
							  <a href="https://www.udkya.com/service-price"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_template_directory_uri() ?>/src/img/index/kv_bt01.png" alt="運動会についてはこちら"></a></li>
						  <li class="person05 -a" id="js-person05">
							  <a href="https://www.min-commu.net/" target="_blank"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_template_directory_uri() ?>/src/img/index/kv_bt02.png" alt="サービス一覧はこちら"></a></li>
                      </ul>
                    </div>
                    <!-- /.mainvisual__people-person-->
                  </div>
                  <!-- /.mainvisual__people-->
                  <!-- /.mainvisual__text-->
                  <div class="mainvisual__banner flex-middle">
                    <div class="mainvisual__banner-title">
                      <dl>
                        <dt><a href="<?= get_the_permalink( $top_post ) ?>"><?= get_the_date( 'Y.m.d', $top_post ) ?></a></dt>
                        <dd><a href="<?= get_the_permalink( $top_post ) ?>"><?= $top_post->post_title ?></a></dd>
                      </dl>
                    </div>
                    <div class="mainvisual__banner-news"><a class="flex-middle-center" href="/news"><span><?= $news_categories[0]->cat_name ?></span><svg class="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 5.5 10" style="enable-background:new 0 0 5.5 10;" xml:space="preserve">
<path class="st0" d="M0.5,10c-0.1,0-0.3-0.1-0.4-0.2c-0.2-0.2-0.2-0.6,0-0.8L4.2,5L0.2,0.9c-0.2-0.2-0.2-0.6,0-0.8s0.6-0.2,0.8,0 l4.5,4.5C5.5,4.7,5.5,4.9,5.5,5S5.5,5.3,5.4,5.4L0.9,9.8C0.8,9.9,0.7,10,0.5,10z" />
</svg></a></div>
                  </div>
                </div>
                <!-- /.mavinvisual-content-->
              </div>
            </div>
          </div>
          <!-- /.mainvisual-->

<div class="mediabx">
			<picture>
				<source media="(min-width: 1024px)" srcset="<?= get_template_directory_uri() ?>/src/img/index/ttl_media.png">
                    <source media="(min-width: 768px)" srcset="<?= get_template_directory_uri() ?>/src/img/index/ttl_media_sp.png">
				<img src="<?= get_template_directory_uri() ?>/src/img/index/ttl_media_sp.png" alt="メディア実績多数">
				</picture>
	<div class="medialist">
	<picture>
		<source media="(min-width: 1024px)" srcset="<?= get_template_directory_uri() ?>/src/img/index/media_list.png" alt="メディア実績">
		<source media="(min-width: 768px)" srcset="<?= get_template_directory_uri() ?>/src/img/index/media_list_sp.png" alt="メディア実績">
		<img src="<?= get_template_directory_uri() ?>/src/img/index/media_list_sp.png" alt="メディア実績多数">
	</picture>
	</div>
	
			</div>

	

			  			<div class="trouble_nav">
							  <p>
								  お困りごとはありますか？
							  </p>
				      <ul>
      <li><a href="https://www.min-commu.net/" target="_blank"><img src="https://www.udkya.com/wordpress/wp-content/uploads/2021/04/tp_gr_bn.png" alt="オンラインイベントを開催したい"></a></li>
      <li><a href="https://online.udkya.com/" target="_blank"><img src="https://www.udkya.com/wordpress/wp-content/uploads/2021/04/tp_bl_bn.png" alt="オンライン運動会について知りたい"></a></li>
      <li><a href="https://neo.udkya.com/"  target="_blank"><img src="https://www.udkya.com/wordpress/wp-content/uploads/2021/04/tp_rd_bn.png?Ver=1.1" alt="感染対策について"></a></li>
						        <li><a href="https://rescue.udkya.com/" target="_blank"><img src="https://www.udkya.com/wordpress/wp-content/uploads/2021/04/tp_yl_bn.png" alt="社内イベントについての悩みを解決したい"></a></li>
    </ul>

<style>
.tp_evevt-search{
margin:40px auto;
text-align: center;
}
.tp_event-btn {
  display: inline-block;
  max-width: 640px;
width: 100%;

  background-color: #e60000;
  font-size: 1.5em;
  color: #fff;
  text-decoration: none;
  font-weight: bold;
  padding: 1.5em 10px;
  border-radius: 4px;
}
.tp_event-btn:hover {
  opacity: 0.6;
}
	@media only screen and (max-width: 768px) {
		.tp_event-btn {

  font-size: 1.3em;

  padding: 1em 10px;

}
	}
</style>
<div class="tp_evevt-search">
<button onclick="newwindow()" class="tp_event-btn">あなたにピッタリのイベントは？</button><br>
<script type="text/javascript">

function newwindow() {
window.open("https://www.udkya.com/event/yes-no/#q_01" , null, 'top=100,left=100,width=800,height=500,status=0');
}
</script>
</div>
<style>

.ev-schedule{
margin:5em auto 80px;
	padding:0;
}
.ev-schedule p{
text-align: center;
		margin:1em auto .5em;
padding:0;
font-size:1.2em;
}
	.ev-schedule table{
		max-width:800px;
		margin:40px auto;
		border: 2px solid #000;
		text-align: left;
		font-size:1.1em;
	}
		.ev-schedule table th{
		padding: .5em 1em;
			width: 35%;
			border-right: 1px solid #000;
			border-bottom: 1px solid #000;
	}
			.ev-schedule table td{
		padding: .5em 1em;
border-bottom: 1px solid #000;
	}
	.top_spbr{
		display: none;
	}
@media screen and (max-width: 768px) {
	.ev-schedule{

		height:auto;

}

		.top_spbr{
		display: block;
	}
}
	.attention-fraudsite{
margin:2em auto 0;
		font-size: 1.5em;
			text-align: center;
	padding:0;
}
	@media screen and (max-width: 768px) {
			.attention-fraudsite{

		font-size: 1.2em;
				text-align:left;
	padding:0;
		}
	}
</style>
							<div class="ev-schedule">
								<h2 class="about__header-title">＜次回イベント無料体験会＞</h2>
								<table>
							
																		                        <tr>
                            <th>12月7日（火）14:00～15:30</th>
							<td><a href="https://www.udkya.com/service-price/trial">少人数コミュニケーション特化型プログラムオンライン運動会体験会</td>
                        </tr>
                        
								</table>
	<a href="https://www.udkya.com/service-price/trial"><img src="https://www.udkya.com/wordpress/wp-content/uploads/2021/08/trial−banner.jpg" alt="運動会体験会"></a>



</div>
										 	<div class="top_online">
			 <a href="https://www.udkya.com/party-revolution2020-2021"><img src="https://www.udkya.com/wordpress/wp-content/uploads/2021/01/konshinkai.png" alt="オンライン懇親会革命"></a><br>


				  			  </div> 

						
							<div class="attention-fraudsite">
								<a href="https://www.undokai.co.jp/fraudsite" target="_blank" rel="noopener">※当社を騙る詐欺サイトにご注意ください</a>
							</div>
	</div>


					
		           <div class="service release-background lazyload">   
			  <a class="service__person lazyload" href="<?= get_page_link( get_page_by_title( 'サービス・料金' )) ?>#reason"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/service-person.png?mod=01" alt="組織を強くする理由を私が教えます"></a>
            <div class="service__outer release-background">
              <div class="service__inner">
                <div class="service__header">
                  <h2 class="service__header-title">サービス・料金</h2>
                  <p class="service__header-subtitle">運動会開催に向けた運動会屋の<span>サービスについての詳細です</span></p>
                </div>
                <!-- /.service__header-->
                <div class="service__content">
                  <div class="service__list flex-middle">
                    <dl class="service__list-dl -flow">
                      <dt><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/icon-baton.svg" alt="バトンのアイコン"></dt>
                      <dd>開催までの流れ</dd>
                    </dl>
                    <dl class="service__list-dl -plan">
                      <dt><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/icon-plan.svg" alt="プランのアイコン"></dt>
                      <dd>プラン</dd>
                    </dl>
                    <dl class="service__list-dl -price">
                      <dt><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/icon-money.svg" alt="ドル袋のアイコン"></dt>
                      <dd>料金</dd>
                    </dl>
                  </div>
                  <div class="service__detail flex-middle-center lazyload"><a class="button lazyload flex-middle-center -white" href="<?= get_page_link( get_page_by_title( 'サービス・料金' )) ?>"><span class="button__text">詳細はこちら</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></a></div>
                </div>
                <!-- /.service__content-->
              </div>
            </div>
            <!-- /.service__outer-->
            <div class="service__tug-of-war release-element">
              <div class="service__tug-of-war-content flex-top-between">
                <ul class="service__tug-of-war-left">
                  <li class="-fukidashi lazyload"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/tugofwar-fukidashi-left.svg" alt="綱引きの掛け声（左）"></li>
                  <li class="-people lazyload"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/tugofwar-left.png" alt="綱引きの画像（左）"></li>
                </ul>
                <ul class="service__tug-of-war-right">
                  <li class="-fukidashi lazyload"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/tugofwar-fukidashi-right.svg" alt="綱引きの掛け声（右）"></li>
                  <li class="-people lazyload"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/tugofwar-right.png" alt="綱引きの画像（右）"></li>
                </ul>
              </div>
            </div>
            <div class="service__sdgs"><a href="<?= get_page_link( get_page_by_title( 'SDGs' )) ?>">
                <div class="service__sdgs-badge">
                  <picture>
                    <source media="(min-width: 1280px)" data-srcset="<?= get_template_directory_uri() ?>/src/img/index/badge-sdgs.svg"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/badge-sdgs-small.svg" alt="SUSTINABLE DEVELOPMENT GOALS">
                  </picture>
                </div>
                <p class="service__sdgs-text">運動会の開催における<br>SDGsの取り組み</p></a></div>
            <!-- /.service__sdgs-->
			  <style>
				  .top_online{
					  text-align: center;
					  margin:120px auto 0;
				  }
				  				  .top_online img{
									  max-width:640px;
									  width:100%;
									  height:auto;
				    -webkit-backface-visibility: hidden;
				  }

				  @media only screen and (max-width: 768px) {
					  				  .top_online{
					  margin:60px auto 0;
				  }
			  </style>
			 	 <!--  <div class="top_online">
			 <a href="https://www.udkya.com/service-price/event-gl" target="_blank" rel="noopener noreferrer"><img src="https://www.udkya.com/wordpress/wp-content/uploads/2020/08/event-gl.png" alt="感染予防対策ガイドラインに則ったイベント運営"></a><br>
			                   <a href="https://www.udkya.com/service-price/online" target="_blank" rel="noopener noreferrer"><img src="https://www.udkya.com/wordpress/wp-content/uploads/2020/06/online_bn.jpg" alt="オンライン運動会"></a>
				  			  </div> -->
          </div>

          <!-- /.service-->
          <div class="about release-element">
            <div class="about__container">
              <div class="about__outer release-background">
                <div class="about__inner">
                  <div class="about__header lazyload">
                    <h2 class="about__header-title">運動会屋とは</h2>
                  </div>
                  <div class="about__content">
                    <div class="about__content-vertical lazyload">
                      <ul class="flex-top-between">
                        <li><span>運動会屋は</span></li>
                        <li><span>年間２００件以上の</span></li>
                        <li><span>企業や団体様のイベントを企画・運営・</span></li>
                        <li><span>プロデュースしています。</span></li>
                        <li><span>経験と実績があるからこそ、</span></li>
                        <li><span>どのような要望にもおこたえでき、</span></li>
                        <li><span>唯一無二のイベントを</span></li>
                        <li><span>みなさまとともに</span></li>
                        <li><span>作ることができます。</span></li>
                      </ul>
                      <div class="about__content-logo"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/logo-large.svg" alt="運動会屋ロゴ"></div>
                    </div>
                    <!-- /.about__content-vertical-->
                    <div class="about__detail flex-middle-center lazyload"><a class="button lazyload flex-middle-center -white" href="<?= get_page_link( get_page_by_title( '運動会屋とは' )) ?>"><span class="button__text">詳細はこちら</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></a></div>
                    <!-- /.about__detail-->
                    <div class="about__lead lazyload">
                      <p class="about__lead-content">私たちが運動会屋です。</p>
                    </div>

<?php
$base_path = trailingslashit(get_template_directory());
$dir = 'src/img/index/';
$center_images = glob($base_path.$dir.'about-person-center*.png');
$side_images = glob($base_path.$dir.'about-person-side*.png');
shuffle($center_images);
shuffle($side_images);
?>
                    <div class="about__result flex-center-wrap">
                      <div class="about__result-content">
                        <div class="about__result-circle lazyload">
                          <div class="about__result-image">
                            <picture>
                              <source media="(min-width: 1024px)" data-srcset="<?= get_template_directory_uri() ?>/src/img/index/icon-group-black.svg"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_template_directory_uri() ?>/src/img/index/icon-group-white.svg" alt="運動会に参加する人々の画像">
                            </picture>
                          </div>
                          <div class="about__result-label"><b><?= $abouts_data[0]['setting_title'] ?></b></div>
                          <div class="about__result-number"><span><?= $abouts_data[0]['setting_prefix'] ?></span><b><?= $abouts_data[0]['setting_number'] ?></b><span><?= $abouts_data[0]['setting_suffix'] ?></span></div>
                          <div class="about__result-date"><?= $abouts_data[0]['setting_small'] ?></div>
                        </div>
                        <div class="about__result-person lazyload"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_theme_file_uri($dir.basename($side_images[0])) ?>?2021012522" alt="運動会屋のスタッフ写真01"></div>
                      </div>
                      <div class="about__result-content">
                        <div class="about__result-person lazyload"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_theme_file_uri($dir.basename($center_images[0])) ?>?2021012522" alt="運動会屋のスタッフ写真02"></div>
                      </div>
                      <div class="about__result-content">
                        <div class="about__result-circle lazyload">
                          <div class="about__result-image"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_template_directory_uri() ?>/src/img/index/icon-hold-white.svg" alt="玉入れの画像"></div>
                          <div class="about__result-label"><b><?= $abouts_data[1]['setting_title'] ?></b></div>
                          <div class="about__result-number"><span><?= $abouts_data[1]['setting_prefix'] ?></span><b><?= $abouts_data[1]['setting_number'] ?></b><span><?= $abouts_data[1]['setting_suffix'] ?></span></div>
                          <div class="about__result-date"><?= $abouts_data[1]['setting_small'] ?></div>
                        </div>
                        <div class="about__result-person lazyload"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_theme_file_uri($dir.basename($center_images[1])) ?>?2021012522" alt="運動会屋のスタッフ写真03"></div>
                      </div>
                      <div class="about__result-content">
                      <div class="about__result-person lazyload"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_theme_file_uri($dir.basename($center_images[2])) ?>?2021012522" alt="運動会屋のスタッフ写真04"></div>
                      </div>
                      <div class="about__result-content">
                        <div class="about__result-circle lazyload">
                          <div class="about__result-image">
                            <picture>
                            <source media="(min-width: 1024px)" data-srcset="<?= get_template_directory_uri() ?>/src/img/index/icon-hachimaki-black.svg"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_template_directory_uri() ?>/src/img/index/icon-hachimaki-white.svg" alt="ハチマキの画像">
                            </picture>
                          </div>
                          <div class="about__result-label"><b><?= $abouts_data[2]['setting_title'] ?></b></div>
                          <div class="about__result-number"><span><?= $abouts_data[2]['setting_prefix'] ?></span><b><?= $abouts_data[2]['setting_number'] ?></b><span><?= $abouts_data[2]['setting_suffix'] ?></span></div>
                          <div class="about__result-date"><?= $abouts_data[2]['setting_small'] ?></div>
                          <div class="about__result-slogan">
                          <div class="about__result-slogan-image"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_template_directory_uri() ?>/src/img/index/arse.png" alt="地球アイコン"></div>
                            <div class="about__result-slogan-text">
                              <p>目指せ！</p>
                              <p>世界一周！</p>
                            </div>
                          </div>
                        </div>
                        <div class="about__result-person lazyload"><img class="lazyload" src="<?= get_template_directory_uri() ?>/src/img/common/dummy.png" data-src="<?= get_theme_file_uri($dir.basename($side_images[1])) ?>?2021012522" alt="運動会屋のスタッフ写真05"></div>
                      </div>
                    </div>
                    <!-- /.about_result-->
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.about-->
          <div class="voice release-background">
            <div class="voice__outer lazyload">
              <div class="voice__inner">
                <div class="voice__header">
                  <h2 class="voice__header-title"><b>お客様の声</b><b><span class="voice__header-title-en">BEFORE</span><span class="voice__header-title-en -effect"><span>AFTER</span></span></b></h2>
                </div>
                <div class="voice__lead lazyload">
                  <p>運動会屋のイベント実施後、社内の変化についてお話を伺いました</p>
                </div>
                <div class="swiper-container lazyload" id="js-swiper">
                  <div class="swiper-wrapper">
<?php
  $arg = array( 'post_type' => 'voices', 'posts_per_page' => 4 );
  $query = new WP_Query($arg);
  if ( $query->have_posts()) : while ($query->have_posts()) : $query->the_post(); 
?>
                    <div class="swiper-slide card">
<?php get_template_part( 'partial/card', 'voice' ); ?>
                    </div>
<?php endwhile; endif; wp_reset_postdata(); ?>
                  </div>
                  <!-- /.swiper-wrapper-->
                  <div class="swiper-pagination" id="js-swiper-pagination"></div>
                </div>
                <!-- /.swiper-container-->
                <div class="voice__detail flex-middle-center lazyload"><a class="button lazyload flex-middle-center -black" href="<?= get_post_type_archive_link( 'voices' ) ?>"><span class="button__text">記事をもっと見る</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></a></div>
              </div>
            </div>
          </div>
          <!-- /.voice-->
          <div class="column release-background lazyload">
            <div class="column__outer lazyload">
              <div class="column__inner">
                <div class="column__header">
                  <h2 class="column__header-title">コラム</h2>
                  <div class="column__header-image lazyload"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/title-times.svg" alt="運動会屋タイムズ"></div>
                </div>
                <!-- / .column__header-->
                <div class="column__lead lazyload">
                  <p>運動会屋が発信するコラムです</p>
                </div>
                <!-- /.column__lead-->
                <div class="column__tag">
                  <ul class="tag">
                    <?= get_all_tags_list(25) ?>
                  </ul>
                </div>
                <!-- /.column_tag-->
                <div class="carousel">
                  <div class="swiper-container lazyload" id="js-swiper-carousel">
                    <div class="swiper-wrapper">
<?php
  $arg = array( 'post_type' => 'columns', 'posts_per_page' => 4 );
  $query = new WP_Query($arg);
  if ( $query->have_posts()) : while ($query->have_posts()) : $query->the_post();
?>
<?php get_template_part( 'partial/card', 'article-swiper' ); ?>

<?php endwhile; endif; wp_reset_postdata(); ?>
                    </div>
                    <!-- /.swiper-wrapper-->
                    <div class="swiper-pagination" id="js-swiper-carousel-pagination"></div>
                  </div>
                  <!-- /.swiper-container-->
                </div>
                <!-- /.carousel-->
                <div class="column__detail flex-middle-center lazyload"><a class="button lazyload flex-middle-center -black" href="<?= get_post_type_archive_link( 'columns' ) ?>"><span class="button__text">記事をもっと見る</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></a></div>
              </div>
            </div>
          </div>
          <!-- /.column-->
          <div class="faq release-background">
            <div class="faq__outer lazyload">
              <div class="faq__inner">
                <div class="faq__header">
                  <h2 class="column__header-title">よくある質問</h2>
                </div>
                <!-- / .faq__header-->
<?php if( !empty(get_option('setting_faq_category_num')) ) : ?>
                <div class="faq__category tab">
                  <ul class="flex-center">
                    <?= get_faq_list() ?>
                  </ul>
                </div>
                <!-- /.faq__category-->
<?php endif; ?>
                <div class="faq__content">
                  <div class="faq__accordion accordion" id="js-accordion">
<?php
  $arg = array( 'post_type' => 'faq', 'posts_per_page' => 3, 'orderby' => 'rand' );
  $query = new WP_Query($arg);
  if ( $query->have_posts()) : while ($query->have_posts()) : $query->the_post(); 
?>
<?php get_template_part( 'partial/card', 'faq' ); ?>
<?php endwhile; endif; wp_reset_postdata(); ?>
                  </div>
                </div>
                <!-- /.faq__content-->
                <div class="faq__detail flex-middle-center lazyload"><a class="button lazyload flex-middle-center -black" href="<?= get_post_type_archive_link( 'faq' ) ?>"><span class="button__text">質問をもっと見る</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></a></div>
              </div>
            </div>
          </div>
          <!-- /.faq-->
          <div class="news release-background">
            <div class="news__outer release-element">
              <ul class="news__flag">
                <li class="-left"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/flag-small-left.svg" alt="小さい旗左">></li>
                <li class="-right"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/flag-small-right.svg" alt="小さい旗右"></li>
              </ul>
              <div class="news__inner">
                <div class="news__header">
                  <h2 class="news__header-title">
                    <p class="-en">NEWS</p>
                    <p class="-ja">お知らせ</p>
                  </h2>
                </div>
                <div class="news__content release-background">
<?php for($i=0;$i<count($news);$i++) : ?>
                  <div class="news__content-list"><a href="<?= get_the_permalink( $news[$i] ) ?>">
                      <dl class="news__content-info flex-middle">
                        <dt><?= get_the_date( 'Y.m.d', $news[$i] ) ?></dt>
                        <dd class="-<?= $news_categories[$i]->slug ?>"><?= $news_categories[$i]->name ?></dd>
                      </dl>
                      <h3 class="news__content-title"><?= $news[$i]->post_title ?></h3></a></div>
                  <!-- /.news__content-list-->
<?php endfor; ?>
                  <div class="news__content-image -top-right lazyload"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/news01.png" alt="パン食い競争の画像"></div>
                  <div class="news__content-image -center-left lazyload"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/news02.png" alt="二人三脚の画像"></div>
                  <div class="news__content-image -bottom-right lazyload"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/news03.png" alt="大縄跳びの画像"></div>
                </div>
                <!-- /.news__content-->
                <div class="news__detail flex-middle-center lazyload"><a class="button lazyload flex-middle-center -white" href="<?= get_post_type_archive_link( 'post' )?>"><span class="button__text">お知らせをもっと見る</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></a></div>
              </div>
            </div>
          </div>
          <!-- /.news-->
          <div class="media release-background">
            <div class="media__outer">
              <div class="media__inner">
                <div class="media__header">
                  <h2 class="media__header-title">メディア実績</h2>
                </div>
                <div class="media__result"><a class="media__result-content -tv lazyload" href="<?= get_category_link(get_term_by( 'name', 'テレビ番組', 'category' )) ?>">
                    <div class="media__result-content-inner flex-middle-wrap lazyload">
                      <div class="media__result-info">
                        <div class="media__result-image"><svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 41.293 46.467">
<g transform="translate(-10117.85 -420.842)">
<path d="M10155,527.857h-33.016a4.144,4.144,0,0,1-4.139-4.139V500.935a4.144,4.144,0,0,1,4.139-4.139H10155a4.143,4.143,0,0,1,4.139,4.139v22.782A4.143,4.143,0,0,1,10155,527.857Zm-33.016-29.656a2.737,2.737,0,0,0-2.733,2.735v22.782a2.737,2.737,0,0,0,2.733,2.735H10155a2.737,2.737,0,0,0,2.735-2.735V500.935A2.737,2.737,0,0,0,10155,498.2Z" transform="translate(0 -66.547)" />
<g transform="translate(10121.895 433.682)"><path d="M10175.754,548.2h-22.926a2.327,2.327,0,0,1-2.324-2.324V526.835a2.327,2.327,0,0,1,2.324-2.324h22.926a2.327,2.327,0,0,1,2.324,2.324v19.041A2.327,2.327,0,0,1,10175.754,548.2Zm-22.926-22.284a.919.919,0,0,0-.919.919v19.041a.919.919,0,0,0,.919.919h22.926a.919.919,0,0,0,.919-.919V526.835a.919.919,0,0,0-.919-.919Z" transform="translate(-10150.504 -524.511)" /></g>
<circle cx="2.027" cy="2.027" r="2.027" transform="translate(10150.264 434.594)" />
<circle cx="2.027" cy="2.027" r="2.027" transform="translate(10150.264 441.296)" />
<path d="M10188.574,448.394a1.223,1.223,0,0,1-.852-.343l-5.676-5.459a1.229,1.229,0,1,1,1.7-1.771l5.676,5.459a1.229,1.229,0,0,1-.853,2.114Z" transform="translate(-55.915 -17.204)" />
<path d="M10225.484,477.186a6.725,6.725,0,0,0-12.074.249Z" transform="translate(-83.724 -46.176)" />
<path d="M10211.228,529.817" transform="translate(-81.812 -95.478)"  stroke="#e60000" stroke-linecap="round" stroke-linejoin="round" stroke-width="8.504"/>
<path d="M10279.336,430.325a1.229,1.229,0,0,1-.865-2.1l7.08-7.026a1.229,1.229,0,1,1,1.73,1.745l-7.08,7.026A1.223,1.223,0,0,1,10279.336,430.325Z" transform="translate(-140.408)" />
<g transform="translate(10125.2 459.938)">
<line x1="3.351" y2="4.562" transform="translate(1.404 1.404)" />
<path d="M10178.6,743.853a1.4,1.4,0,0,1-1.131-2.236l3.351-4.562a1.4,1.4,0,1,1,2.263,1.663l-3.352,4.562A1.4,1.4,0,0,1,10178.6,743.853Z" transform="translate(-10177.198 -736.482)" />
</g>
<g transform="translate(10144.768 459.867)">
<line x2="3.459" y2="4.271" transform="translate(1.404 1.404)" />
<path d="M10340.027,742.994a1.4,1.4,0,0,1-1.093-.521l-3.459-4.271a1.4,1.4,0,1,1,2.183-1.768l3.459,4.271a1.4,1.4,0,0,1-1.09,2.288Z" transform="translate(-10335.163 -735.914)" />
</g>
</g>
</svg>
                        </div>
                        <div class="media__result-genre -mobile">
                          <p>テレビ番組</p>
                        </div>
                      </div>
                      <div class="media__result-number">
                        <p><?= get_term_by( 'name', 'テレビ番組', 'category' )->count ?></p>
                      </div>
                      <div class="media__result-genre -pc">
                        <p>テレビ番組</p>
                      </div>
                    </div></a><a class="media__result-content -book lazyload" href="<?= get_category_link(get_term_by( 'name', '新聞・雑誌', 'category' )) ?>">
                    <div class="media__result-content-inner flex-middle-wrap lazyload">
                      <div class="media__result-info">
                        <div class="media__result-image"><svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 56.223 45.238">
<g transform="translate(-1216.223 -197.096)">
<path d="M1299.985,197.913a.721.721,0,0,0-.623-.128l-22.907,5.939-24.427-5.942a.722.722,0,0,0-.893.7l-.207,34.683a.722.722,0,0,0,.565.709l24.672,5.508a.723.723,0,0,0,.336,0l23.221-5.916a.722.722,0,0,0,.544-.7V198.484A.722.722,0,0,0,1299.985,197.913Zm-47.411,1.489,23.141,5.629-.2,32.725-23.138-5.166Zm46.247,32.8-21.864,5.571.2-32.735,21.662-5.616Z" transform="translate(-31.17 -0.599)"/>
<g transform="translate(1216.223 197.095)">
<path d="M1245.357,242.335a.718.718,0,0,1-.167-.02l-28.414-6.8a.722.722,0,0,1-.553-.705l.139-36.1a.721.721,0,0,1,.72-.719h0a.721.721,0,0,1,.717.725l-.137,35.529,27.682,6.626,25.658-6.824V197.817a.72.72,0,1,1,1.44,0V234.6a.722.722,0,0,1-.535.7l-26.369,7.013A.717.717,0,0,1,1245.357,242.335Z" transform="translate(-1216.223 -197.095)"/>
</g>
<path d="M1296.344,253.559l14.384,3.561-.009,7.3-14.448-3.459.073-7.4M1295.137,252l-.1,9.919,16.9,4.046.012-9.8L1295.137,252Z" transform="translate(-70.785 -49.311)"/>
<path d="M1545.774,254.573v7.179l-13.552,3.8v-7.525l13.552-3.453M1547,253l-16,4.076v10.087l16-4.483V253Z" transform="translate(-282.703 -50.209)"/>
<path d="M1311.326,383.229a.723.723,0,0,1-.174-.021l-15.691-3.872a.722.722,0,1,1,.346-1.4l15.691,3.872a.722.722,0,0,1-.172,1.423Z" transform="translate(-70.671 -162.393)"/>
<path d="M1309.53,488.015a.728.728,0,0,1-.163-.019l-15.9-3.659a.722.722,0,0,1,.324-1.407l15.9,3.659a.722.722,0,0,1-.161,1.426Z" transform="translate(-68.875 -256.694)"/>
<path d="M1303.8,414.917a.723.723,0,0,1-.168-.02l-6.614-1.581a.722.722,0,0,1,.336-1.4l6.614,1.581a.722.722,0,0,1-.167,1.425Z" transform="translate(-72.063 -192.911)"/>
<path d="M1392.17,437.039a.724.724,0,0,1-.172-.021l-6.452-1.581a.722.722,0,1,1,.344-1.4l6.452,1.581a.722.722,0,0,1-.171,1.424Z" transform="translate(-151.575 -212.778)"/>
<path d="M1304.025,448.438a.731.731,0,0,1-.162-.018l-6.323-1.452a.722.722,0,0,1,.323-1.407l6.323,1.452a.722.722,0,0,1-.161,1.426Z" transform="translate(-72.527 -223.131)"/>
<path d="M1389.891,469.423a.727.727,0,0,1-.172-.021l-6.452-1.581a.722.722,0,0,1,.344-1.4l6.452,1.581a.722.722,0,0,1-.171,1.424Z" transform="translate(-149.528 -241.862)"/>
<path d="M1530.63,378.952a.722.722,0,0,1-.21-1.413l15.08-4.6a.722.722,0,1,1,.421,1.381l-15.08,4.6A.722.722,0,0,1,1530.63,378.952Z" transform="translate(-281.723 -157.902)"/>
<path d="M1531.582,486.359a.722.722,0,0,1-.184-1.42l15.188-4a.722.722,0,0,1,.368,1.4l-15.187,4A.725.725,0,0,1,1531.582,486.359Z" transform="translate(-282.578 -254.897)"/>
<path d="M1614.167,411.186a.722.722,0,0,1-.2-1.416l6.3-1.805a.722.722,0,1,1,.4,1.388l-6.3,1.805A.72.72,0,0,1,1614.167,411.186Z" transform="translate(-356.748 -189.358)"/>
<path d="M1529.869,436.39a.722.722,0,0,1-.2-1.416l6.3-1.805a.722.722,0,0,1,.4,1.388l-6.3,1.805A.72.72,0,0,1,1529.869,436.39Z" transform="translate(-281.04 -211.994)"/>
<path d="M1617.471,444.695a.722.722,0,0,1-.192-1.418l6.027-1.667a.722.722,0,0,1,.385,1.392l-6.027,1.667A.721.721,0,0,1,1617.471,444.695Z" transform="translate(-359.715 -219.577)"/>
<path d="M1530.633,468.675a.722.722,0,0,1-.191-1.418l6.555-1.805a.722.722,0,0,1,.383,1.392l-6.555,1.805A.72.72,0,0,1,1530.633,468.675Z" transform="translate(-281.726 -240.989)"/>
</g>
</svg>
                        </div>
                        <div class="media__result-genre -mobile">
                          <p>新聞・雑誌</p>
                        </div>
                      </div>
                      <div class="media__result-number">
                        <p><?= get_term_by( 'name', '新聞・雑誌', 'category' )->count ?></p>
                      </div>
                      <div class="media__result-genre -pc">
                        <p>新聞・雑誌</p>
                      </div>
                    </div></a><a class="media__result-content -net lazyload" href="<?= get_category_link(get_term_by( 'name', 'インターネット', 'category' )) ?>">
                    <div class="media__result-content-inner flex-middle-wrap lazyload">
                      <div class="media__result-info">
                        <div class="media__result-image"><svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 42.019 43.318">
<g transform="translate(-368.827 -181.826)">
<path d="M408.437,181.826h-37.2a2.412,2.412,0,0,0-2.409,2.41v38.5a2.412,2.412,0,0,0,2.409,2.409h37.2a2.412,2.412,0,0,0,2.409-2.409v-38.5A2.412,2.412,0,0,0,408.437,181.826Zm-37.2,1.518h37.2a.891.891,0,0,1,.891.891V188.3H370.345v-4.068A.891.891,0,0,1,371.236,183.344Zm37.2,40.281h-37.2a.891.891,0,0,1-.891-.891V189.821h38.982v32.913A.891.891,0,0,1,408.437,223.626Z" transform="translate(0)" />
<g transform="translate(396.691 184.801)">
<path d="M630.384,212.524a1.465,1.465,0,1,1,1.465-1.465A1.467,1.467,0,0,1,630.384,212.524Zm0-2.323a.858.858,0,1,0,.858.858A.859.859,0,0,0,630.384,210.2Z" transform="translate(-628.919 -209.594)" />
</g>
<g transform="translate(400.645 184.801)">
<path d="M667.289,212.524a1.465,1.465,0,1,1,1.465-1.465A1.467,1.467,0,0,1,667.289,212.524Zm0-2.323a.858.858,0,1,0,.857.858A.859.859,0,0,0,667.289,210.2Z" transform="translate(-665.824 -209.594)" />
</g>
<g transform="translate(404.598 184.743)">
<path d="M704.193,211.989a1.465,1.465,0,1,1,1.465-1.465A1.467,1.467,0,0,1,704.193,211.989Zm0-2.323a.858.858,0,1,0,.858.858A.859.859,0,0,0,704.193,209.666Z" transform="translate(-702.728 -209.059)" />
</g>
<g transform="translate(373.988 209.971)">
<path d="M447.554,446.06h-29.8a.759.759,0,0,1,0-1.518h29.8a.759.759,0,1,1,0,1.518Z" transform="translate(-416.998 -444.542)" />
</g>
<g transform="translate(373.838 213.37)">
<path d="M423.521,477.784h-7.158a.759.759,0,1,1,0-1.518h7.158a.759.759,0,1,1,0,1.518Z" transform="translate(-415.604 -476.266)" />
</g>
<g transform="translate(385.27 213.37)">
<path d="M530.5,477.784h-7.43a.759.759,0,1,1,0-1.518h7.43a.759.759,0,1,1,0,1.518Z" transform="translate(-522.309 -476.266)" />
</g>
<g transform="translate(396.354 213.37)">
<path d="M633.966,477.784h-7.43a.759.759,0,1,1,0-1.518h7.43a.759.759,0,1,1,0,1.518Z" transform="translate(-625.776 -476.266)" />
</g>
<g transform="translate(373.838 216.934)">
<path d="M423.521,511.057h-7.158a.759.759,0,1,1,0-1.518h7.158a.759.759,0,1,1,0,1.518Z" transform="translate(-415.604 -509.539)" />
</g>
<g transform="translate(385.27 216.934)">
<path d="M530.5,511.057h-7.43a.759.759,0,1,1,0-1.518h7.43a.759.759,0,1,1,0,1.518Z" transform="translate(-522.309 -509.539)" />
</g>
<g transform="translate(396.354 216.934)">
<path d="M633.966,511.057h-7.43a.759.759,0,1,1,0-1.518h7.43a.759.759,0,1,1,0,1.518Z" transform="translate(-625.776 -509.539)" />
</g>
<path d="M433.287,276.22H421.565a.759.759,0,1,1,0-1.518h11.722a.759.759,0,1,1,0,1.518Z" transform="translate(-46.41 -82.926)" />
<g transform="translate(-40.334 -112.657)" fill="none">
<path d="M444.527,320.427H415.4a1.4,1.4,0,0,1-1.4-1.4V309.4a1.4,1.4,0,0,1,1.4-1.4h29.129a1.4,1.4,0,0,1,1.4,1.4v9.631A1.4,1.4,0,0,1,444.527,320.427Z" stroke="none" />
<path d="M 444.425048828125 318.9272155761719 L 444.425048828125 309.5 L 415.5 309.5 L 415.5 318.9272155761719 L 444.425048828125 318.9272155761719 M 444.5269775390625 320.4272155761719 L 415.3980407714844 320.4272155761719 C 414.6259765625 320.4272155761719 414 319.80126953125 414 319.0291442871094 L 414 309.3980407714844 C 414 308.6259765625 414.6259765625 308 415.3980407714844 308 L 444.5269775390625 308 C 445.299072265625 308 445.925048828125 308.6259765625 445.925048828125 309.3980407714844 L 445.925048828125 319.0291442871094 C 445.925048828125 319.80126953125 445.299072265625 320.4272155761719 444.5269775390625 320.4272155761719 Z" stroke="none" />
</g>
</g>
</svg>
                        </div>
                        <div class="media__result-genre -mobile">
                          <p>インターネット</p>
                        </div>
                      </div>
                      <div class="media__result-number">
                        <p><?= get_term_by( 'name', 'インターネット', 'category' )->count ?></p>
                      </div>
                      <div class="media__result-genre -pc">
                        <p>インターネット</p>
                      </div>
                    </div></a><a class="media__result-content -radio lazyload" href="<?= get_category_link(get_term_by( 'name', 'ラジオ', 'category' )) ?>">
                    <div class="media__result-content-inner flex-middle-wrap lazyload">
                      <div class="media__result-info">
                        <div class="media__result-image"><svg class="icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48.66 36.842">
<g transform="translate(-11850.152 -478.867)">
<g transform="translate(11850.153 485.682)">
<path d="M11894.191,544.747h-39.416a4.627,4.627,0,0,1-4.621-4.623V519.342a4.627,4.627,0,0,1,4.621-4.623h39.416a4.628,4.628,0,0,1,4.623,4.623v20.782A4.628,4.628,0,0,1,11894.191,544.747Zm-39.416-28.217a2.815,2.815,0,0,0-2.811,2.812v20.782a2.815,2.815,0,0,0,2.811,2.812h39.416a2.815,2.815,0,0,0,2.813-2.812V519.342a2.815,2.815,0,0,0-2.812-2.812Z" transform="translate(-11850.154 -514.719)" />
</g>
<path d="M11941.472,487.493a.893.893,0,0,1-.187-.02l-32.157-6.815a.906.906,0,0,1,.376-1.772l32.155,6.815a.905.905,0,0,1-.187,1.791Z" transform="translate(-47.183)" />
<path d="M11895.107,572.045a7.33,7.33,0,1,1,7.328-7.33A7.339,7.339,0,0,1,11895.107,572.045Zm0-12.245a4.915,4.915,0,1,0,4.915,4.915A4.922,4.922,0,0,0,11895.107,559.8Z" transform="translate(-30.472 -63.593)" />
<circle cx="1.491" cy="1.491" r="1.491" transform="translate(11877.678 492.976)" />
<circle cx="1.491" cy="1.491" r="1.491" transform="translate(11883.216 492.976)" />
<circle cx="1.491" cy="1.491" r="1.491" transform="translate(11888.752 492.976)" />
<path d="M12011.831,590.473h-10.647a.905.905,0,1,1,0-1.811h10.647a.905.905,0,0,1,0,1.811Z" transform="translate(-121.588 -88.925)" />
<path d="M12011.831,622.963h-10.647a.905.905,0,1,1,0-1.811h10.647a.905.905,0,0,1,0,1.811Z" transform="translate(-121.588 -115.239)" />
</g>
</svg>
                        </div>
                        <div class="media__result-genre -mobile">
                          <p>ラジオ</p>
                        </div>
                      </div>
                      <div class="media__result-number">
                        <p><?= get_term_by( 'name', 'ラジオ', 'category' )->count ?></p>
                      </div>
                      <div class="media__result-genre -pc">
                        <p>ラジオ</p>
                      </div>
                    </div></a></div>
                <div class="media__detail flex-middle-center lazyload"><a class="button lazyload flex-middle-center -black" href="<?= get_category_link( get_category_by_slug( 'media' ) )?>"><span class="button__text">メディア実績一覧を見る</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></a></div>
              </div>
            </div>
          </div>
          <!-- /.media-->
          <div class="contact release-background lazyload">
            <div class="contact__outer release-background">
              <div class="contact__inner">
                <div class="contact__header lazyload">
                  <p>いますぐ、</p>
                  <p class="contact__header-logo"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/logo-large.svg" alt="運動会屋ロゴ"></p>
                  <p><span>運動会屋</span>に相談だ。</p>
                </div>
                <div class="contact__lead lazyload">
                  <p>運動会屋へのお問い合わせは、お気軽にどうぞ。</p>
                </div>
                <div class="contact__button lazyload"><a class="button lazyload flex-middle-center -white" href="<?= get_page_link( get_page_by_title( 'お問い合わせ' )) ?>"><span class="button__text">お問い合わせ</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></a></div>
                <div class="contact__line lazyload"><a class="button contact__line-button" href="https://lin.ee/beE9ebF/775ldykn"><img class="contact__line-icon lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/icon-line-color.png" alt="LINEアイコン">
                    <p class="contact__line-text">LINEでの<br>お問い合わせはこちら</p></a>
                  <div class="contact__line-id-qr">
                    <dl class="contact__line-id">
                      <dt>LINE ID</dt>
                      <dd>@775ldykn</dd>
                    </dl>
                    <div class="contact__line-qr"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/index/line-qr.png" alt="LINE QRコード"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.contact-->




<?php get_footer(); ?>
