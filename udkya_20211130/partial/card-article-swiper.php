<?php $category = current(get_the_terms( $post, get_taxonomy_slug($post->post_type))) ?>
                      <div class="swiper-slide">
                        <div class="carousel__slide"><a class="carousel-link" href="<?php the_permalink() ?>">
                            <div class="carousel-image lazyload" style="background-image: url(<?= coco_get_the_thumbnail_url() ?>);"></div>
                            <div class="carousel-content">
                              <div class="carousel-info">
                                <div class="carousel-date"><span><?= get_the_date('Y.m.d') ?></span></div>
                                <div class="carousel-category"><?= $category->name ?></div>
                              </div>
                              <div class="carousel-title"><?= the_title() ?></div>
                            </div></a>
                          <div class="carousel-tag">
                            <ul class="tag">
                              <?= coco_get_the_tag_list($post->ID); ?>
                            </ul>
                          </div>
                        </div>
                      </div>
