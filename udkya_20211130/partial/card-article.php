<?php $category = current(get_the_terms( $post, get_taxonomy_slug($post->post_type))) ?>
                <div class="card">
                  <div class="card__content"><a class="card__link lazyload" href="<?php the_permalink() ?>">
                      <div class="card__image"><img class="lazyload" data-src="<?= coco_get_the_thumbnail_url() ?>" alt="<?= $post->post_type ?> <?= $category->name ?>画像"></div>
                      <p class="card__small-text"><?= $category->name ?></p>
                      <h3 class="card__title"><?php the_title() ?></h3></a>
                    <!-- /.card__link-->
                    <ul class="card__tag tag">
                      <?= coco_get_the_tag_list($post->ID); ?>
                    </ul>
                    <!-- /.card__tag-->
                    <div class="card__date"><a href="<?php the_permalink() ?>"><?= get_the_date('Y.m.d') ?></a></div>
                    <!-- /.card__date-->
                  </div>
                </div>