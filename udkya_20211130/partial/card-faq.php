<?php  $category = current(get_the_terms( $post, 'faq-category' )); ?>
                    <div class="js-accordion-box faq__accordion-box lazyload">
                      <div class="faq__include-category"><span><?= $category->name ?></span></div>
                      <div class="faq__question">
                        <dl class="flex-top">
                          <dt>Q</dt>
                          <dd><?php the_title() ?></dd>
                        </dl>
                      </div>
                      <div class="faq__answer">
                        <dl class="flex-top">
                          <dt>A</dt>
                          <dd class="js-accordion-element" data-default-line-small="3" data-default-line-medium="2" data-total-height=""><?php the_content() ?></dd>
                        </dl>
                      </div>
                      <div class="faq__more accordion__more">
                        <button class="faq__more-button flex-middle accordion__more-button js-accordion-button"><span>もっと読む</span><svg class="icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 18 18" style="enable-background:new 0 0 18 18;" xml:space="preserve"><path class="st0" d="M9,14.1c-0.3,0-0.6-0.1-0.9-0.4L0.4,6c-0.5-0.5-0.5-1.3,0-1.7s1.3-0.5,1.7,0L9,11.1l6.9-6.9 c0.5-0.5,1.3-0.5,1.7,0c0.5,0.5,0.5,1.3,0,1.7l-7.8,7.8C9.6,14,9.3,14.1,9,14.1z"/></svg></button>
                      </div>
                    </div>
