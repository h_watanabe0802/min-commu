<?php  $customer = current(get_the_terms( $post, 'voices-customer' )); ?>

                        <div class="card__content"><a class="card__link lazyload" href="<?php the_permalink() ?>">
                          <div class="card__image"><img class="lazyload" data-src="<?= coco_get_the_thumbnail_url() ?>" alt="<?php the_title() ?>"></div>
                          <p class="card__small-text"><?= $customer->name ?></p>
                          <h3 class="card__title"><?php the_title() ?></h3></a>
                        <!-- /.card__link-->
                        <ul class="card__tag tag">
                          <?= coco_get_the_tag_list($post->ID); ?>
                        </ul>
                        <!-- /.card__tag--><a class="card__date" href="<?php the_permalink() ?>"><?php the_date('Y.m.d') ?></a>
                        <!-- /.card__date-->
                      </div>