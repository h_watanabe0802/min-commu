<?php if( $post->post_type == 'columns' && !is_tag() ) : ?>
          <div class="page-header release-background -column">
            <div class="page-header__inner release-background">
              <div class="page-header__content">
                <div class="page-header__title">
                  <h2><img src="<?= get_template_directory_uri() ?>/src/img/common/title-times.svg" alt="運動会屋タイムズ"></h2>
                  <p class="page-header__title-text">運動会屋が発信する運動会に関するコラムです</p>
                </div>
              </div>
            </div>
          </div>
          <!-- /.page-header-->

<?php else: ?>
<?php 
  $title = '';
  if ( is_singular('staff') ) {
    $title = get_staff_detail_title();
  } else if( is_archive() || is_single() ) {
    // echo $post_type;
    $title = get_the_list_title($post->post_type);
  } else if( is_page() ) {
    $title = $post->post_title;
  }
  if( is_tag() ) {
    $en_title = 'ARTICLE';
  } else if( $post->post_type == 'post' ) {
    $en_title = 'NEWS';
  } elseif( is_page() ) {
    $en_title = get_post_meta( $post->ID, '_udkya_page_en_title', true);
    if(empty($en_title)) {
      $en_title = strtoupper($post->post_name);
      $en_title = str_replace( '-', ' ', $en_title );
    }
  } else {
    $en_title = strtoupper( $post->post_type );
  }
?>
           <div class="page-header release-background">
            <div class="page-header__inner release-background">
              <div class="page-header__content">
                <div class="page-header__icon"><img src="<?= get_template_directory_uri() ?>/src/img/common/flag-triangle.svg" alt="三角の旗アイコン"></div>
                <div class="page-header__slug">
                  <p><?= $en_title ?></p>
                </div>
                <div class="page-header__title">
                  <h2 class="page-header__title-text"><?= $title ?></h2>
                </div>
              </div>
            </div>
          </div>
          <!-- /.page-header-->
<?php endif; ?>
          <div class="breadcrumb">
            <ul class="breadcrumb__list flex-middle">
<?php if($post_type == 'staff') : ?>
              <?php display_staff_breadcrumb() ?>
<?php else : ?>
              <?php display_breadcrumb() ?>
<?php endif; ?>
            </ul>
          </div>
          <!-- /.breadcrumb-->