<div class="js-form-content contact__content -active -confirm">
    <form id="js-inquiry-form" class="form" action="" method="POST">
        <h3 class="form__title">確認画面</h3>
        <div class="form__step">
            <dl>
                <dt>&#10102;</dt>
                <dd>フォーム入力</dd>
            </dl>
            <dl class="-current">
                <dt>&#10103;</dt>
                <dd>確認画面</dd>
            </dl>
            <dl>
                <dt>&#10104;</dt>
                <dd>送信完了</dd>
            </dl>
        </div>
        <!-- /.form__step-->
        <div class="form__remind">
            <p>以下の内容で送信してもよろしいですか？</p>
        </div>
        <!-- /.form__remind-->
        <div class="form__content-primary">
            <dl class="confirm__group flex-middle-wrap">
                <dt>お問い合わせ案件</dt>
                <dd><?= $_POST['inquiry-matter']; ?></dd>
            </dl>
            <dl class="confirm__group flex-middle-wrap">
                <dt>団体・法人名</dt>
                <dd><?= $_POST['inquiry-company']; ?></dd>
            </dl>
            <dl class="confirm__group flex-middle-wrap">
                <dt>所属部署</dt>
                <dd><?= $_POST['inquiry-division']; ?></dd>
            </dl>
            <dl class="confirm__group flex-middle-wrap">
                <dt>お名前</dt>
                <dd><?= $_POST['inquiry-yourname']; ?></dd>
            </dl>
            <dl class="confirm__group flex-middle-wrap">
                <dt>電話番号</dt>
                <dd><?= $_POST['inquiry-phone']; ?></dd>
            </dl>
            <dl class="confirm__group flex-middle-wrap">
                <dt>メールアドレス</dt>
                <dd><?= $_POST['inquiry-email']; ?></dd>
            </dl>
            <dl class="confirm__group flex-middle-wrap">
                <dt>住所</dt>
                <dd>〒<?= $_POST['inquiry-zip']; ?><br><?= $_POST['inquiry-address']; ?></dd>
            </dl>
            <dl class="confirm__group flex-middle-wrap">
                <dt>運動会屋を知ったきっかけ</dt>
                <dd><?= $_POST['inquiry-opportunity']; ?><br><?= $_POST['inquiry-opportunitytxt']; ?></dd>
            </dl>                            
            <dl class="confirm__group flex-middle-wrap">
                <dt>運動会の目的、<br>規模、予算など</dt>
                <dd><?= nl2br($_POST['inquiry-purpose']); ?></dd>
            </dl>
            <dl class="confirm__group flex-middle-wrap">
                <dt>お打ち合わせ</dt>
                <dd><?= $_POST['inquiry-meeting'] ? '希望する' : '希望しない'; ?></dd>
            </dl>
            <dl class="confirm__group flex-middle-wrap">
                <dt>お打ち合わせ方法の<br>ご希望</dt>
                <dd>
                    <?php
                    $html = '';
                    $inquiryMeetingHow = inquiryMeetingHow();

                    foreach((array) $_POST['inquiry-meeting-how'] as $key => $val){
                        if($val != 'other'){
                            $html .= '<span>'.$inquiryMeetingHow[$val].',</span>';
                        }else {
                            $html .= '<span>'.$inquiryMeetingHow[$val].'（'.$_POST['inquiry-meeting-tool'].'）</span>';
                        }
                    }
                    echo $html;
                    ?>
                </dd>
            </dl>
            <dl class="confirm__group flex-middle-wrap">
                <dt>お打ち合わせの<br>希望日時</dt>
                <dd><?= $_POST['inquiry-meeting-date']; ?></dd>
            </dl>
        </div>
        <!-- /.form__content-primary-->
        <div class="form__content-thirdry">
            <div class="form__button flex-middle-center-wrap lazyload">
                <button id="sendcontact" class="button lazyload flex-middle-center -black" type="submit"><span class="button__text">送信する</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></button>
                <button id="js-back" class="button lazyload flex-middle-center -black -back" type="button"><span class="button__text">戻る</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></button>
                <input type="hidden" name="step" value="2">
                <input type="hidden" name="inquiry-matter" value="<?= $_POST['inquiry-matter']; ?>">
                <input type="hidden" name="inquiry-company" value="<?= $_POST['inquiry-company']; ?>">
                <input type="hidden" name="inquiry-division" value="<?= $_POST['inquiry-division']; ?>">                                
                <input type="hidden" name="inquiry-yourname" value="<?= $_POST['inquiry-yourname']; ?>">
                <input type="hidden" name="inquiry-phone" value="<?= $_POST['inquiry-phone']; ?>">
                <input type="hidden" name="inquiry-email" value="<?= $_POST['inquiry-email']; ?>">
                <input type="hidden" name="inquiry-zip" value="<?= $_POST['inquiry-zip']; ?>">
                <input type="hidden" name="inquiry-address" value="<?= $_POST['inquiry-address']; ?>">
                <input type="hidden" name="inquiry-opportunity" value="<?= $_POST['inquiry-opportunity']; ?>">  
                <input type="hidden" name="inquiry-opportunitytxt" value="<?= $_POST['inquiry-opportunitytxt']; ?>">                                   
                <input type="hidden" name="inquiry-purpose" value="<?= $_POST['inquiry-purpose']; ?>">
                <input type="hidden" name="inquiry-meeting" value="<?= $_POST['inquiry-meeting']; ?>">
                <input type="hidden" name="inquiry-meeting-how" value="<?= isset($_POST['inquiry-meeting-how']) ? implode(',', $_POST['inquiry-meeting-how']) : ''; ?>">
                <input type="hidden" name="inquiry-meeting-tool" value="<?= $_POST['inquiry-meeting-tool']?>">
                <input type="hidden" name="inquiry-meeting-date" value="<?= $_POST['inquiry-meeting-date']; ?>">
                <input type="hidden" name="inquiry-agree" value="<?= $_POST['inquiry-agree']; ?>">
                <input type="hidden" name="inquiry-recaptcha" value="<?= htmlspecialchars($_POST['g-recaptcha-response'], ENT_QUOTES,'UTF-8'); ?>">
            </div>
        </div> 
        <!-- /.form__content-thirdry-->
    </form>
</div>
<!-- /.contact__content-->
