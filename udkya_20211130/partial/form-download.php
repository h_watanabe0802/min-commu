				<div class="js-form-content contact__content -active -download" id="js-download">
					<div class="contact__message">
						<div class="contact__message-image -left"><img src="<?= get_theme_file_uri(); ?>/src/img/contact/download-left.png" alt="棒運びする画像左"></div>
						<div class="contact__message-content">
							<p>運動会の開催・運動会屋に関する資料について</p>
							<p>下記フォームに必要事項を入力後</p>
							<p>ダウンロードが行えるようになります。</p>
						</div>
						<div class="contact__message-image -right"><img src="<?= get_theme_file_uri(); ?>/src/img/contact/download-right.png" alt="棒運びする画像右"></div>
					</div>
					<!-- /.contact__message-->
					<form class="form" id="js-download-form" action="?" method="POST">
						<h3 class="form__title">資料ダウンロード用フォーム</h3>
						<div class="form__content-primary">
							<dl class="form__group">
								<dt>
									<label for="download-email"><span class="require">必須</span>メールアドレス</label>
								</dt>
								<dd>
									<input type="email" id="download-email" name="download-email" placeholder="例）contact@udkya.com"><span class="error download-email">※メールアドレスを入力してください。</span>
								</dd>
							</dl>
							<dl class="form__group">
								<dt>
									<label for="download-company">法人・団体名</label>
								</dt>
								<dd>
									<input type="text" id="download-company" name="download-company" placeholder="例）株式会社運動会屋">
								</dd>
							</dl>
                          
							<dl class="form__group">
								<dt>
									<label for="download-yourname"><span class="require">必須</span>お名前</label>
								</dt>
								<dd>
									<input type="text" id="download-yourname" name="download-yourname" placeholder="例）運動会　太郎"><span class="error download-yourname">※お名前を入力してください。</span>
								</dd>
							</dl>
							<dl class="form__group">
								<dt>
									<label for="download-phone">電話番号</label>
								</dt>
								<dd>
									<input type="text" id="download-phone" name="download-phone" placeholder="例）03-6416-0430">
								</dd>
							</dl>
						</div>
						<!-- /.form__content-primary-->
						<div class="form__content-thirdry">
							<dl class="form__group">
								<dd>
									<label class="label">
										<input type="checkbox" name="download-agree" value="1"><span class="imitate"></span><span class="label__text"><a href="<?= get_page_link( get_page_by_title( 'プライバシーポリシー' )) ?>">プライバシーポリシー</a>について、同意する</span>
									</label><span class="error download-agree">※チェックを入れてください。</span>
								</dd>
							</dl>
							<div class="form__recaptcha">
								<div class="form__recaptcha-content">
									<div class="g-recaptcha" data-callback="validateReCaptchaDownload" data-sitekey="6Ld-dvUUAAAAAGhivqiiLTuEIfTLT10Ig_lSbY4D"></div><span class="error download-recaptcha">※チェックを入れてください。</span>
								</div>
							</div>
							<div class="form__button flex-middle-center lazyload">
								<button class="button lazyload flex-middle-center -black" id="js-download-submit" type="submit"><span class="button__text">送信する</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></button>
							</div>
						</div>
						<!-- /.form__content-thirdry-->
						<div class="form__result" id="js-form-result">
							<h4 class="form__result-title"></h4>
							<div class="form__result-content"></div>
						</div>
						<!-- /.form__result-->
						<div class="form__document" id="js-form-document">
<?php
$pdf_posts = get_posts( array( 'post_type' => 'download', 'posts_per_page' => -1 ) );
foreach( $pdf_posts as $pdf_post) :
	$pdf_id = get_post_meta($pdf_post->ID)['download-file'][0];
	$img_src = wp_get_attachment_image_src($pdf_id, 'medium', false)[0];
	$pdf_url = wp_get_attachment_url( $pdf_id );
	$pdf_size = round(filesize(get_attached_file( $pdf_id ) ) / 1000000. , 1 );
?>
							<a class="form__document-content" href="/" download="<?= basename( $pdf_url ) ?>" data-url="<?= $pdf_url ?>">
								<div class="form__document-image"><img src="<?= $img_src ?>" alt="<?= $pdf_post->post_title ?>"></div>
								<div class="form__document-name">
									<p><?= $pdf_post->post_title ?></p>
								</div>
								<div class="form__document-size"><span><?php printf("%.1fMB", $pdf_size) ?></span></div>
								<div class="form__document-button">
									<p>ダウンロードする</p>
								</div>
							</a>
<?php endforeach; ?>
						</div>
						<!-- /.form__document-->
					</form>
				</div>
				<!-- /.contact__content-->
