<div class="js-form-content contact__content -active -inquiry" id="js-inquiry">
    <div class="contact__message">
        <div class="contact__message-image -left"><img src="<?= get_theme_file_uri(); ?>/src/img/contact/contact-left.png" alt="棒運びする画像左"></div>
        <div class="contact__message-content">
            <p>運動会の開催・運動会屋に関するお問い合わせは、</p>
            <p>お電話、LINEまたは下記フォームより承っております。</p>
            <p>どうぞ、お気軽にお問い合わせください。</p>
        </div>
        <div class="contact__message-image -right"><img src="<?= get_theme_file_uri(); ?>/src/img/contact/contact-right.png" alt="棒運びする画像右"></div>
    </div>
    <!-- /.contact__message-->
    <div class="contact__lead">
        <p><a href="https://www.undokai.co.jp/contact" target="_blank" rel="noopener">メディア掲載などについてのお問い合わせはこちらより</a></p>
    </div>
    <!-- /.contact__lead-->
    <ul class="contact__how">
        <li><a href="tel:0120384604"><img src="<?= get_theme_file_uri(); ?>/src/img/contact/button-tel-yellow.svg?2020091115"></a></li>
        <li><a href="https://lin.ee/beE9ebF/775ldykn"><img src="<?= get_theme_file_uri(); ?>/src/img/contact/button-line-yellow.svg"></a></li>
    </ul>
    <!-- /.contact__how-->
    <form class="form h-adr" id="js-inquiry-form" action="" method="POST">
        <span class="form__country p-country-name">Japan</span>
        <h3 class="form__title">お問い合わせフォーム</h3>
        <div class="form__step">
            <dl class="-current">
                <dt>&#10102;</dt>
                <dd>フォーム入力</dd>
            </dl>
            <dl>
                <dt>&#10103;</dt>
                <dd>確認画面</dd>
            </dl>
            <dl>
                <dt>&#10104;</dt>
                <dd>送信完了</dd>
            </dl>
        </div>
        <!-- /.form__step-->
        <div class="form__content-primary">
            <dl class="form__group -button">
                <dt><span class="require">必須</span>お問い合わせ案件</dt>
                <dd>
                    <label class="label">
                        <input type="radio" name="inquiry-matter" value="お問い合わせ"<?= $_POST['inquiry-matter'] == 'お問い合わせ' ? ' checked="checked"':'';?>><span class="imitate"></span><span class="label__text">お問い合わせ</span>
                    </label>
                    <label class="label">
                        <input type="radio" name="inquiry-matter" value="資料請求"<?= $_POST['inquiry-matter'] == '資料請求' ? ' checked="checked"':'';?>><span class="imitate"></span><span class="label__text">資料請求</span>
                    </label><span class="error inquiry-matter">※お問い合わせ案件を選択してください。</span>
                </dd>
            </dl>
            <dl class="form__group">
                <dt>
                    <label for="inquiry-company"><span class="require">必須</span>法人・団体名</label>
                </dt>
                <dd>
                    <input type="text" id="inquiry-company" name="inquiry-company" placeholder="例）株式会社運動会屋" value="<?= $_POST['inquiry-company']; ?>"><span class="error inquiry-company">※法人・団体名を入力してください。</span>
                </dd>
            </dl>
            <dl class="form__group">
                <dt>
                    <label for="inquiry-division"><span class="require">必須</span>所属部署</label>
                </dt>
                <dd>
                    <input type="text" id="inquiry-division" name="inquiry-division" placeholder="例）総務部" value="<?= $_POST['inquiry-division']; ?>"><span class="error inquiry-division">※所属部署を入力してください。</span>
                </dd>
            </dl>                            
            <dl class="form__group">
                <dt>
                    <label for="inquiry-yourname"><span class="require">必須</span>お名前</label>
                </dt>
                <dd>
                    <input type="text" id="inquiry-yourname" name="inquiry-yourname" placeholder="例）運動会　太郎" value="<?= $_POST['inquiry-yourname']; ?>"><span class="error inquiry-yourname">※お名前を入力してください。</span>
                </dd>
            </dl>
            <dl class="form__group">
                <dt>
                    <label for="inquiry-phone"><span class="require">必須</span>電話番号</label>
                </dt>
                <dd> 
                    <input type="text" id="inquiry-phone" name="inquiry-phone" placeholder="例）03-6416-0430" value="<?= $_POST['inquiry-phone']; ?>"><span class="error inquiry-phone">※電話番号を入力してください。</span>
                </dd>
            </dl>
            <dl class="form__group">
                <dt>
                    <label for="inquiry-email"><span class="require">必須</span>メールアドレス</label>
                </dt>
                <dd>
                    <input type="email" id="inquiry-email" name="inquiry-email" placeholder="例）contact@udkya.com" value="<?= $_POST['inquiry-email']; ?>"><span class="error inquiry-email">※メールアドレスを入力してください。</span>
                </dd>
            </dl>
            <dl class="form__group">
                <dt>
                    <label for="inquiry-zip"><span class="require">必須</span>郵便番号</label>
                </dt>
                <dd><span class="zip-mark">〒</span>
                    <input class="-zip p-postal-code" type="text" id="inquiry-zip" name="inquiry-zip" placeholder="例）150-0034" value="<?= $_POST['inquiry-zip']; ?>"><span class="error inquiry-zip">※郵便番号を入力してください。</span>
                </dd>
            </dl>
            <dl class="form__group">
                <dt>
                    <label for="inquiry-address"><span class="require">必須</span>住所</label>
                </dt>
                <dd>
                    <input class="p-region p-locality p-street-address p-extended-address" type="text" id="inquiry-address" name="inquiry-address" placeholder="例）東京都渋谷区代官山町9-10 SodaCCo3F" value="<?= $_POST['inquiry-address']; ?>"><span class="error inquiry-address">※住所を入力してください。</span>
                </dd>
            </dl>
            <dl class="form__group -button">
                <dt><span class="require">必須</span>運動会屋を知ったきっかけ</dt>
                <dd>
                    <label style="margin-bottom:2rem;" class="label">
                        <input type="radio" name="inquiry-opportunity" value="ネット検索"<?= $_POST['inquiry-opportunity'] == 'ネット検索' ? ' checked="checked"':'';?>><span class="imitate"></span><span class="label__text">ネット検索</span>
                    </label>
                    <label style="margin-bottom:2rem;" class="label">
                        <input type="radio" name="inquiry-opportunity" value="広告"<?= $_POST['inquiry-opportunity'] == '広告' ? ' checked="checked"':'';?>><span class="imitate"></span><span class="label__text">広告</span>
                    </label>
                    <label style="margin-bottom:2rem;" class="label">
                        <input type="radio" name="inquiry-opportunity" value="SNS"<?= $_POST['inquiry-opportunity'] == 'SNS' ? ' checked="checked"':'';?>><span class="imitate"></span><span class="label__text">SNS</span>
                    </label>
                    <label style="margin-bottom:2rem;" class="label">
                        <input type="radio" name="inquiry-opportunity" value="DM"<?= $_POST['inquiry-opportunity'] == 'DM' ? ' checked="checked"':'';?>><span class="imitate"></span><span class="label__text">DM</span>
                    </label> 
                    <label style="margin-bottom:2rem;" class="label">
                        <input type="radio" name="inquiry-opportunity" value="展示会"<?= $_POST['inquiry-opportunity'] == '展示会' ? ' checked="checked"':'';?>><span class="imitate"></span><span class="label__text">展示会</span>
                    </label>  
                    <label style="margin-bottom:2rem;" class="label">
                        <input type="radio" name="inquiry-opportunity" value="メディア（TV、新聞など）"<?= $_POST['inquiry-opportunity'] == 'メディア（TV、新聞など）' ? ' checked="checked"':'';?>><span class="imitate"></span><span class="label__text">メディア（TV、新聞など）</span>
                    </label>  
                    <label style="margin-bottom:2rem;" class="label">
                        <input type="radio" name="inquiry-opportunity" value="紹介"<?= $_POST['inquiry-opportunity'] == '紹介' ? ' checked="checked"':'';?>><span class="imitate"></span><span class="label__text">紹介</span>
                    </label>   
                    <label style="margin-bottom:2rem;" class="label">
                        <input type="radio" name="inquiry-opportunity" value="過去に取引有り"<?= $_POST['inquiry-opportunity'] == '過去に取引有り' ? ' checked="checked"':'';?>><span class="imitate"></span><span class="label__text">過去に取引有り</span><br>
                    </label> 
                    <label style="margin-bottom:2rem;" class="label">
                        <input type="radio" name="inquiry-opportunity" value="その他"<?= $_POST['inquiry-opportunity'] == 'その他' ? ' checked="checked"':'';?>><span class="imitate"></span><span class="label__text">その他</span>
                    </label>
                        <label style="margin-bottom:2rem;" class="label">
                    <input style="margin-bottom:2rem;" type="text" class="-small"id="inquiry-opportunitytxt"  name="inquiry-opportunitytxt" placeholder="例）その他" value="<?= $_POST['inquiry-opportunitytxt']; ?>">
                    </label>
                    <span class="error inquiry-matter">※運動会屋を知ったきっかけを選択してください。</span>
                </dd>
            </dl>                            
            <dl class="form__group">
                <dt>
                    <label for="inquiry-purpose"><span class="require">必須</span>運動会の目的、規模、予算など</label>
                </dt>
                <dd>
                    <textarea id="inquiry-purpose" name="inquiry-purpose" placeholder="例）目的：運動会を通じて、組織力を高めたい！　規模：300人　予算：300万円"><?= $_POST['inquiry-purpose']; ?></textarea><span class="error inquiry-purpose">※運動会の目的、規模、予算などを入力してください。</span>
                </dd>
            </dl>
        </div>
        <!-- /.form__content-primary-->
        <div class="form__content-secondary">
            <dl class="form__group -button">
                <dt><span class="require">必須</span>お打ち合わせ</dt>
                <dd>
                    <label class="label">
                        <input type="radio" name="inquiry-meeting" value="1"<?= $_POST['inquiry-meeting'] == 1 ? ' checked="checked"':''; ?>><span class="imitate"></span><span class="label__text">希望する</span>
                    </label>
                    <label class="label">
                        <input type="radio" name="inquiry-meeting" value="0"<?= $_POST['inquiry-meeting'] == 0 ? ' checked="checked"':''; ?>><span class="imitate"></span><span class="label__text">希望しない</span>
                    </label><span class="error inquiry-meeting">※お打ち合わせを選択してください。</span>
                </dd>
            </dl>
            <dl class="js-form-group form__group<?= $_POST['inquiry-meeting'] != 1 ? ' -inactive':'';?>">
                <dt><span class="require">必須</span>お打ち合わせ方法のご希望</dt>
                <?php
                $inquiryMeetingHow = inquiryMeetingHow();
                $inquiryMeetingHowChecked = explode(',',  $_POST['inquiry-meeting-how']);

                if($_POST['inquiry-meeting'] == 1){
                    $disabled = '';
                    $inquiryMeetingTool = $_POST['inquiry-meeting-tool'];
                    $inquiryMeetingDate = $_POST['inquiry-meeting-date'];
                }else{
                    $disabled = ' disabled="disabled"';
                    $inquiryMeetingTool = '';
                    $inquiryMeetingDate = '';
                }

                $html = '';
                foreach((array) $inquiryMeetingHow as $key => $val){
                    if(in_array($key, $inquiryMeetingHowChecked)){
                        $checked = ' checked="checked"';
                    }else{
                        $checked = '';
                    }
                    $html .= '<dd>';
                    $html .= '<label class="label">';
                    $html .= '<input type="checkbox" name="inquiry-meeting-how[]" value="'.$key.'"'.$disabled.$checked.'><span class="imitate"></span><span class="label__text">'.$val.'</span>';
                    $html .= '</label>';
                    if($key == 'other'){
                        $html .= '<span class="error inquiry-meeting-how">※お打ち合わせ方法を選択してください。</span>';
                    }
                    $html .= '</dd>';
                }
                echo $html;
                ?>
                <dd>
                    <input class="exclude" type="text" name="inquiry-meeting-tool" placeholder="例）ツール名やサービス名"<?= $disabled; ?> value="<?= $inquiryMeetingTool; ?>"><span class="error inquiry-meeting-tool">※ツール名やサービス名を入力してください。</span>
                </dd>
            </dl>
            <dl class="js-form-group form__group<?= $_POST['inquiry-meeting'] != 1 ? ' -inactive':'';?>">
                <dt>
                    <label for="inquiry-meeting-date"></label><span class="require">必須</span>お打ち合わせ希望日時
                </dt>
                <dd><i class="icon js-datetitmepicker-icon"><img src="<?= get_theme_file_uri(); ?>/src/img/contact/icon-calendar.svg" alt="カレンダーアイコン"></i>
                    <input class="-small datetimepicker" type="text" id="inquiry-meeting-date" name="inquiry-meeting-date"<?= $disabled; ?> value="<?= $inquiryMeetingDate; ?>"><span class="error inquiry-meeting-date">※お打ち合わせ希望日時を入力してください。</span>
                </dd>
            </dl>
            <div class="form__notice">
                <p>※お打ち合わせ日時については希望を伺った上で改めて調整の上実施致します。</p>
            </div>
        </div>
        <!-- /.form__content-secondary-->
        <div class="form__content-thirdry">
            <dl class="form__group">
                <dd>
                    <label class="label">
                        <input type="checkbox" name="inquiry-agree" value="1"<?= $_POST['inquiry-agree'] == 1 ? ' checked="checked"':''; ?>><span class="imitate"></span><span class="label__text"><a href="<?= get_page_link( get_page_by_title( 'プライバシーポリシー' )) ?>">プライバシーポリシー</a>について、同意する</span>
                    </label><span class="error inquiry-agree">※チェックを入れてください。</span>
                </dd>
            </dl>
            <div class="form__recaptcha">
                <div class="form__recaptcha-content">
                    <div class="g-recaptcha" data-callback="validateReCaptchaInquiry" data-sitekey="6Ld-dvUUAAAAAGhivqiiLTuEIfTLT10Ig_lSbY4D"></div><span class="error inquiry-recaptcha">※チェックを入れてください。</span>
                </div>
            </div>
            <div class="form__button flex-middle-center lazyload">
                <input type="hidden" name="step" value="1">
                <button class="button lazyload flex-middle-center -black" id="js-inquiry-submit" type="submit"><span class="button__text">確認する</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></button>
            </div>
        </div>
        <!-- /.form__content-thirdry-->
    </form>
</div>
<!-- /.contact__content-->
