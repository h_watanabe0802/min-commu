				<div class="js-form-content contact__content -active -thanks" onLoad=”ga(‘send’, ‘event’, ‘form’, ‘load’, ‘contact’, null, true);”>
					<form class="form" action="" method="POST">
						<h3 class="form__title">送信完了</h3>
						<div class="form__step">
							<dl>
								<dt>&#10102;</dt>
								<dd>フォーム入力</dd>
							</dl>
							<dl>
								<dt>&#10103;</dt>
								<dd>確認画面</dd>
							</dl>
							<dl class="-current">
								<dt>&#10104;</dt>
								<dd>送信完了</dd>
							</dl>
						</div>
						<!-- /.form__step-->
						<div class="form__remind">
							<p>お問い合わせを受け付けました。</p>
						</div>
						<!-- /.form__remind-->
						<div class="form__thanks">
							<p>この度は弊社ホームページをご利用いただきまして誠にありがとうございます。</p>
							<p>お問い合わせいただきました内容を確認後、担当者より改めてご連絡いたします。</p>
						</div>
						<!-- /.form__thanks-->
						<div class="form__content-thirdry">
							<div class="form__button flex-middle-center-wrap lazyload">
								<a class="button lazyload flex-middle-center -black" href="<?php echo home_url(); ?>"><span class="button__text">トップページへ戻る</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></a>
							</div>
						</div>
					</form>
				</div>
				<!-- /.contact__content-->
<!-- Event snippet for オンライン運動会お問い合わせ conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-1026211830/VQ7CCPm9gdYBEPb_qukD'});
</script>
