					<div class="card-wrapper flex-top-wrap">
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<?php get_template_part( 'partial/card', 'article' ); ?>
<?php endwhile; ?>
<?php else: ?>
    <p>現在記事はありません。</p>
<?php endif; ?>
					</div>
					<div class="pagination">
					<?php echo coco_pagination(); ?>
					</div>

