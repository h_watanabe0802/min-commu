            <ul class="nav__main">
              <?= echo_menu_nav('nav__main') ?>
            </ul>
            <div class="nav__external">
              <p class="nav__external-title">運動会屋グループサイト</p>
              <ul class="nav__external-list">
                <li><a href="https://www.undokai.co.jp/"><span>運動会屋コーポレートサイト</span></a></li>
                <li><a href="https://www.udkrent.com/"><span>運動会レンタル.com</span></a></li>
                <li><a href="https://www.undokai.net/"><span>運動会市場</span></a></li>
                <li><a href="https://www.spocom.org/"><span>国際協力NGO JSC<br><span class="small-text">（NPO法人ジャパンスポーツコミュニケーションズ）</span></span></a></li>

                <li><a href="https://www.worldcaravan.org/"><span>UNDOKAIワールドキャラバン</span></a></li>
                <li><a href="https://office-yoga.jp/"><span>オフィスヨガ</span></a></li>
                <li><a href="https://karehasu.co.jp/"><span>カレハス</span></a></li>
                <li><a href="https://www.fam2.jp/"><span>Spice&Cafe FamFam</span></a></li>
                </ul>
                <ul class="nav__external-list"> 
                  <li><a href="https://campiece.com/"><span>CAMPiece</span></a></li>
                  <li><a href="https://www.soshiki.jp/"><span>組織文化Lab</span></a></li>
                  <li><a href="https://www.ha-ko-bi.com/"><span>はこび</span></a></li>
                </ul>
</div>
 

