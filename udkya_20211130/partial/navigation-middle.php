          <div class="header__tel"><a class="flex-bottom-center header__tel-link" href="tel:<?= get_option( 'tel' ) ?>"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/freedial-white.svg" alt="フリーダイアル"></a></div>
            <div class="header__contact flex-middle">
              <div class="header__form"><a aria-label="お問い合わせページへ" href="<?= get_page_link( get_page_by_title( 'お問い合わせ' )) ?>">
                  <dl>
                    <dt><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/icon-mail-white.svg" alt="メールアイコン"></dt>
                    <dd>お問い合わせフォーム</dd>
                  </dl></a></div>
              <div class="header__line"><a aria-label="LINEアプリで連絡" href="https://lin.ee/beE9ebF/775ldykn">
                  <dl>
                    <dt><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/icon-line-color.png" alt="LINEアイコン"></dt>
                    <dd>LINEでのお問い合わせ</dd>
                  </dl></a></div>
            </div>