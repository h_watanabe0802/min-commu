            <ul class="nav__sns flex-middle">
              <li><a href="https://www.facebook.com/undokaiya/"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/icon-facebook.svg" alt="Facebook"></a></li>
              <li><a href="https://twitter.com/undokaiya"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/icon-twitter.svg" alt="Twitter"></a></li>
              <li><a href="https://www.instagram.com/undokaiya/"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/icon-instagram.svg" alt="Instagram"></a></li>
              <li><a href="https://www.youtube.com/channel/UCqYzJkR3ZaQ6SuA4v2GxORA"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/icon-youtube.svg" alt="Youtube"></a></li>
              <li><a href="https://lin.ee/beE9ebF/775ldykn"><img class="lazyload" data-src="<?= get_template_directory_uri() ?>/src/img/common/icon-line-grayscale.svg" alt="LINE"></a></li>
            </ul>