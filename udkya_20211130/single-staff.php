<?php get_header(); ?>
<?php get_template_part( 'partial/content', 'title' ); ?>
<?php if(have_posts()) : while(have_posts()) : the_post();
  $staff_id = $post->ID;
  $staff_metas = get_post_meta($staff_id);
  $office = current(wp_get_post_terms( $staff_id, 'office' ));
  $teams = wp_get_post_terms( $staff_id, 'teams' );
  $team_names = array();
  foreach($teams as $team) {
    $team_names[] = $team->name;
  }
?>
          <div class="staff__header">
            <div class="staff__header-image lazyload"><img src="<?= wp_get_attachment_image_url( $staff_metas['profile-image-main'][0], 'medium' ) ?>" alt="<?= $staff_metas['display_name'][0] ?>の顔写真"></div>
            <div class="staff__header-text">
              <div class="staff__header-content">
                <div class="staff__header-lead">
                  <p><?= $staff_metas['words-for-customer'][0] ?></p>
                </div>
                <div class="staff__header-info flex-middle-center-wrap">
                  <div class="staff__header-person">
                    <div class="staff__header-catchcopy">
                      <p><?= $staff_metas['catch-copy'][0] ?></p>
                    </div>
                    <div class="staff__header-name">
                      <h2 class="staff__header-name-text"><?= $staff_metas['display_name'][0] ?></h2>
                    </div>
                  </div>
                  <div class="staff__header-belong">
                    <dl>
                      <dt><?= join(' ', $team_names) ?></dt>
                      <dd><?= $office->name ?></dd>
                    </dl>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.staff__header-->
          <div class="staff__message">
            <p><?= $staff_metas['staff-message'][0] ?></p>
          </div>
          <div class="article">
            <div class="editor">
<?php the_content(); ?>
            </div>
            <!-- /.editor-->
            <aside class="aside lazyload">
              <ul class="aside__pagefeed flex-top">
                <li><?= next_post_link('%link', '←次のスタッフ', true, '', 'teams') ?></li>
                <li><?= previous_post_link('%link', '前のスタッフ→', true, '', 'teams') ?></li>
              </ul>
<?php
  $arg = array(
    'post_type' => ['columns', 'voices', 'post'],
    'posts_per_page' => 4,
    'meta_key' => '_udkya_writer_id',
    'meta_value' => $staff_id
  );
  $query = new WP_Query($arg);
  if ( $query->have_posts()) :
?>
              <div class="aside__inner">
                <h3 class="aside__title"><span>最近書いた記事</span></h3>
              </div>
              <div class="carousel">
                <div class="swiper-container lazyload" id="js-swiper-carousel">
                  <div class="swiper-wrapper">
<?php while ($query->have_posts()) : $query->the_post(); ?>
<?php get_template_part( 'partial/card', 'article-swiper' ); ?>
<?php endwhile; ?>
                  </div>
                  <!-- /.swiper-wrapper-->
                  <div class="swiper-pagination" id="js-swiper-carousel-pagination"></div>
                </div>
                <!-- /.swiper-container-->
              </div>
              <!-- /.carousel-->
<?php endif; wp_reset_postdata(); ?>
            </aside>
            <!-- /.aside-->
            <div class="staff__button flex-middle-center lazyload"><a class="button lazyload flex-middle-center -black" href="<?= get_post_type_archive_link( 'staff' ) ?>"><span class="button__text">スタッフ一覧へ</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></a></div>
          </div>

<?php endwhile;endif; ?>

<?php get_footer(); ?>
