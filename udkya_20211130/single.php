<?php get_header(); ?>
<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
<?php get_template_part( 'partial/content', 'title' ); ?>
<?php $category = current(get_the_terms( $post, get_taxonomy_slug($post->post_type))) ?>

	
          <div class="post-header">
            <div class="post-header__date"><?php the_date('Y.m.d') ?></div>
            <div class="post-header__small-text"><?= $category->name ?></div>
            <!-- / ↑カテゴリー名 or お客様名-->
            <h3 class="post-header__title"><?php the_title() ?></h3>
            <!-- / ↑記事タイトル-->
            <ul class="post-header__tag tag">
              <?= coco_get_the_tag_list($post->ID); ?>
            </ul>
          </div>
          <div class="editor">
<?php the_content(); ?>
          </div>
          <!-- /.editor-->
<?php endwhile; ?>
<?php
  $writer_id = get_post_meta( $post->ID, '_udkya_writer_id', true);
  if(!empty($writer_id)) :
    $writer_metas = get_post_meta($writer_id);
?>
          <div class="writer flex-center lazyload">
            <div class="writer__cloud"><img src="<?= get_template_directory_uri() ?>/src/img/common/writer-cloud.svg" alt="この記事を書いた人"></div>
            <div class="writer__content flex-middle">
              <div class="writer__image"><img src="<?= wp_get_attachment_image_url( $writer_metas['profile-image-main'][0], 'medium' ) ?>" alt="<?= $writer_metas['display_name'][0] ?>"></div>
              <div class="writer__text">
                <div class="writer__catchcopy">
                  <p><?= $writer_metas['catch-copy'][0] ?></p>
                </div>
                <div class="writer__name">
                  <p><?= $writer_metas['display_name'][0] ?></p>
                </div>
              </div>
            </div>
          </div>
          <!-- /.writer-->
  <?php endif; // !empty($writer_id) ?>
<?php endif; ?>

			<?php
    $title = get_the_title();
    $url_title = urlencode($title) ;
?>
<script src="https://kit.fontawesome.com/f542304393.js" crossorigin="anonymous"></script>
  <div class="share_box">
    <div class="fb_share">            <a href="https://www.facebook.com/share.php?u=<?php the_permalink(); ?>" target="blank" rel="noopener"><i class="fab fa-facebook-f"></i>Facebookでシェア</a> </div>
    <div class="tw_share">            <a href="https://twitter.com/share?url=<?php the_permalink() ?>&amp;text=<?php echo $url_title; ?>&amp;via=undokaiya" target="blank" rel="noopener"><i class="fab fa-twitter"></i>Twitterでシェア</a> </div>
  </div>	

          <aside class="aside lazyload">
            <ul class="aside__pagefeed flex-top">
              <li><?= next_post_link('%link', '←次の記事') ?></li>
              <li><?= previous_post_link('%link', '前の記事→') ?></li>
            </ul>
<?php
  $tag_ids = wp_get_post_tags( $post->ID, array( 'fields' => 'ids' ));
if(!empty($tag_ids)) :
  $arg = array(
    'post_type' => $post->post_type,
    'post__not_in' => [$post->ID],
    'tag__in' => $tag_ids,
    'posts_per_page' => 4
  );
  $query = new WP_Query($arg);
  if ( $query->have_posts()) : 
?>
            <div class="aside__inner">
              <h3 class="aside__title"><span>関連する記事</span></h3>
            </div>
            <div class="carousel">
              <div class="swiper-container lazyload" id="js-swiper-carousel">
                <div class="swiper-wrapper">
  <?php while ($query->have_posts()) : $query->the_post(); ?>
  <?php get_template_part( 'partial/card', 'article-swiper' ); ?>
  <?php endwhile; ?>
                </div>
                <!-- /.swiper-wrapper-->
                <div class="swiper-pagination" id="js-swiper-carousel-pagination"></div>
              </div>
              <!-- /.swiper-container-->
            </div>
            <!-- /.carousel-->
<?php endif; wp_reset_postdata(); // $query->have_posts() ?>
  <?php endif; //!empty($tag_ids) ?>
          </aside>
          <!-- /.aside-->
          <div class="post__button flex-middle-center lazyload"><a class="button lazyload flex-middle-center -black" href="<?= get_post_type_archive_link( $post->post_type ) ?>"><span class="button__text"><?= get_the_list_title($post->post_type) ?>一覧へ</span><span class="button__line -top"></span><span class="button__line -right"></span><span class="button__line -bottom"></span><span class="button__line -left"></span></a></div>
          <div class="tag-all release-background">
            <div class="tag-all__inner">
              <ul class="tag">
                <?= get_all_tags_list() ?>
              </ul>
            </div>
          </div>
          <!-- /.tag-list-->
<?php get_footer(); ?>

