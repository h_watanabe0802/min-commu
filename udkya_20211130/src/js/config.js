/* Init */
let $windowWidth = $(window).width()
let $windowHeight = $(window).height()
let $tabletMedium = 768
let $tabletLarge = 1024
let $pc = 1280

/* Wrapper */
$body = $('body')
$wrapper = $('#js-wrapper')

/* Header */
const $header = $('#js-header')
const $headerHeight = $header.height()
const $headerLogo = $('#js-header-logo')
const $headerLogoHeight = $headerLogo.height()
const $logoSmall = $('#js-logo-small')
const $logoLarge = $('#js-logo-large')

/* Mainvisual */
const $commonOffsetTop = parseInt($windowHeight * 0.54, 10)

/* Footer */
const $footer = $('#js-footer')

/* Logo Change */
let $logoAnimateFlag = false
function logoChange($scrollVal) {
	if(!$('body').hasClass('not-small-logo')){
		if($scrollVal > $commonOffsetTop){
			if(!$logoAnimateFlag){
				$logoAnimateFlag = true
				$headerLogo.fadeOut(240, function(){
					$logoLarge.css({'display': 'none'})
					$logoSmall.css({'display': 'inline'})
					$headerLogo.fadeIn()
					$body.addClass('is-small-logo')
				})
			}
		}else{
			if($logoAnimateFlag){
				$logoAnimateFlag = false
				$headerLogo.fadeOut(240, function(){
					$logoLarge.css({'display': 'block'})
					$logoSmall.css({'display': 'none'})
					$headerLogo.fadeIn()
					$body.removeClass('is-small-logo')
				})
			}
		}
	}
}

/* Release Sticky Header */
function toggleStickyHeader($scrollVal, $moveVal, $numVal) {
	if($scrollVal > $footer.offset().top - $(window).height() + ($(window).height() / parseInt($numVal))){
		$header.css({'transform': 'translateY(-' + ($moveVal + 6) + 'px)'})
	}else{
		$header.css({'transform': 'none'})
	}
}

/* ScrollStop */
let $currentScrollY;
function scrollStop(){
	$currentScrollY = $(window).scrollTop();
	$('body').css({
		'position': 'fixed',
		'width': '100%',
		'top': -1 * $currentScrollY,
		'z-index': 3,
	});
}

/* ScrollReturn */
function scrollReturn(){
	$('body').removeAttr('style');
	$('html, body').prop({scrollTop: $currentScrollY});
}

/* Download */
if(document.documentMode && navigator.msSaveOrOpenBlob){
	window.addEventListener('click', function(eve){
		var a = eve.target
		if(!a.hasAttribute('download')) return
			eve.preventDefault()
			var filename = a.getAttribute('download')
			var xhr = new XMLHttpRequest()
			xhr.open('GET', a.href)
			xhr.responseType = 'blob'
			xhr.send()
			xhr.onload = function(){
			navigator.msSaveOrOpenBlob(xhr.response, filename)
		}
	})
}


/* Online FAQ */
        $(".qa-list dd").hide();
        $(".qa-list dl").on("click", function (e) {
            $('dd', this).slideToggle('fast');
            if ($(this).hasClass('open')) {
                $(this).removeClass('open');
            } else {
                $(this).addClass('open');
            }
        });


/* Online matchHeight */
$(function(){
  $('.appli_content').matchHeight();
});