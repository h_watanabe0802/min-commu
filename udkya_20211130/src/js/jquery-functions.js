$(function(){

	/* Menu Button */
	const $menuButton = $('#js-menu-button')
	$menuButton.on('click', function(){
		if(!$body.hasClass('is-menu-open')){
			scrollStop()
			$body.addClass('is-menu-open')
			if($body.hasClass('is-small-logo')){
				$logoSmall.css({'display': 'none'})
			}
		}else{
			scrollReturn()
			$body.removeClass('is-menu-open')
		}
	})

	/* Add Target(blank), rel(noopener, noreferrer) */
	const $host = location.host
	$('a[href^="http"]').each(function(){
		if(!$(this).attr('href').match($host)){
			$(this).attr('target', '_blank').attr('rel', 'noopener noreferrer')
		}
	})

	/* Scroll */
	$(window).on('scroll', function(){
		toggleStickyHeader($(this).scrollTop(), $headerHeight, 3)
		if($windowWidth >= $tabletLarge){
			logoChange($(this).scrollTop())
		}
		if($(this).scrollTop() > $commonOffsetTop){
			$('#js-gotop').addClass('is-inview')
		}else{
			$('#js-gotop').removeClass('is-inview')
		}
	})

	/* Go Page Top */
	const $goPageTopButton = $('#js-gotop button')
	$goPageTopButton.on('click', function(e){
		e.preventDefault()
		$('html, body').animate({scrollTop: 0})
	})

	/* Smooth Scroll */
	$(document).on('click', 'a[href ^= "#"]', function(e){
		e.preventDefault()
    let $href= $(this).attr('href')
    let $target = $($href == "#" || $href == "" ? 'html' : $href)
    let $position = $target.offset().top + -80
    $('body, html').animate({scrollTop: $position}, 500, 'swing')
  })
})

$(window).on('load', function(){
	$('html').addClass('is-loaded')
})