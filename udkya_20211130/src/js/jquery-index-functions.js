$(function(){

	/* Ticker */
	const $ticker = $('#js-mainvisual-text')
	$ticker.height($ticker.height())
	const $tickerHeight = $ticker.height()
	const $tickerList = $ticker.find('ul')
	$ticker.find('li').css({'display': 'block'})

	if($ticker !== null){
		setInterval(function(){
			let $currentEle = $ticker.find('li').first()
			let $currentDistance = parseInt($currentEle.css('margin-top'), 10)
			let $movingDistance = ($tickerHeight + $currentDistance)

			$currentEle.animate({'margin-top': '-' + $movingDistance, 'opacity': 0}, 1000, () => {
				$currentEle.css({'margin-top': 0, 'opacity': 1})
				$tickerList.append($currentEle)
			})

		}, 5000)
	}

	/* Slider */
	const $sliderEle = $('#js-swiper')
	const $sliderParinationEle = $('#js-swiper-pagination')

	if($sliderEle !== null){
		let $slider = new Swiper($sliderEle, {
			speed: 1000,
			spaceBetween: 14,
			loop: true,
			pagination: {
				el: $sliderParinationEle,
				clickable: true,
			},
			autoplay: {
				delay: 5000,
			},
		})
	}

	/* Accordion ( Index ) */
	if($('.js-accordion-element')[0]){
		$.each($('.js-accordion-element'), function(){

			let $totalHeight = $(this).height()
			let $lineHeight = parseInt($(this).css('line-height'), 10)
			let $defaultHeightSmall = $(this).attr('data-default-line-small')
			let $defaultHeightMedium = $(this).attr('data-default-line-medium')

			if($windowWidth < $tabletMedium){
				let $defaultHeight = ($lineHeight * $defaultHeightSmall)
				$(this).attr('data-total-height', $totalHeight).css({'height': $defaultHeight}).attr('data-default-height', $defaultHeight)

				if($totalHeight <= ($lineHeight * $defaultHeightSmall)){
					$(this).parents('.js-accordion-box').find('.js-accordion-button').css({'display': 'none'})
				}
			}else{
				let $defaultHeight = ($lineHeight * $defaultHeightMedium)
				$(this).attr('data-total-height', $totalHeight).css({'height': $defaultHeight}).attr('data-default-height', $defaultHeight)
				if($totalHeight <= ($lineHeight * $defaultHeightMedium)){
					$(this).parents('.js-accordion-box').find('.js-accordion-button').css({'display': 'none'})
				}
			}
			$(this).css({'opacity': 1})
		})
	}

	$(document).on('click', '.js-accordion-button', function(e){

		e.preventDefault()

		let $accordionParents = $(this).parents('.js-accordion-box')
		let $accordionTarget = $accordionParents.find('.js-accordion-element')
		let $accordionDefaultHeight = $accordionTarget.attr('data-default-height')
		let $accordionTotalHeight = $accordionTarget.attr('data-total-height')

		$accordionParents.siblings('.js-accordion-box').find('.js-accordion-element').css({'height': $accordionParents.siblings('.js-accordion-box').find('.js-accordion-element').attr('data-default-height')})
		$accordionParents.siblings('.js-accordion-box').find('.js-accordion-button').removeClass('-active')

		if(!$(this).hasClass('-active')){
			$(this).addClass('-active')
			$accordionTarget.css({'height': $accordionTotalHeight})
		}else{
			$(this).removeClass('-active')
			$accordionTarget.css({'height': $accordionDefaultHeight})
		}
	})


})