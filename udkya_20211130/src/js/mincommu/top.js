
$(function () {

    $(".sp_open").click(function () {
        $(".nav_wrap").toggleClass("on");
        return false;
    });
    $(".sp_close").click(function () {
        $(".nav_wrap").toggleClass("on");
        return false;
    });

    $('.slick01').slick({
        autoplay: true, //自動でスクロール
        autoplaySpeed: 0, //自動再生のスライド切り替えまでの時間を設定
        speed: 15000, //スライドが流れる速度を設定
        cssEase: "linear", //スライドの流れ方を等速に設定
        slidesToShow: 5, //表示するスライドの数
        swipe: false, // 操作による切り替えはさせない
        arrows: false, //矢印非表示
        pauseOnFocus: false, //スライダーをフォーカスした時にスライドを停止させるか
        pauseOnHover: false, //スライダーにマウスホバーした時にスライドを停止させるか
        responsive: [{
            breakpoint: 769, //ブレイクポイントを指定
            settings: {
                slidesToShow: 3
            }
        }]
    });

    $('.slick.s6').on('init', function (event, slick) {
        // $(this).append('<div class="slick-counter"><p>COODINATE</p><span class="s">#</span><span class="current"></span><span class="ss"></span><span class="total"></span></div>');
        // $('.current').text(slick.currentSlide + 1);
        // $('.total').text(slick.slideCount);
    })
        .slick({
            autoplay: true,
            autoplaySpeed: 3000,
            dots: false,
            fade: false,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            prevArrow: '<img src="img/prev.svg" class="slide-arrow prev-arrow">',
            nextArrow: '<img src="img/next.svg" class="slide-arrow next-arrow">',
            responsive: [{
                breakpoint: 769, //ブレイクポイントを指定
                settings: {
                    slidesToShow: 1
                }
            }]
        })
        .on('beforeChange', function (event, slick, currentSlide, nextSlide) {

            // set();
        })
        .on('afterChange', function (event, slick, currentSlide, nextSlide) {


        })
    $('.slick.s2').on('init', function (event, slick) {
        // $(this).append('<div class="slick-counter"><p>COODINATE</p><span class="s">#</span><span class="current"></span><span class="ss"></span><span class="total"></span></div>');
        // $('.current').text(slick.currentSlide + 1);
        // $('.total').text(slick.slideCount);
    })
        .slick({
            autoplay: false,
            autoplaySpeed: 3000,
            dots: true,
            fade: false,
            arrows: true,
            slidesToShow: 3,
            infinite: false,
            slidesToScroll: 1,
            prevArrow: '<svg class="slide-arrow prev-arrow" xmlns="http://www.w3.org/2000/svg" width="18.312" height="24.739" viewBox="0 0 18.312 24.739"><g id="arrow2-1" transform="translate(2.801 2.801)"><line id="線_3" data-name="線 3" y1="9.569" x2="12.711" transform="translate(0 0)" fill="none" stroke="#deab00" stroke-linecap="round" stroke-width="4"/><line id="線_4" data-name="線 4" x2="12.711" y2="9.569" transform="translate(0 9.569)" fill="none" stroke="#deab00" stroke-linecap="round" stroke-width="4"/></g></svg>',
            nextArrow: '<svg class="slide-arrow next-arrow" xmlns="http://www.w3.org/2000/svg" width="18.312" height="24.739" viewBox="0 0 18.312 24.739"><g id="arrow2-1" transform="translate(2.801 2.801)"><line id="線_3" data-name="線 3" y1="9.569" x2="12.711" transform="translate(0 0)" fill="none" stroke="#deab00" stroke-linecap="round" stroke-width="4"/><line id="線_4" data-name="線 4" x2="12.711" y2="9.569" transform="translate(0 9.569)" fill="none" stroke="#deab00" stroke-linecap="round" stroke-width="4"/></g></svg>',
            responsive: [{
                breakpoint: 769, //ブレイクポイントを指定
                settings: {
                    slidesToShow: 1
                }
            }]
        })
        .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            // TweenMax.to(".slick-active .snap_img", 3, {
            // scale: 1,
            // x:0
            // })

        })
        .on('afterChange', function (event, slick, currentSlide, nextSlide) {

            // TweenMax.to(".snap_img", 3, {
            // scale: 1.1,
            // x:0

            // })

            $("img").not(".slick-active .snap_img").removeClass("on");
            $(".slick-active .snap_img").addClass("on");
        })

    $(".g_text1").each(function () {
        var text = $(this).text();
        $(this).next().text(text)
        $(this).next().next().text(text)

    });
    var sets = 0;

    function set() {
        sets = sets + 720;
        TweenMax.to('.day', 2, {
            ease: Expo.easeOut,
            // ease: Elastic.easeOut.config(1 ,0.3),
            // ease: Bounce.easeOut,
            rotationY: sets


        });
    }

    // TweenMax.to('.sec2', 2, {
    //     ease: Bounce.easeOut,
    //     // ease: Elastic.easeOut.config(1 ,0.3),
    //     // ease: Bounce.easeOut,
    //     opacity: 0


    // });


    // TextTypingというクラス名がついている子要素（span）を表示から非表示にする定義
    function TextTypingAnime() {
        $('.TextTyping').each(function () {
            var elemPos = $(this).offset().top - 50;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            var thisChild = "";
            if (scroll >= elemPos - windowHeight) {
                thisChild = $(this).children(); //spanタグを取得
                //spanタグの要素の１つ１つ処理を追加
                thisChild.each(function (i) {
                    var time = 100;
                    //時差で表示する為にdelayを指定しその時間後にfadeInで表示させる
                    $(this).delay(time * i).fadeIn(time);
                });
            } else {
                thisChild = $(this).children();
                thisChild.each(function () {
                    $(this).stop(); //delay処理を止める
                    $(this).css("display", "none"); //spanタグ非表示
                });
            }
        });
    }
    // 画面をスクロールをしたら動かしたい場合の記述
    $(window).scroll(function () {
        TextTypingAnime(); /* アニメーション用の関数を呼ぶ*/
    }); // ここまで画面をスクロールをしたら動かしたい場合の記述

    // 画面が読み込まれたらすぐに動かしたい場合の記述
    $(window).on('load', function () {
        //spanタグを追加する
        var element = $(".TextTyping");
        element.each(function () {
            var text = $(this).html();
            var textbox = "";
            text.split('').forEach(function (t) {
                if (t !== " ") {
                    textbox += '<span>' + t + '</span>';
                } else {
                    textbox += t;
                }
            });
            $(this).html(textbox);

        });

        TextTypingAnime(); /* アニメーション用の関数を呼ぶ*/
    }); // ここまで画面が読み込まれたらすぐに動かしたい場合の記述

    //////////	youtube api ///////////
    class_name = new Array("type1", "type2", "type3", "type4"); //*1
    font_size = new Array("uppercase", "lowercase", "uppercase", "lowercase"); //*1
    count = -1; //*2
    cont = 0; //*2
    // imgTimer();

    function imgTimer() {
        //画像番号
        count++; //*3
        cont++;
        //画像の枚数確認
        if (count == class_name.length) count = 0; //*4
        //画像出力
        var random_0 = Math.floor(Math.random() * 4);
        console.log(class_name[random_0])
        $(".sec5 h2").removeClass();
        $(".sec5 h2").addClass(class_name[random_0]).addClass(font_size[random_0]).addClass("noise");
        //次のタイマー呼びだし
        console.log(class_name[random_0])
        if (cont == 30) {

        } else {
            console.log("count" + count)
            setTimeout("imgTimer()", 100); //*6
        }
    }

    //  h１グリッジ
    $(".h1").each(function () {
        var g = $(this).text();
        var h = String(g).split("");
        var f = "";
        for (var e = 0; e < h.length; e++) {
            f += '<span class="obj01t">' + h[e] + "</span>"
        }
        $(this).html(f)
    });
    var min = 5;
    var max = 50;
    var min1 = 5;
    var max1 = 10;
    var count = Math.floor(Math.random() * (max + 1 - min)) + min;
    var count1 = Math.floor(Math.random() * (max1 + 1 - min1)) + min1;

    function clear() {
        TweenMax.to(".obj01t", 0, {
            x: 0,
            y: 0,
            color: "#fff",
            opacity: 1,
            scaleX: 1,
            scaleY: 1
        })
    }

    function glitch() {
        var b = Math.floor(Math.random() * 3);
        console.log(b);
        color = ["#00aaff", "#c0147c", "#ffee00"];
        $(".obj01t").each(function (n) {
            var k = -300;
            var a = 300;
            var i = -800;
            var m = 800;
            var j = Math.floor(Math.random() * (a + 1 - k)) + k;
            var l = Math.floor(Math.random() * (m + 1 - i)) + i;
            ob = ".obj01t:nth-child(" + n + ")";
            TweenMax.to(ob, 0, {
                x: l,
                y: j,
                color: color[b],
                opacity: 0.4,
                scaleX: 25,
                scaleY: 2
            })
        });
        setTimeout(clear, 100)
    }

    function glitch2() {
        var b = Math.floor(Math.random() * 3);
        color = ["#00aaff", "#c0147c", "#17d200"];
        $(".obj01t").each(function (n) {
            var k = -250;
            var a = 300;
            var i = -800;
            var m = 800;
            var j = Math.floor(Math.random() * (a + 1 - k)) + k;
            var l = Math.floor(Math.random() * (m + 1 - i)) + i;
            ob = ".obj01t:nth-child(" + n + ")";
            TweenMax.to(ob, 0, {
                x: l,
                y: j,
                color: color[b],
                opacity: 0.4,
                scaleX: 25,
                scaleY: 2
            })
        });
        setTimeout(clear, 100)
    }

    function gritch_wrap() {
        setInterval(glitch, 2000);
        setInterval(glitch2, 2200)
    }

    gritch_wrap();
    // $(".youtube2").hover(function(){

    // })
    // $(".slick-active .g_text1").each(function() {
    //     var g = $(this).text();
    //     var h = String(g).split("");
    //     var f = "";
    //     for (var e = 0; e < h.length; e++) {
    //         f += '<span class="obj" style="display:inline-block;">' + h[e] + "</span>"
    //     }
    //     $(this).html(f)
    // });

    // TweenMax.staggerTo(".obj", 0.1, {
    //     repeat: 1,
    //     y: 10,
    //     yoyo: true
    // }, 0.1)



    $(".nav_category dt a").on("click", function () {
        $(this).parent().next().slideToggle(500);
        $(this).parent().toggleClass("on");
        return false;
    });

    function text_set() {
        var min = 0;
        var max = 6;

        var count = Math.floor(Math.random() * (max + 1 - min)) + min;
        var count1 = Math.floor(Math.random() * (max + 1 - min)) + min;
        var count2 = Math.floor(Math.random() * (max + 1 - min)) + min;
        $(".obj01t:nth-child(" + count + ")").addClass("on")
        $(".obj01t:nth-child(" + count1 + ")").addClass("on")
        $(".obj01t:nth-child(" + count2 + ")").addClass("on")

    }
    function text_normal() {
        $(".obj01t").removeClass("on")
    }

    var flg = 0;
    function change() {

        if (flg == 0) {
            text_set()
            flg = 1;
        } else {
            text_normal()
            flg = 0;
        }
    }
    // setInterval(change, 3000);


    function set1() {
        $(".sec2_a1").addClass("fade_t")
    }
    function set2() {
        $(".sec2_a2").addClass("fade_t")
    }
    function set3() {
        $(".sec2_a3").addClass("fade_t")
    }
    function set4() {
        $(".sec2_a4").addClass("fade_t")
    }
    function set5() {
        $(".sec2_a5").addClass("fade_t")
    }
    function set6() {
        $(".sec2_a6").addClass("fade_t")
    }
    function set7() {
        $(".sec2_a4 img").addClass("fade_t")
    }


    $('.sec2').on('inview', function (event, isInView) {
        if (isInView) {

            setTimeout(set1, 300);
            setTimeout(set2, 600);
            setTimeout(set3, 900);
            setTimeout(set4, 1200);
            setTimeout(set5, 1500);
            setTimeout(set6, 1800);
            setTimeout(set7, 4000);
        } else {
            $(this).removeClass('fadeInRight');
        }
    });

    function set8() {
        $(".sec4-1__innar").addClass("fade_t")
    }
    function set9() {
        $(".r__wrap__top").addClass("fade_t")
    }
    function set10() {
        $(".r__wrap__bottom").addClass("fade_t")
    }
    function set11() {
        $(".l__wrap").addClass("fade_t")
    }

    $('.sec4-1').on('inview', function (event, isInView) {
        if (isInView) {

            setTimeout(set8, 500);
            setTimeout(set9, 1000);
            setTimeout(set10, 1400);
            setTimeout(set11, 1700);
        } else {
            $(this).removeClass('fadeInRight');
        }
    });

    function set12() {
        $(".r1").addClass("fade_t")
    }
    function set13() {
        $(".r2").addClass("fade_t")
    }
    function set14() {
        $(".r3").addClass("fade_t")
    }
    function set15() {
        $(".r4").addClass("fade_t")
    }
    function set16() {
        $(".r5").addClass("fade_t")
    }


    $('.sec5').on('inview', function (event, isInView) {
        if (isInView) {
            $(".sec5 ul").addClass("active")
            setTimeout(set12, 1000);
            setTimeout(set13, 1300);
            setTimeout(set14, 3500);
            setTimeout(set15, 1800);
            setTimeout(set16, 3000);
        } else {
            $(this).removeClass('fadeInRight');
        }
    });
    // //=================================
    // //カーソル要素
    // var cursor=$("#cursor");
    // //ちょっと遅れてくる要素
    // var stalker=$("#stalker");

    // //mousemoveイベントでカーソル要素を移動
    // $(".sec2_a4").on("mousemove",function(e){
    // 	//マウス位置を取得するプロパティ
    // 	var x=e.clientX;
    // 	var y=e.clientY;

    // 	//カーソル要素のcssを書き換え。重複しなければtransformを使うのがおすすめ
    // 	cursor.css({
    // 		"opacity":"1",
    // 		"top":y+"px",
    // 		"left":x+"px"
    // 	});


    // });

    // //aタグホバー
    // $(cursor).on({
    // 	"mouseenter": function() {
    // 		//activeクラス付与
    // 		cursor.addClass("active");
    // 		stalker.addClass("active");
    // 	},
    // 	"mouseleave": function() {
    // 		cursor.removeClass("active");
    // 		stalker.removeClass("active");

    // 	}

    // });


    //音を鳴らす
    $('.nav_category a').mouseover(function () {

        document.getElementById("overSound").currentTime = 0;
        document.getElementById("overSound").volume = 0.01;
        document.getElementById("overSound").play();

    });

    var flg2 = 0;
    function set_anim3() {

        if (flg2 == 0) {

            $(".c2").addClass("rotate");
            flg2 = 1;
        } else {
            $(".c2").removeClass("rotate");
            flg2 = 0;
        }
    }


    setInterval(set_anim3, 3000)



});

