$(function(){

	/* Chnage Form Tab */
	$(document).on('click', '#js-contact-tab a', function(e){

		e.preventDefault()

		$(this).parents('ul').find('a').removeClass('-active')
		$(this).addClass('-active')

		const $targetForm = $('#' + $(this).attr('href'))

		$('.js-form-content').not($targetForm).animate({'opacity': 0}, 500, function(){
			$('.js-form-content').not($targetForm).css({'display': 'none'}).removeClass('-active')
			$targetForm.css({'display': 'block'}).animate({'opacity': 1}, 500).addClass('-active')
		})
	})
})

function validateResult($flag, $name){
	if(!$flag){
		$('[name="' + $name + '"]').addClass('focus')
		$('.error.' + $name).fadeIn().css({'display': 'block'})
	}else{
		$('[name="' + $name + '"]').removeClass('focus')
		$('.error.' + $name).fadeOut().css({'display': 'none'})
	}
}

function validateRadio($ele){
	for(let $i = 0; $i < $ele.length; $i++){
		if($ele[$i].checked){
			return true
			break
		}
	}
}

function validateEmail($val){
	if(!$val.match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/)){
		return false
	}else{
		return true
	}
}

function validateZipcode($val){
	if($val.match(/^[0-9０-９]{3}[-ー−]?[0-9０-９]{4}$/)){
		return true
	}else{
		return false
	}
}

/* Toggle Disabled Required */
function toggleDisabled($ele, $action){
	if($action){
		$ele.removeAttr('disabled')
	}else if(!$action){
		$ele.attr('disabled', 'disabled')
	}
}