let $flagsDownload = {
	yourname: false,
	email: false,
	agree: false,
	recaptcha: false,
}
let $namesDownload = {
	yourname: 'download-yourname',
	email: 'download-email',
	agree: 'download-agree',
	recaptcha: 'download-recaptcha',
}
function validateReCaptchaDownload(){
	$flagsDownload['recaptcha'] = true
}

$(function(){

	/* Allow Document Download */
	$(document).on('click', '#js-form-document a', function(e){
		if(!$downloadDocumentEle.hasClass('-allow-download')){
			e.preventDefault()
			alert('資料をダウンロードするにはフォームの入力が必要です。')
			$formDownload.find('input[name="download-email"]').focus()
		}
	})

	const $formDownload = $('#js-download-form')
	const $submitDownload  = $('#js-download-submit')

	const $downloadResult = $('#js-form-result')
	const $downloadResultTitle = $downloadResult.find('.form__result-title')
	const $downloadResultContent = $downloadResult.find('.form__result-content')
	const $downloadDocumentEle = $('#js-form-document')

	/* Submit and Validate */
	$submitDownload.on('click', function(e){
		e.preventDefault()

		/* [name="inquiry-email"] */
		const $eleEmailDownload = $('[name="'+ $namesDownload['email'] +'"]')
		if($eleEmailDownload.val() !== ''){
			if(!validateEmail($eleEmailDownload.val())){
				$flagsDownload['email'] = false
				$('.error.' + $namesDownload['email']).text('※メールアドレスに使用できない文字が含まれています')
			}else{
				$flagsDownload['email'] = true
			}
		}else{
			$flagsDownload['email'] = false
		}
		validateResult($flagsDownload['email'], $namesDownload['email'])

		/* [name="inquiry-yourname"] */
		const $eleYourNameDownload = $('[name="'+ $namesDownload['yourname'] +'"]')
		$flagsDownload['yourname'] = ($eleYourNameDownload.val() !== '') ? true : false
		validateResult($flagsDownload['yourname'], $namesDownload['yourname'])

		/* [name="inquiry-agree"] */
		const $eleDownloadAgree = $('[name="'+ $namesDownload['agree'] +'"]')
		$flagsDownload['agree'] = validateRadio($eleDownloadAgree) || false
		validateResult($flagsDownload['agree'], $namesDownload['agree'])

		/* reCaptcha */
		const $nameDownloadreCaptcha = $namesDownload['recaptcha']
		validateResult($flagsDownload['recaptcha'], $nameDownloadreCaptcha)

		let $resultFlagDownload = []

		for(let key in $flagsDownload){
			$resultFlagDownload.push($flagsDownload[key])
		}

		if($.inArray(false, $resultFlagDownload) >= 0){
			console.log('Validation Error')
			if($('.focus').length){
				if($windowWidth >= 768){
					$('html, body').animate({scrollTop: ($('.focus').eq(0).offset().top - 150)});
				}else{
					$('html, body').animate({scrollTop: ($('.focus').eq(0).offset().top - 100)});
				}
			}
		}else{
			console.log('Validation Success')
			var $ajaxAction  = 'coco_ajax';


			const $downloadValues = {
				email: $formDownload.find('input[name="download-email"]').val(),
				company: $formDownload.find('input[name="download-company"]').val(),
				yourname: $formDownload.find('input[name="download-yourname"]').val(),
				phone: $formDownload.find('input[name="download-phone"]').val(),
				recaptcha: $formDownload.find('[name="g-recaptcha-response"]').val(),
			}

			$.ajax({
				url: $ajaxURL,
				type: 'POST',
				data:{
					'action': $ajaxAction,
					'values': $downloadValues
				}
			})
			.done(($data) => {
				if(parseInt($data)){
					console.log('AJAX Success')
					$downloadResultTitle.html('送信完了しました。')
					$downloadResultContent.html('<p>ご協力ありがとうございます。</p><p>下記より資料のダウンロードが可能になっております。</p>')
					$downloadDocumentEle.addClass('-allow-download')
					$.each($downloadDocumentEle.find('a'), function(){
						$(this).attr('href', $(this).attr('data-url'))
					})
					$formDownload.find('input').val('')
					$formDownload.find('input[type="checkbox"]').prop('checked', false)
					$formDownload.find('.form__group').animate({'opacity': .5})
					$submitDownload.attr('disabled', 'disabled')
					$('html, body').animate({scrollTop:($submitDownload.offset().top + 50) });
				}else{
					console.log('AJAX Success. But Failed email send')
					$downloadResultTitle.html('送信できませんでした。')
					$downloadResultContent.html('<p>送信中にエラーが発生しました。</p><p>お手数ですがページを再読込して操作をやり直してください。<br>ERROR CODE: 02</p>')
					$downloadDocumentEle.removeClass('-allow-download')
					$.each($downloadDocumentEle.find('a'), function(){
						$(this).attr('href', '/')
					})
					$submitDownload.removeAttr('disabled')
				}
			})
			.fail(($data) => {
				console.log('AJAX Failed')
				$downloadResultTitle.html('送信できませんでした。')
				$downloadResultContent.html('<p>送信中にエラーが発生しました。</p><p>お手数ですがページを再読込して操作をやり直してください。<br>ERROR CODE: 03</p>')
				$downloadDocumentEle.removeClass('-allow-download')
				$.each($downloadDocumentEle.find('a'), function(){
					$(this).attr('href', '/')
				})
				$submitDownload.removeAttr('disabled')
			})
			.always(($data) => {
				$downloadResult.fadeIn(680)
				$('html, body').animate({scrollTop:($submitDownload.offset().top + 50) });
				console.log($data)
			})
		}
	})
})