let $flagsInquiry = {
	matter: false,
	company: false,
	yourname: false,
	phone: false,
	email: false,
	zip: false,
	address: false,
	purpose: false,
	agree: false,
	recaptcha: false,
	meeting: false,
	meetingHow: true,
	meetingTool: true,
	meetingDate: true,
}

let $namesInquiry = {
	matter: 'inquiry-matter',
	company: 'inquiry-company',
	yourname: 'inquiry-yourname',
	phone: 'inquiry-phone',
	email: 'inquiry-email',
	zip: 'inquiry-zip',
	address: 'inquiry-address',
	purpose: 'inquiry-purpose',
	agree: 'inquiry-agree',
	recaptcha: 'inquiry-recaptcha',
	meeting: 'inquiry-meeting',
	meetingHow: 'inquiry-meeting-how',
	meetingTool: 'inquiry-meeting-tool',
	meetingDate: 'inquiry-meeting-date',
}

let $meetingHowChecked

function validateReCaptchaInquiry(){
	$flagsInquiry['recaptcha'] = true
}

$(function(){

	/* Datetimepicker */
	$.datetimepicker.setLocale('ja');
	$(document).on('click', '.js-datetitmepicker-icon', function(){
		$(this).next('.datetimepicker').datetimepicker('show')
	})
	$('.datetimepicker').datetimepicker({
		format: 'Y/m/d H:i',
		minDate: 0,
		defaultTime: '10:00',
		scrollInput: false,
		allowTimes: ['10:00', '10:30', '11:00', '11:30', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00', '17:30', '18:00']
	})

	const $formInquiry = $('#js-inquiry-form')
	const $submitInquiry = $('#js-inquiry-submit')

	/* Meeting */
	const $contactFormGroup = $formInquiry.find('.js-form-group')
	const $meetingTargetEle = $formInquiry.find('input[name="inquiry-meeting-how[]"], input[name="inquiry-meeting-date"]')

	const $meetingRadio = $formInquiry.find('input[name="' + $namesInquiry['meeting'] + '"]')
	let $meetingChecked

	$meetingRadio.on('change', function(){
		if(parseInt($(this).val())){
			$contactFormGroup.removeClass('-inactive')
			toggleDisabled($contactFormGroup.find($meetingTargetEle), 1)
		}else{
			$contactFormGroup.addClass('-inactive')
			toggleDisabled($contactFormGroup.find($meetingTargetEle), 0)
			$('input[name="inquiry-meeting-tool"]').attr('disabled', 'disabled')
			$contactFormGroup.find('.error').css({'display': 'none'})
		}
		$meetingChecked = $(this).val()
	})

	/* Toggle Disabled from Meeting Tool */
	const $meetingHowEle = $formInquiry.find('input[name="' + $namesInquiry['meetingHow'] + '[]"]')
	const $meetingToolEle = $formInquiry.find('input[name="' + $namesInquiry['meetingTool'] + '"]')

	$meetingHowEle.on('change', $meetingHowEle, function(){
		$meetingHowChecked= []
		$meetingHowEle.each(function(){
			if($(this).prop('checked') === true){
				$meetingHowChecked.push($(this).val())
			}
		})

		if($meetingHowChecked.includes('other')){
			toggleDisabled($meetingToolEle, 1)
		}else{
			toggleDisabled($meetingToolEle, 0)
		}
	})

	/* Submit and Validate */
	$submitInquiry.on('click', function(e){

		e.preventDefault()

		/* [name="inquiry-matter"] */
		const $eleMatter = $('[name="'+ $namesInquiry['matter'] +'"]')
		$flagsInquiry['matter'] = validateRadio($eleMatter) || false
		validateResult($flagsInquiry['matter'], $namesInquiry['matter'])

		/* [name="inquiry-company"] */
		const $eleCompany = $('[name="'+ $namesInquiry['company'] +'"]')
		$flagsInquiry['company'] = ($eleCompany.val() !== '') ? true : false
		validateResult($flagsInquiry['company'], $namesInquiry['company'])

		/* [name="inquiry-yourname"] */
		const $eleYourName = $('[name="'+ $namesInquiry['yourname'] +'"]')
		$flagsInquiry['yourname'] = ($eleYourName.val() !== '') ? true : false
		validateResult($flagsInquiry['yourname'], $namesInquiry['yourname'])

		/* [name="inquiry-phone"] */
		const $elePhone = $('[name="'+ $namesInquiry['phone'] +'"]')
		$flagsInquiry['phone'] = ($elePhone.val() !== '') ? true : false
		validateResult($flagsInquiry['phone'], $namesInquiry['phone'])

		/* [name="inquiry-email"] */
		const $eleEmail = $('[name="'+ $namesInquiry['email'] +'"]')
		if($eleEmail.val() !== ''){
			if(!validateEmail($eleEmail.val())){
				$flagsInquiry['email'] = false
				$('.error.' + $namesInquiry['email']).text('※メールアドレスに使用できない文字が含まれています')
			}else{
				$flagsInquiry['email'] = true
			}
		}else{
			$flagsInquiry['email'] = false
		}
		validateResult($flagsInquiry['email'], $namesInquiry['email'])

		/* [name="inquiry-zip"] */
		const $eleZip = $('[name="'+ $namesInquiry['zip'] +'"]')
		if($eleZip.val() !== ''){
			if(!validateZipcode($eleZip.val())){
				$flagsInquiry['zip'] = false
				$('.error.' + $namesInquiry['zip']).text('※正しい郵便番号を入力してください。')
				validateResult($flagsInquiry['zip'], $namesInquiry['zip'])
			}else{
				$flagsInquiry['zip'] = true
				validateResult($flagsInquiry['zip'], $namesInquiry['zip'])
			}
		}else{
			$flagsInquiry['zip'] = false
			validateResult($flagsInquiry['zip'], $namesInquiry['zip'])
		}

		/* [name="inquiry-address"] */
		const $eleAddress = $('[name="'+ $namesInquiry['address'] +'"]')
		$flagsInquiry['address'] = ($eleAddress.val() !== '') ? true : false
		validateResult($flagsInquiry['address'], $namesInquiry['address'])

		/* [name="inquiry-purpose"] */
		const $elePurpose = $('[name="'+ $namesInquiry['purpose'] +'"]')
		$flagsInquiry['purpose'] = ($elePurpose.val() !== '') ? true : false
		validateResult($flagsInquiry['purpose'], $namesInquiry['purpose'])

		/* [name="inquiry-agree"] */
		const $eleInquiryAgree = $('[name="'+ $namesInquiry['agree'] +'"]')
		$flagsInquiry['agree'] = validateRadio($eleInquiryAgree) || false
		validateResult($flagsInquiry['agree'], $namesInquiry['agree'])

		/* reCaptcha */
		const $nameInquiryreCaptcha = $namesInquiry['recaptcha']
		validateResult($flagsInquiry['recaptcha'], $nameInquiryreCaptcha)

		/* [name="inquiry-meeting"] */
		const $eleMeeting = $('[name="'+ $namesInquiry['meeting'] +'"]')
		$flagsInquiry['meeting'] = validateRadio($eleMeeting) || false
		validateResult($flagsInquiry['meeting'], $namesInquiry['meeting'])

		if(parseInt($meetingChecked)){
			if($meetingHowChecked && $meetingHowChecked.length > 0){
				$flagsInquiry['meetingHow'] = true
				$('.error.' + $namesInquiry['meetingHow']).fadeOut().css({'display': 'none'})

				if($meetingHowChecked && $meetingHowChecked.includes('other')){
					const $eleMeetingTool = $('[name="'+ $namesInquiry['meetingTool'] +'"]')
					$flagsInquiry['meetingTool'] = ($eleMeetingTool.val() !== '') ? true : false
					validateResult($flagsInquiry['meetingTool'], $namesInquiry['meetingTool'])
				}else{
					$flagsInquiry['meetingTool'] = true
					validateResult($flagsInquiry['meetingTool'], $namesInquiry['meetingTool'])
				}
			}else{
				$flagsInquiry['meetingHow'] = false
				validateResult($flagsInquiry['meetingHow'], $namesInquiry['meetingHow'])
			}

			const $eleMeetingDate = $('[name="'+ $namesInquiry['meetingDate'] +'"]')
			$flagsInquiry['meetingDate'] = ($eleMeetingDate.val() !== '') ? true : false
			validateResult($flagsInquiry['meetingDate'], $namesInquiry['meetingDate'])
		}else{
			$flagsInquiry['meetingHow'] = true
			$flagsInquiry['meetingTool'] = true
			$flagsInquiry['meetingDate'] = true
		}

		let $resultFlagInquiry = []

		for(let key in $flagsInquiry){
			$resultFlagInquiry.push($flagsInquiry[key])
		}

		if($.inArray(false, $resultFlagInquiry) >= 0){
			console.log('Error')
			if($('.focus').length){
				if($windowWidth >= 768){
					$('html, body').animate({scrollTop: ($('.focus').eq(0).offset().top - 150)});
				}else{
					$('html, body').animate({scrollTop: ($('.focus').eq(0).offset().top - 100)});
				}
			}
		}else{
			console.log('Success')
			$formInquiry.submit()
		}
	})

	if($('#js-back') !== null){
		$('#js-back').on('click', function(e){
			e.preventDefault()
			console.log(1)
			$formInquiry.find('[name="step"]').val('back')
			$formInquiry.submit()
		})
	}

})