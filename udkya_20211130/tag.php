<?php get_header(); ?>
<?php get_template_part( 'partial/content', 'title' ); ?>
          <div class="list">
            <div class="tag-header release-background">
              <div class="tag-header__content">
                <h3 class="tag-header__ttile">#<?php single_tag_title() ?></h3>
                <p class="tag-header__sub">に関する記事の一覧です</p>
              </div>
            </div>
<?php 
$tag_list_link = get_tag_link( $tag_id );
$posts_count = array('voices' => 0, 'columns' => 0, 'post' => 0);
$matches = get_matches_for_tag_post_type();
if($matches==array()) {
  $posts = $wp_query->posts;
} else {
  $posts = get_posts( array(
    'post_type'      => coco_all_post_type_for_tag_post(),
    'tag__in' => $tag_id
  ) );
}

foreach( $posts as $post ) {
  $posts_count[$post->post_type] += 1;
}
?>
            <div class="list-tab tab">
              <ul>
                <li><a href="<?= $tag_list_link ?>">All（<?= count($posts) ?>）</a></li>
<?php if($posts_count['voices']>0) : ?>
                <li><a href="<?= $tag_list_link.'?g=voices' ?>">お客様の声（<?= $posts_count['voices'] ?>）</a></li>
<?php endif; ?>
<?php if($posts_count['columns']>0) : ?>
                <li><a href="<?= $tag_list_link.'?g=columns' ?>">コラム（<?= $posts_count['columns'] ?>）</a></li>
<?php endif; ?>
<?php if($posts_count['post']>0) : ?>
                <li><a href="<?= $tag_list_link.'?g=post' ?>">お知らせ（<?= $posts_count['post'] ?>）</a></li>
<?php endif; ?>
              </ul>
            </div>
<?php get_template_part( 'partial/list', 'posts' ); ?>
          </div>
          <!-- /.list-->
          <div class="tag-all release-background">
            <div class="tag-all__inner">
              <ul class="tag">
                <?= get_all_tags_list() ?>
              </ul>
            </div>
          </div>
          <!-- /.tag-list-->
<?php get_footer(); ?>
